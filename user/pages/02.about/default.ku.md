---
title: 'Derbarê me'
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

## Erk
Misyona me ji jêr ve avakirina sîstemek aboriya 'glokal'e di modela postkapîtalîst de, ji bo rêya jiyana komînal çêbikin. Hevkarî, ehlaq, hevgirtin û zelalî bingeh in ji bo avakirina sîstemek bi rastî adîlbûn ji bo herkesê. Pêşketin û bikaranîna torek amûrên dijîtal bi hêz (cîhanî) û torên NH (herêmî) ji bo serkeftina me pir girîngin. Dem û enerjiya me ji bo avakirina komînalbûn û koperatîve, û alternatîvên rastî çêbikin, bi teorî pratîk bikîn.

## Lênêrîn
Xewna me ewe ku em toreke mirov û komînên xwe-rêxistinkirî û xwe-bihêzkirî, ku tam serbest û ji derveyê desthilatdariya navendî û bê sînor ava bikine. Em armanc û prensîpên xwe bi hev re biryar li ser didin, ji bo 99% gel, lê jî ji bo pêwîstiyên herêm li gorî taybetmendiyên xwe wiha ye. Wisa gel kontrola jiyana xwe dîsa digirin destê xwe, ji ber ku derfeta me heye, hilbijartina modelên civakî yên din li ser bingeha avakirina rêxistina bênavend pêkbînin. Hevkarê me ekosîstemekî nû û serbixwe diafirîne, ku armanca wê, kêmkirina newekheviya cîhanî ya aborî û civakî ye. Bandora vê komûne: dewlemendiya cîhaniya nû bi destê herkesê dikeve. Wek bandora domîno, ev sîtema aborî çarçoveya şoreşgere ku ji guhertina cîhanî re derî vedike, û bandora wê ser hemû mijarên jiyanê heye: perwerde, tendirustî, enerjî, xanî, veguhestin, û hwd., û derfet diafirîne ji bo awayên sax û nekarsazî yan tekiliyên mirov û bi xweza re sax dîsa nasbikin.

## Prensîp
Bingeha çalakiya komînên FC **Şoreşa întegral, , Hevkariya P2P û Ehlaqê Haker e.**


### 1) Şoreş întegral
Proseyek ya nirxên dîrokî ye, ji bo avakirina civakek nikare xwe rêve bibe,li ser bingeha xweserîtî ye û ji bo ji holê rakirina hemî formên dedthelatdarî yên weke:dewlet, kapîtalîzm û hemî formên din yên ku bandorê li ser têkiliya di navbera mirov û jîngehî de çedikine. Xwe li ser tevgera wijdanî,kesî û kolektîv ava dike. Ji bo dubare paşve anîna nirx û yeksaniyan ya bo jiyanek hevbeş hewl dide. Li ser bingeha adil ya avahîsaziyên nû û formên xwe rêxistinkirinê di her qadek ya jiyanê de bi armanca dilniyabûna di biryar dayîn û yeksaniya civînan de ku pêwîstiyên me yên binhegîn û jiyanîne. Evane hene:
Têkiliyên mirovan yên wekhev li ser bingeha azadî û ne zorê ye
* Rêxistinên Xweser û meclîsên komûnên populer yên serdest
* Komun ji kerkes re ye ango Komûn û gel
    * Destûra hevbeş ji bo berjewendiya giştî, bi xwedaniya populer û kontrola wê
    * Avakirina sîstemek koperatîv ku giştî û xwerêveber be, li ser bingeha beramberî
    * Destûrek serbixwe ya xwe gihandins agahî û zanînan
* Aboriya nû li ser hevkarî û nêzîkbûnê ye.
* Hevkariya bi jiyan û biyosfer.


<a class="block-link" href="http://integrarevolucio.net/en/integral-revolution/ideological-bases-of-the-call/">http://integrarevolucio.net/en/integral-revolution/ideological-bases-of-the-call/</a>


### 2) P2P Hevkarî Kirin
* Hevkariya P2P tê wateya herkes hinek rolên anjî erkên misoger digre ser milê xwe û di nava komînê de weku dilxwazek girêdanker dibe malê giştî.
* ADilsozek raste rast li gel berpirsyarên hatine pejirandin divê çaverê nemîne . Her çawa be , weku çawa her beşdarek wê hewl bide lênêrîna mutûman û piştgiriya di nava giştî de pêkbîne, wê derfet hebe hinek tiştan ji giştiyê jî paşve bi dest bixe.
* Hemî dînamîkên hevkarîyê ne-hîyerarşîkin, bi vî awayî herkes ku tevilbun çêkiribe wê weku dengek yeksan di vê proseyê de bêne êjmartin.
* Li vê derê rêyek heye ku em fêrî prensîb û herêmên cuda cuda yên P2P bibin.

<a class="block-link" href="https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles">Hun Dikarin Agahiyên Bi Nirx Yên Zêdetir Li Vir Bibîn</a>



### 3) Ehlaqê Haker
Hack kirin tiştek wêdetire ji wêneyê Sîberkirîmînal li gel sernavên reş. Ya em û piraniya kesên teknîkî yên azad fêhm dikin, çalakiya hack kirinê serlêdana ji bo nirxên ehlaqî, azadî û sererastkirina berhemane. Mifteyên Ehlaqê hack kirinê evane ne:
* Hemî agahî divê azad bin
* Navketina komputeran - her tiştek din ku di derbarê ka cîhan çawa dimeşe de fêrî we dike - divê bê sinor û giştî be. Divê herdem bête li berdest û ev gelek girînge.
* Desthilatên nebawer - Zêdekirina ne navendîtî ye
* Divê kesên hacker bi tiştên hack kirî bêne darizandin, neku bi krîterên weku pile, temen, nijad, zayend anjî payeyên wan
* Hun dikarin li ser komputerê huner û ciwankariyê bi afirînin
* Komputer dikare jiyana we di aliyê baştir de biguhere
* Daneyên kesên din navêjin nava gelaşê
* Daneyên giştî bixin berdest, yên taybet biparêzin

<a class="block-link" href="https://ccc.de/en/hackerethik">Agahiyên Zêdetir Li Ser Ehlaqê Hacker Bibîne Vê Derê</a>