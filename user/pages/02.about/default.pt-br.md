---
title: 'Sobre nós'
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

## Missão
Nossa missão é criar um sistema econômico global inovador, a partir dos movimentos de bases, em favor de um modelo alternativo e pós-capitalista, e assim abrir caminho para uma mudança coletiva em direção a uma vida em comum. Cooperação, solidariedade e transparência são fatores fundamentais para que seja possível criar um sistema verdadeiramente justo para todos. O desenvolvimento e o emprego de poderosas ferramentas digitais interconectadas (globais) e redes regionais (locais) são cruciais para o nosso sucesso. Usamos nosso tempo e energia construtivamente, de forma colaborativa, para criar alternativas reais, transformando a teoria em prática.

## Visão
Vislumbramos uma rede global de comunidades e indivíduos auto-organizados e autocapacitados, que seja independente dos abusos de forças e restrições centralizadas. Nossos princípios e disposições globais são definidos coletivamente, em nome dos 99%, ao mesmo tempo que também são decididos a nível local, de acordo com necessidades e circunstâncias regionais específicas. Contando com liberdade para escolher diversos modelos da sociedade alternativa com base em uma estrutura de organização descentralizada, as pessoas retomam o controle de suas próprias vidas. Nosso trabalho cooperativo está criando um ecossistema independente, que visa reduzir a desigualdade econômica e social no planeta e levar a uma nova riqueza global acessível a todos sob a forma dos Comuns (uso e governança de 'Bens Comuns' na sociedade). Como um efeito dominó, esse marco muda as regras do jogo do sistema econômico e abre as portas para uma mudança global, com impacto em todos os aspectos da vida: educação, saúde, energia, habitação e transporte. Além disso, serão criadas também oportunidades para redescobrir relacionamentos saudáveis e livres de exploração com os outros seres humanos e a natureza.

## Princípios
As ações da comunidade da FairCoop são baseadas em três grandes princípios já existentes: **Revolução Integral, Colaboração P2P, Ética hacker**

### 1) Revolução Integral
É um processo de significado histórico para a construção de uma nova sociedade autogerida, baseada na autonomia e na abolição de toda forma de dominação: o Estado, o capitalismo e tudo aquilo que afeta o relacionamento entre os seres humanos e as relações destes com a natureza. A Revolução Integral se compromete com a ação consciente, pessoal e coletiva para a melhoria e recuperação dos valores e qualidades que nos permitem viver uma vida em comum. É baseada na construção de novas estruturas e formas de organização em todas as áreas da vida, para garantir a igualdade na tomada de decisões e a justiça no atendimento de nossas necessidades básicas e vitais. Elas incluem:

* Relações humanas equitativas, baseadas na liberdade e não na coerção
* Auto-organização e assembleias populares soberanas
* O povo, os Comuns
		* Recuperar a propriedade comum em benefício de todos, com posse e controle popular
		* Construir um sistema cooperativado público e autogerenciado, baseado na ajuda mútua
		* Libertar o acesso à informação e ao conhecimento
* Uma nova economia baseada na cooperação e proximidade
* Cooperação com a vida e a biosfera

<a class="block-link" href="http://integrarevolucio.net/en/integral-revolution/ideological-bases-of-the-call/">Saiba mais sobre os conceitos da Revolução Integral</a>



### 2) Colaboração P2P
* A colaboração P2P significa que todos assumem certos papéis e tarefas dentro da comunidade como um compromisso voluntário para o bem comum.
* Não se deve esperar uma reciprocidade direta pelos compromissos aceitos. No entanto, como cada participante tentará manter a reciprocidade e a solidariedade dentro dos Comuns, haverá oportunidades para receber algo de volta da coletividade.
* Nenhuma das dinâmicas de colaboração é hierárquica, portanto todos os envolvidos são considerados iguais no processo.
* Há muito mais o que aprender sobre os princípios P2P e suas várias áreas.

<a class="block-link" href="https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles">Veja mais informações interessantes aqui</a>



### 3) Ética Hacker
Hacking vai muito além da imagem que a mídia promove do cibercriminoso de moletom preto. Nós e a maioria das pessoas da área de tecnologia livre entendemos o hacking como uma atividade orientada por valores morais, de liberdade e melhoria para o bem. As principais diretrizes da ética hacker são:
* Toda a informação deve ser livre
* O acesso a computadores – e qualquer outro meio que seja capaz de ensinar algo sobre como o mundo funciona – deve ser ilimitado e total. Esse preceito sempre se refere ao imperativo “mãos-na-massa”!
* Desacredite a autoridade e promova a descentralização.
* Hackers devem ser julgados segundo seu hacking, e não segundo critérios sujeitos a vieses tais como graus acadêmicos, raça, cor, religião, posição ou idade.
* Você pode criar arte e beleza no computador
* Computadores podem mudar sua vida para melhor.
* Nunca vaze dados de outras pessoas.
* Disponibilize dados públicos, proteja dados privados.

<a class="block-link" href="https://ccc.de/en/hackerethik">Veja mais informações sobre a ética hacker</a>