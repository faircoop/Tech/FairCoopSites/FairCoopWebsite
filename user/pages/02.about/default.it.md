---
title: 'Chi siamo'
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

## La nostra missione
La comunità di FairCoop sta creando un sistema economico glocale, innovatore, dal basso verso l'alto, che promuove un modello alternativo e postcapitalista. La cooperazione, l'etica, la solidarietà e la trasparenza sono fattori chiave al fine di riuscire nell'intento di creare un'economia veramente giusta per tutti e tutte. Lo sviluppo e l'uso di strumenti digitali potenti e interconnessi (a livello globale) e di reti regionali (a livello locale) sono cruciali per il suo successo. Il nostro tempo e la nostra energia vengono usate costruttivamente in forma non violenta al fine di creare alternative reali, mettendo direttamente in pratica le teorie. Tutto ciò che viene costruito in questo processo è disponibile ed aperto per tutti e tutte.

## Visione
Siamo in un percorso di creazione di una rete globale di comunità locali autodirette e autodeterminate e di individui indipendenti dalle forze e dalle restrizioni centralizzate, spesso abusive. In questo modo, le persone recuperano il controllo delle proprie vite. Tutti e tutte i/le partecipanti definiscono collettivamente i principi e il contesto strutturale a nome del 99%. Avendo la base in una struttura decentralizzata, possono fiorire differenti modelli di società alternativa. I/le partecipanti possono definire le proprie necessità regionali specifiche in funzione delle realtà di provenienza. La libertà di scegliere un altro sistema, aprirà la porta alla possibilità di riuscire al cambiamento sociale a livello globale, conosciuto con il nome di rivoluzione integrale, la quale ha a che fare con tutti gli aspetti della vita come: l'educazione, la salute, l'energia, il diritto alla casa ed i trasporti. La cooperazione e il contributo di ogni partecipante a questa società glocale condurranno ad una nuova ricchezza mondiale, accessibile a tutti e condivisa. Questa dinamica ridurrà gradualmente la diseguaglianza economica e sociale globale.

## I principi
Le azioni della comunità FairCoop si basano su tre rami principali di principi già esistenti: **la rivoluzione integrale, i principi della collaborazione peer to peer, l'etica hacker.**

### 1) La rivoluzione integrale:
E' un processo d'importanza storica che si pone l'obiettivo di costruire una nuova società autogestita, basata sull'autonomia e sull'abolizione di ogni forma di dominio: lo stato, il capitalismo e tutte le altre forme che influenzano le relazioni umane e le relazioni con il contesto naturale. Si impegna a porre in atto azioni coscienti, personali e collettive, al fine di migliorare e recuperare i valori e le qualità che ci permettono di vivere una vita in comune. Allo stesso tempo si impegna a costruire nuove strutture e forme di organizzazione in ogni area della vita in modo da garantire eguaglianza nei processi decisionali e l'equità nella soddisfazione delle nostre necessità  elementari e di vita.

Questo include:
* Relazioni umane eque e non coercitive basate sulla libertà.
* Autorganizzazione e assemblee popolari sovrane
* il bene comune, il pubblico
    * Recuperare la proprietà comune in favore di tutti e tutte, con la proprietà e il controllo popolare
    * Costruire un sistema cooperativo che sia pubblico e autogestito, basato sul mutuo aiuto
    * Liberare l'accesso all'informazione ed alla conoscenza
* una nuova economia basata sulla cooperazione e la prossimità.
* Cooperare con la vita e la biosfera.

<a class="block-link" href="http://integrarevolucio.net/it/rivoluzione-integrale/basi-ideologiche-dellinvito/">Ottieni Più Informazioni Sulla Rivoluzione Integrale</a>


### 2) I principi della collaborazione P2P (peer to peer)
* Collaborazione P2P significa che tutti assumono determinati ruoli e compiti all'interno di una comunità come impegno volontario per il bene comune.
* Una diretta reciprocità non dovrebbe essere prevista attraverso gli impegni accettati. Tuttavia, poiché ciascuno dei partecipanti cercherà di mantenere la mutualità e la solidarietà all'interno dei Commons, ci saranno opportunità per ottenere qualcosa di nuovo dall’insieme.
* Tutte le dinamiche di collaborazione non sono gerarchiche, quindi tutti i soggetti coinvolti sono considerati come una voce uguale nel processo.
* 
<a class="block-link" href="https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles">Ulteriori Informazioni</a>



### 3) Etica Hacker
L' Hakeraggio è tutt'altro, rispetto a ciò che l' mmaginario comune dipinge come cyberdelinquenti in felpa e cappuccio nero. Il modo in cui noi e la maggior parte delle persone che lavora nel campo della tecnologia libera, intendiamo l'Hackeraggio, è in riferimento a valori morali di libertà e di miglioramento del bene comune. I punti chiave dell'Etica Hacker sono:
* Tutta l'informazione deve essere libera
* L'accesso ai computer e a qualsiasi mezzo che può insegnarti qualcosa su come funziona il mondo deve essere illimitato e totale. Dai sempre maggiore importanza all'imperativo pratico!
* La diffidenza verso le autorità promuove la decentralizzazione.
* Gli hackers devono essere giudicati per le loro attività di pirateria e non secondo criteri quali titolo di studio, razza, età, sesso o posizione sociale
* Puoi creare arte e bellezza in un computer
* Non diffondere i dati di altre persone.
* Rendi pubbliche le informazioni disponibili, proteggi i dati privati.

<a class="block-link" href="https://ccc.de/en/hackerethik">Ulteriori Informazioni Sull'etica Hacker Qui</a>