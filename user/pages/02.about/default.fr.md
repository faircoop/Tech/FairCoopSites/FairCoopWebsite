---
title: 'À propos de FairCoop'
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

## Mission
Notre mission consiste à créer un nouveau système économique global post capitaliste et frayer un chemin vers un changement collectif pour une vie meilleure en commun. Coopération , éthique, solidarité et transparence sont les valeurs clés pour une vraie justice. Le développement et l'utilisation d'un ensemble d'outils digitaux interconnectés au niveau global et d'un reseau de points régionaux sont les élements essentiels à notre réussite. Nous utilisons notre énergie d'une manière constructive et collaborative afin de créer une réelle alternative en passant de la théorie à la pratique.

## Vision
Nous imaginons un réseau global de communautés auto organisées, indépendantes des forces centralisées et bien souvent abusives. Nos principes généraux et notre structure sont définis collectivement au niveau global et dans le même temps, sont implementés au niveau local avec l'adaptation des spécificités régionales.De cette manière, les gens peuvent reprendre le contrôle de leur vie en ayant le choix entre plusieurs modèles de sociétés basés sur une structure décentralisée. Notre travail collaboratif crée un écosystème indépendant qui tend à réduire les inégalités sociales. Partant de ce système, on devrait pouvoir permettre un changement global impactant tous les aspects de nos vies comme l'éducation, la sécurité sociale, l'énergie ou encore les transports. Nous allons redécouvrir des relations sans exploitation avec nos semblables et avec la nature.

## Principes
Les actions de la communauté FairCoop sont basées sur trois branches majeures de principes existants: **Révolution intégrale - Collaboration P2P - Ethique Hackers**

### 1) Révolution intégrale
* Des relations humaines équitables basées sur la liberté et la non-coercition.
* L'auto-organisation et les assemblées populaires souveraines.
* Les communs, le public
    * Récupérer la propriété commune au profit de tous, avec possession et contrôle populaire
    * Construire un système coopératif public et autogéré, basé sur l'entraide
    * Libérer l'accès à l'information et au savoir
* Une nouvelle économie basée sur la coopération et la proximité. *Coopération avec la vie et la biosphère.

<a class="block-link" href="http://integrarevolucio.net/fr/revolution-integrale/bases-ideologiques-de-lappel/">En Savoir Plus Sur Les Concepts De La Révolution Intégrale</a>


### 2) Collaboration P2P
* La collaboration P2P signifie que tout le monde assume certains rôles et tâches au sein de la communauté en tant qu'engagement volontaire pour le bien commun.
* Une réciprocité directe via les engagements acceptés ne devrait pas être attendue. Cependant, au fur et à mesure que chacun des participants s'efforcera de maintenir la réciprocité et la solidarité au sein de la Chambre des communes, il y aura des occasions de retirer quelque chose de l'ensemble.
* Toutes les dynamiques de collaboration sont non hiérarchiques, de sorte que toute personne impliquée est considérée comme ayant une voix égale dans le processus.
* Il y a beaucoup plus à apprendre sur les principes P2P et ses différents sous-domaines.

<a class="block-link" href="https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles">Vous Trouverez D'autres Informations Utiles Ici</a>



### 3) L'éthique hacker
Le piratage est bien plus que l'image sociale des cybercriminels avec des sweats à capuche noirs. La façon dont nous, et la plupart des gens de technologie libre, comprenons l'action du piratage fait référence aux valeurs morales, à la liberté et à l'amélioration pour le bien. Les clés de l'éthique des hackers sont:
* Toutes les informations devraient être libres
* L'accès aux ordinateurs - et tout ce qui pourrait vous apprendre quelque chose sur le fonctionnement du monde - devrait être illimité et total. Toujours céder à l'impératif pratique !
* Autorité de méfiance - promouvoir la décentralisation
* Les pirates doivent être jugés en fonction de leur piratage, et non de critères tels que les diplômes, l'âge, la race, le sexe ou la position
* Vous pouvez créer de l'art sur un ordinateur
* Les ordinateurs peuvent changer votre vie pour le meilleur
* Ne pas jeter les données d'autres personnes
* Rendre les données publiques disponibles, protéger les données privées


<a class="block-link" href="https://ccc.de/en/hackerethik">Plus D'informations Sur L'éthique Des Hackers Ici</a>