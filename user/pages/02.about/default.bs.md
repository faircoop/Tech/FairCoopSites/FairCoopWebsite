---
title: 'O nama'
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

## Misija
Naša misija je stvoriti inovativni glokalni (globalni/lokalni) ekonomski sistem od dna do vrha u korist alternativnog i post-kapitalističkog modela kako bi popločali put kolektivnoj promjeni prema životu u zajedništvu. Kooperacija, etika, solidarnost i transparentnost su za nas ključni faktori da stvorimo istinski pravičan sistem za sve. Razvoj i upotreba moćnih međusobno povezanih digitalnih alata (globalno) i regionalnih mreža (lokalno) su krucijalne za uspjeh. Mi koristimo naše vrijeme i energiju konstruktivno i na zajednički način da stvorimo stvarnu alternativu provodeći teoriju u praksu.

## Vizija
Zamišljamo globalnu mrežu samo-organizovanih i samo-osnaženih lokalnih zajednica i pojedinaca, nezavisnih od napasnih centralizovanih snaga i restrikcija. Naši globalni principi i okvir su definisani zajednički, u ime 99% nas, i u isto vrijeme, realizirane na lokalnom nivou u skladu sa određenim regionalnim potrebama i okolnostima. Na ovaj način, ljudi preuzimaju kontrolu nad svojim životima, imajući slobodu da izaberu razne alternative društvenih modela temeljenih na decentralizovanoj organizacijskoj strukturi. Naše kooperativno djelovanje je kreiranje nezavisnog ekosistema, čiji je cilj da se smanji globalna ekonomska i socijalna nejednakost, koja će dovesti do novog globalnog bogatstva dostupnog svima u obliku zajedničarstva/zajedništva. Kao domino-efekt, okvir koji mijenja igru ovog ekonomskog sistema će otvoriti vrata za globalnu smjenu, utičući na sve aspekte života kao što su edukacija, zdravstvena zaštita, energija, stanovanje, transport i stvoriti će prilike da se ponovo otkriju zdravi i neiskorištavajući odnosi sa našim bližnjim i prirodom.

## Prinicipi
Djelovanja FairCoop zajednice utemeljena su na tri glavne grane već postojećih principa: **Integralna Revolucija, P2P Kolaboracija, Haker Etika**

### 1) Integralna Revolucija

Ovo je proces od historijske važnosti, za uspostavljanje novog samo-upravljajućeg društva, temeljeno na autonomiji i napuštanju svih oblika dominacije: države, kapitalizma i svih drugih oblika koji utiču na odnose kako među ljudima tako i sa prirodnim okruženjem. Posvećena je svjesnom djelovanju, pojedinačnom i kolektivnom, radi poboljšanja i povrata vrijednosti i kvaliteta koje nam omogućuju život u zajedništvu. Utemeljena na uspostavljanju novih struktura i oblika organizacije u svim područjima života da bi se osigurala jednakost u donošenju odluka i pravičnost, u ostvarenju naših osnovnih i vitalnih potreba. Ovo uključuje:

* Pravične ljudske odnose temeljene na slobodi i izostanku prinude.
* Samo-organizacija i suverenost javnih skupština.
* Zajedničari, javnost
	* Ponovno sticanje zajedničkog vlasništva u korist svih sa javnim posjedom i kontrolom
	* Izgradnja kooperativnog sistema koji je javan i samo-upravljajući, temeljen na uzajamnoj pomoći
	* Osloboditi pristup informacijama i znanju
* Nova ekonomija temeljena na kooperaciji i neposrednosti.
* Učinite javne podatke dostupnim, zaštitite privatne podatke

[Saznajte Više O Konceptima Integralne Revolucije Ovdje](http://integrarevolucio.net/en/integral-revolution/ideological-bases-of-the-call/)


### 2) P2P Kolaboracija

* P2P kolaboracija znači da svako preuzima određene uloge i zadatke unutar zajednice kao volontersku obavezu za zajedničko dobro.
* Ne očekuje se direktni reprocitet za preuzete obaveze. Međutim, kako svaki učesnik pokušava da podrži uzajamnost i solidarnost unutar Zajedničara, doći će do prilike da se dobije nešto zauzvrat.

* Sve dinamike kolaboracije nisu hijerarhijske, tako da svi koji su uključeni imaju jednako pravo glasa tokom procesa.
* Sve dinamike kolaboracije nisu hijerarhijske, tako da svi koji su uključeni imaju jednako pravo glasa tokom procesa.

[Više Vrijednih Informacija Možete Pronaći Ovdje](https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles)


### 3) Haker Etika

Hakiranje je više od socijalne slike cyber kriminalaca sa crnim kapuljačama. Način na koji mi, kao i većina slobodnih tech ljudi razumijemo djelovanje hakiranja odnosi se na moralne vrijednosti, slobodu i poboljšanje općeg dobra. Ključne haker etike su:

* Sve informacije trebaju biti besplatne
* Pristup kompjuterima - i sve ono što može poučiti o načinu kako svijet funkcionira- treba biti neograničeno i potpuno. Uvijek se pridržavaj imperativa praktičnosti!
* Ne vjerovati vlastima - promicati decentralizaciju
* Hakere treba cijeniti po hakiranju a ne kriterijima kao što su diplome, godine, rasa, spol ili pozicija
* Kompjuterom se može stvarati umjetnost i ljepota
* Kompjuteri mogu promijeniti vaš život na bolje
* Ne razbacujte podatke drugih ljudi
* Učinite javne podatke dostupnim, zaštiite privatne podatke.

[Viđe Informacija O Haker Etici Ovdje](https://ccc.de/en/hackerethik)
