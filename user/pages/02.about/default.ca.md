---
title: 'Sobre nosaltres'
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

## Missió
La nostra missió és crear un innovador sistema econòmic glocal de baix cap amunt, un model alternatiu i postcapitalista per a preparar el camí del canvi col·lectiu cap a una vida en comú. La cooperació, l'ètica, la solidaritat i la transparència són factors clau a l'hora de crear un vertader sistema de justícia per a totes les persones. El desenvolupament i l'ús de potents eines digitals interconnectades (globals) i de xarxes regionals (locals) és crucial per al nostre èxit. Estem utilitzant el nostre temps i la nostra energia de manera constructiva i col·laborativa per crear alternatives reals, posant en pràctica les teories.

## Visió
Visualitzem una xarxa global de comunitats locals i individus autoorganitzats i autoempoderats, independent de restriccions i poders centralitzats abusius. Els nostres principis i marcs globals es decideixen col·lectivament, en nom del 99% i, al mateix temps, es concreten localment d'acord amb les necessitats i circumstàncies específiques de cada regió. D'aquesta manera, les persones recuperen el control de les seues pròpies vides, tenint la llibertat d'escollir diversos models socials alternatius basats en una estructura d'organització descentralitzada. El nostre treball cooperatiu està creant un ecosistema independent que té com a objectiu reduir la desigualtat econòmica i social mundial, portant-nos cap a una nova riquesa global accessible a totes les persones en forma de béns comuns. Igual que un efecte dominó, aquesta dinàmica que canvia les regles del joc del sistema econòmic obrirà les portes a una transformació global que tindrà un impacte en tots els aspectes de la vida, com l'educació, l'assistència sanitària, l'energia, l'habitatge, el transport... i crearà oportunitats per a redescobrirformes saludables i no explotadores de relacionar-nos entre nosaltres i amb la Naturalesa.

## Principis
Les accions de la comunitat de FairCoop es basen en tres branques principals de principis ja existents: **Revolució Integral, el model col·laboratiu entre pars P2P, l'Ètica Hacker**

### 1) Revolució Integral
És un procés de rellevància històrica per a la construcció d'una nova societat autogestionada, basada en l'autonomia i l'abolició de totes les formes de dominació: l'estat, el capitalisme iqualsevol altra que afecte les relacions entre éssers humans i el medi ambient. Implica l'acció conscient, personal i col·lectiva, per a la millora i recuperació dels valors i qualitats que ens permeten viure una vida en comú. Es basa en la construcció de noves estructures i formes d'organització en tots els àmbits de la vida per assegurar la igualtat en la presa de decisions i l'equitat per a satisfer les nostres necessitats bàsiques i vitals. Aquestes inclouen:
* Relacions humanes equitatives basades en la llibertat i la no-coerció.
* Autoorganització i assemblees populars sobiranes.
* Els béns comuns, allò públic
	* Recuperar la propietat comuna en benefici de totes, mitjançant la possessió i el control popular.
	* Construir un sistema cooperatiu que siga públic i autogestionat, basat en el suport mutu.
	* Alliberar l'accés a la informació i al coneixement.
* Una nova economia basada en la cooperació i la proximitat.
* Cooperació amb la vida i la biosfera.

<a class="block-link" href="http://integrarevolucio.net/ca/revolucio-integral/bases/">Descobreix Més Informació Sobre Els Conceptes De La Revolució Integral Aquí</a>


### 2) Col·laboració entre pars - P2P
* La col·laboració P2P significa que totes les persones assumeixen determinats rols i tasques dins de la comunitat com un compromís voluntari pel bé comú.
* No s'hauria d'esperar una reciprocitat directa pel fet d’haver acceptat certs compromisos. Tanmateix, atés que cadascun dels participants intentarà mantenir la mutualitat i la solidaritat amb els iguals, hi haurà oportunitats d'obtenir alguna cosa des d'alguna part del conjunt.
* Totes les dinàmiques de col·laboració són no-jeràrquiques, de manera que es considera que totes les persones involucrades tenen la mateixa veu en el procés.
* Hi ha molt més per conéixer sobre els principis P2P i les seues diferents subàrees.

<a class="block-link" href="https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles">Ací Trobaràs Més Informació Interessant</a>



### 3) Ètica Hacker
El hackeig és més que l’estereotip del ciberdelinqüent tot vestit de negre i amb caputxa. La forma en què nosaltres i la majoria de gent del món de les tecnologies lliures entenem l'acció delhackeig es refereix als valors morals, la llibertat i la millora de l’objecte hackejat. Les claus de l'ètica hacker són:
* Tota la informació ha de ser lliure.
* L'accés als ordinadors -i tot allò que puga ensenyar alguna cosa sobre el funcionament del món- hauria de ser il·limitat i total.
* Desconfia de l’autoritat: promou la descentralització.
* Els hackers haurien de ser jutjats pel seu hackeig, no per criteris com ara els seus títols, l'edat, la raça, el sexe o la posició.
* Es pot crear art i bellesa en un ordinador.
* Els ordinadors poden canviar a millor la teua vida.
* No esborres les dades d'altres persones.
* Feu que la informació pública estiga disponible, protegiu les dades privades.


<a class="block-link" href="https://ccc.de/en/hackerethik">Més Informació Sobre L'ètica Hacker Aquí (En Anglés)</a>