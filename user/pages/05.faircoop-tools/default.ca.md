---
title: 'Eines FairCoop'
media_order: tools.jpg
menu: Eines
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin</h2>
<p>FairCoin és una moneda digital basada en una xarxa descentralitzada entre pars (P2P) que està protegida amb un fort xifratge. És una poderosa eina per a canviar el món, permetent-nosindependitzar-nos dels bancs centrals, les institucions financeres i els governs. Per tant, és una eina clau en el nou sistema econòmic. Comparada amb altres criptomonedes, com Bitcoin, el seu valor és estable i és sotmés a ajustos regulars mitjançant el consens de la comunitat.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Anar a la web de FairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>FairPay és un sistema de targetes contactless (NFC) per a comerciants i clients per a fer que els pagaments de FairCoin siguen tan fàcils com fer-ho amb una targeta de crèdit. L'adhesiu NFC conté un identificador únic de la usuària de la targeta que es comunica directament amb el panell de l'app a la qual està vinculada. A través d'aquest panell, els comerciants tenen la possibilitat fins i tot de canviar directament aquests FairCoins que reben a euros, en cas necessari.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Anar a la web de FairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>Existeixen tres principals FairFunds: el Fons del Sud Global, el Fons del Procomú i el Fons d'Infraestructura Tecnològica. Els FairCoins acumulats en aquests fons tenen com a objectiu finançar futurs projectes que estiguen en harmonia amb els objectius de cada fons i donar suport, a major escala, l'economia circular de FairCoin. A causa del creixement estable del valor de FairCoin com a moneda, el poder econòmic dels fons també creix constantment. D'aquesta manera, som cada vegada més capaços de finançar més i majors projectes que estiguen orientatscap als nostres objectius comuns.</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Anar a la web de FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org és un mapa i directori virtual d'espais físics de tot tipus que accepten FairCoins com a mètode de pagament. Això és especialment útil perquè les usuàries troben fàcilment llocs on gastar FairCoins en productes i serveis del món real, al mateix temps que recolzen l'economia circular.</p>
<a class="btn btn-tools" href="https://use.fair-coin.org/" target="blank">Anar a useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin.net és una manera fàcil de comprar FairCoins a través de diferents monedes nacionals com EUR, USD, SYP, CNY, RUB, GBP, INR, NGN, BRL, MXN, etc. amb mètodes de pagament corrents com transferències bancàries, targetes de crèdit o de dèbit, o fins i tot, utilitzant efectiu a través dels llocs físics (nodes locals). Aquest preu oficial de FairCoin és estable, definit per l’assemblea i ajustat sovint en funció de diversos paràmetres de preu.</p>

<a class="btn btn-tools"href="https://getfaircoin.net/" target="blank">Anar a getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>FairSaving és un servei d'estalvis en FairCoins que FairCoop proporciona a través de la plataforma getFairCoin. S'ofereix a aquells que volen utilitzar la nostra infraestructura per a mantenir els seus Faircoins amb les adequades mesures de seguretat. És recomanable per a aquelles persones sense ordinador propi o a aquelles que no confien prou en les seues habilitats informàtiques per a assumir la responsabilitat de tindre cura del seu moneder virtual.</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Anar a FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>L'OCP és l'eina organitzativa clau que oferim a les persones, col·lectius i coordinadors de projectes dins de FreedomCoop, de Bank of the Commons i de molts altres projectes de cooperacióque vindran en el futur. També es pot utilitzar, per exemple, per a gestionar projectes col·lectius oberts i els seus equips de treball mitjançant la creació de grups de tasques, comptabilitzar el temps dedicat per cada membre per a distribuir els ingressos o gestionar una cartera en línia de FairCoin acceptant i fent pagaments..</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Anar a OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>FreedomCoop és una Societat Cooperativa Europea (SCE) que ofereix un conjunt d'eines per a l'autogestió, l'autoocupació, l'autonomia econòmica i la desobediència financera. Els membres autoocupats poden, per exemple, lliurar les seues factures a FreedomCoop i rebre els seus diners en el seu compte virtual individual. No es requereixen identificacions personals ni altres documents. Totes les quotes de membres es paguen en FairCoins. Els beneficis de FreedomCoop es redistribueixen als nodes locals i, per tant, són crucials per a finançar i dinamitzar la seua creació.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Anar a FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>FairMarket és el mercat en línia de FairCoop, on totes les participants poden oferir els seus productes i serveis a la comunitat. Interconnecta a les persones en sintonia amb els mercats alternatius, les monedes socials, el comerç just i els principis generals de FairCoop. Tots els productes i serveis es paguen amb FairCoin. Això augmenta la usabilitat/circulació de la moneda i ajuda a construir alternatives per a una economia més equitativa.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Anar a FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>Bank of the Commons o Banc del Procomú (BotC) és una iniciativa cooperativa oberta que té com a objectiu transformar la banca, els pagaments i les monedes per a donar suport a l'economia dels moviments cooperatius i socials tant en l'àmbit global com local. BotC ha adoptat FairCoin com una moneda social global estratègica i la seua tecnologia de cadena de blocscom a part del desenvolupament i l'adopció d'estructures financeres descentralitzades per als bens comuns. BotC està indirectament lligat al conjunt d'eines de FairCoop per a ampliar el seucamp potencial d'usuàries tant com siga possible.</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Anar a BankoftheCommons</a>

</div>

</div>
