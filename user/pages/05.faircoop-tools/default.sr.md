---
title: 'ФаирЦооп Тоолс'
media_order: tools.jpg
menu: Алати
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin</h2>
<p>FairCoin je digitalna valuta zasnovana na decentralizovanoj, enkripcijom zaštićenoj mreži 'lice-u-lice' (P2P). To je moćno sredstvo za promenu sveta, nezavisno od centralnih banaka, finansijskih institucija i vlada. Zbog toga je i ključno za novi ekonomski sistem. U poređenju sa drugim kriptovalutama, kao što je bitcoin, njegova vrednost je stabilna i periodično se usaglašava u zajednici, putem konsenzusa.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Идите FairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>FairPay je sistem NFC kartica za kupce i trgovce kojima se plaćanje FairCoin-om obavlja lako, kao i sa kreditnom karticom. NFC nalepnica sadrži jedinstvenu identifikaciju (ID) koja prepoznaje korisnika kartice i direktno komunicira sa povezanom aplikacijom App Panel. Kroz ovaj panel, trgovci čak imaju mogućnost da menjaju FairCoin-e direktno u evre, ukoliko im je to potrebno.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Идите FairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>Trenutno postoje tri glavna fonda FairCoop-a: Fond Globalnog Juga, Fond opštih dobara i Fond za tehnološku infrastrukturu. Sačuvani FairCoin-i u ovim fondovima imaju za cilj da finansiraju buduće projekte koji su u harmoniji sa ciljem svakog fonda, kao i da podrže i osnaže cirkularnu ekonomiju. U skladu sa stabilnim rastom vrednosti FairCoin-a kao valute, ekonomska snaga fondova je u stalnom porastu. Zbog toga smo sve više u mogućnosti da finasiramo veće projekte usmerene ka našim opštim ciljevima.</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Идите FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org je virtualna mapa i imenik različitih fizičkih mesta koja primaju FairCoin kao metod plaćanja. Ovo je posebno korisno za lakše pronalaženje lokacije za trošenje FairCoin-a na svetske proizvode i usluge, a istovremeno i za podršku cirkularnoj ekonomiji.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Идите useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin.net je jednostavan način da kupite FairCoin za različite nacionalne valute kao što su EUR, USD, SYP, CNY, RUB, GBP, INR, NGN, BRL, MXN, itd. plaćanjem putem doznake, kreditne ili debitne kartice, ili čak upotrebom keša, na fizičkim mestima (PoE lokalne petlje). Zvanična cena FairCoin-a je stabilna, definisana konsenzusom na globalnim skupovima i periodično usklađivana u skladu sa različitim vrednosnim parametrima.</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Идите getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>FairSaving je FairCoop-ova usluga za štednju, koja se omogućava kroz getFairCoin platformu. Ponuđena je onima koji žele da koriste našu infrastrukturu, kako bi čuvali svoje FairCoin-e pod prikladnim bezbednostima merama. Preporučuje se ljudima koji nemaju svoj kompjuter, ili onima koji nemaju poverenje u svoje kompjuterske veštine toliko da bi preuzeli odgovornost za upotrebu i čuvanje svojih elektronskih novčanika.</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Идите FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>OCP je ključni organizacioni instrument koji nudimo pojedincima, kolektivima i koordinatorima projekata unutar FreedomCoop-a, Bank of the Commons i mnogih drugih u budućnosti. Može da se upotrebljava, na primer, za upravljanje otvorenim zajedničkim projektima i timskim radom, kroz postavljanje zadataka, računanje utrošenog vremena od strane svakog člana, što je osnova za distribuciju prihoda, ili za upravljanje FairCoin novčanikom na internetu, za prihvatanje uplata i plaćanje.</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Идите OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>FreedomCoop je Evropsko kooperativno društvo (SCE) koje nudi set instrumenta za samoupravljanje, samozapošljavanje, ekonomsku autonomiju i finansijsku neposlušnost. Samozaposleni članovi mogu, na primer, da predaju svoje fakture FreedomCoop-u i da primaju uplate na svojim individualnim računima. Lična identifikacija i drugi dokumenti nisu neophodni. Sve naknade za članstvo se mogu platiti FairCoin-om. Profit od FreedomCoop-a se redistribuira lokalnim petljama i zbog toga je krucijalan za njihovo finansiranje i dinamiku rada.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Идите FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>FairMarket je FairCoop-ovo internet tržište, na kojem svi učesnici mogu da nude svoje proizvode i usluge. Ono međusobno povezuje pojedince koji su usaglašeni sa alternativnim tržištima, društvenim valutama, pravičnom trgovinom i uopšte sa principima FairCoop-a. Svi proizvodi i usluge se plaćaju FairCoin-om. Na ovaj način se povećava vidljivost/ cirkulacija valute i pomaže se izgradnja alternative za pravičniju ekonomiju.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Идите FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>Bank of the Commons je otvorena koooperativna inicijativa čiji je cilj transformacija bankarstva, plaćanja i valuta, radi podrške ekonomiji koja se koristi u kooperativnim i društvenim pokretima na globalnom i lokalnom nivou. BotC je usvojila FairCoin kao stratešku, globalnu, društvenu valutu i njegovu blokčejn tehnologiju, kao deo razvitka i usvajanja decentralizovane finansijske strukture za opšta dobra. BotC je indirektno povezana sa FairCoop-ovim setom sredstava za rad, kako bi se što više proširilo polje korisnika.</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Идите BankoftheCommons</a>

</div>

</div>