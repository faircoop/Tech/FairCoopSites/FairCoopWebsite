---
title: Outils
media_order: tools.jpg
menu: Outils
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin</h2>
<p>FairCoin est une monnaie numérique basée sur un réseau peer-to-peer décentralisé protégé par un cryptage fort. C'est un outil puissant pour changer le monde en étant indépendant des banques centrales, des institutions financières et des gouvernements. C'est donc un outil clé dans le nouveau système économique. Comparé à d'autres crypto monnaies, comme le Bitcoin, sa valeur est stable et fréquemment ajustée via la communauté par consensus.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Lien vers FairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>FairPay est un système de cartes NFC permettant aux clients et aux commerçants de faire des paiements FairCoin aussi facilement qu'avec une carte de crédit. L'autocollant NFC contient un identifiant unique qui identifie l'utilisateur de la carte et communique directement avec l'application liée. Grâce à cette application, les commenrçants ont la possibilité d'échanger ces FairCoins directement en Euros si nécessaire.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Lien vers FairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>Il existe trois grands fonds FairFund, le Global South Fund, le Commons Fund et le Technology Infrastructure Fund. Les FairCoins stockés dans ces fonds ont pour but de financer de futurs projets qui sont en harmonie avec l'objectif de chaque fonds et de soutenir la plus grande économie circulaire FairCoin. En raison de la croissance stable de la valeur de FairCoin en tant que devise, la puissance économique des fonds est également en croissance constante. Ainsi, nous sommes de plus en plus en mesure de financer des projets plus nombreux et de plus grande envergure pour atteindre nos objectifs communs.</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Lien vers FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org est une carte virtuelle et un répertoire des lieux physiques qui acceptent les FairCoins comme méthode de paiement. Ceci est utile pour les utilisateurs, pour  trouver facilement des emplacements où dépenser leurs FairCoins dans la vie de tous les jours et pour soutenir l'économie circulaire en même temps.</p>
<a class="btn btn-tools"href="https://use.fair-coin.org/" target="blank">Lien vers useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin.net est un moyen facile pour acheter des FairCoins via différentes devises nationales comme EUR, USD, SYP, CNY, RUB, GBP, INR, NGN, BRL, MXN, etc. avec des méthodes de paiement couramment utilisées comme le virement bancaire, la carte de crédit ou de débit ou même en utilisant de l'argent cash via les points d'échange physiques (nœuds locaux). La valeur du FairCoin officiel est stable, défini par les assemblées et fréquemment ajusté au regard de divers paramètres de prix</p>

<a class="btn btn-tools"href="https://getfaircoin.net/" target="blank">Lien vers getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>FairSaving est un service d'épargne FairCoin que FairCoop fournit via la plateforme getFairCoin. Il est offert à ceux qui veulent utiliser notre infrastructure pour garder leurs Faircoins en auyant une garantie de sécurité appropriée. Ce service est recommandé aux personnes qui ne possédent pas d'ordinateur personnel ou à ceux qui pensent ne pas avoir suffisamment de compétences informatiques pour prendre en charge leur portefeuille Coop.</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Lien vers FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>L'OCP (plateforme collaborative ouverte) est l'outil organisationnel clé que nous offrons aux particuliers, aux collectivitéss et aux coordinateurs de projet au sein de Freedom Coop, de la Banque des Commons et autres projets de coopération dans le futur. Il peut également être utilisé, par exemple, pour gérer des projets collectifs ouverts et le travail d'équipe en créant des groupes de travail, pour comptabiliser les activités de chaque membre et permetrre une distribution des revenus ou gérer un portefeuille en ligne FairCoin pour accepter et effectuer des paiements.</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Lien vers OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>FreedomCoop est une société coopérative européenne (SCE) qui offre une boîte à outils pour l'autogestion, le travail indépendant, l'autonomie économique et la désobéissance financière. Les membres indépendants peuvent, par exemple, remettre leurs factures à FreedomCoop et recevoir leur argent dans leur compte virtuel individuel. Les identifiants personnels ou autres documents ne sont pas requis. Tous les frais d'adhésion sont payables en FairCoin. Les bénéfices de FreedomCoop sont redistribués aux nœuds locaux et donc cruciaux pour financer et dynamiser leur création.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Lien vers FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>FairMarket est le marché en ligne de FairCoop, où tous les participants peuvent offrir leurs produits et services à la communauté. Il interconnecte des individus qui sont en accord avec les marchés alternatifs, les monnaies sociales, le commerce équitable et les principes généraux de FairCoop. Tous les produits et services sont payés en FairCoins. Cela augmente la facilité d'utilisation / circulation de la monnaie et aide à construire des alternatives pour une économie plus équitable.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Lien vers FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>La Banque des Commons (BotC) est une initiative de coopérative ouverte dont l'objectif est de transformer la banque, les paiements et les devises afin de soutenir l'économie utilisée dans les mouvements coopératifs et sociaux au niveau mondial et local. BotC a adopté le FairCoin comme monnaie sociale mondiale stratégique et sa technologie blockchain dans le cadre du développement et de l'adoption de structures financières décentralisées pour les communes. BotC est indirectement lié à la boîte à outils de FairCoop afin d'élargir le plus possible le champ potentiel des utilisateurs.</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Lien vers BankoftheCommons</a>

</div>

</div>
