---
title: 'Strumenti FairCoop'
media_order: tools.jpg
menu: Strumenti
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin</h2>
<p>FairCoin è una moneta digitale basata su una rete peer-to-peer decentralizata e protetta da una forte criptazione. E' uno strumento importante per cambiare il mondo permettendo alle persone di svincolarsi dalle banche centrali e dalle istituzioni finanziarie. Per questo motivo, è uno strumento chiave del nuovo sistema economico. A confronto con altre criptomonete, come BitCoin, il suo valore è stabile e si adatta costantemente attraverso il consenso della comunità. FairCoin combina giustizia economica e consenso umano.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Vai a FairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>FairPay è un sistema di carte NFC per clienti e commercianti pensata per rendere i pagamenti in FairCoin semplici come quelli realizzati attraverso carta di credito. La carta NFC contiene un ID unico, che identifica l'utente della carta e si collega direttamente ad un Pannello di controllo connesso alla carta stessa, anche gli esercenti hanno la possibilità di cambiare i FairCoins in euro, se necessario.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Vai aFairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>Ci sono tre principali Fondi (chiamati FairFunds), il Global South Fund (fondo per il Sud Globale), il Commons Fund ( Fondo per i Beni Comuni) e il Technology Infrastructure Fund (Fondo per le Infrastrutture Tecnologiche). I FairCoins accumulati in questi fondi hanno l'obiettivo di: finanziare in futuro progetti che sono in armonia con l'obiettivo di ciascun fondo e stimolare lo sviluppo dell'economia circolare in FairCoin. Data la stabile crescita del valore del FairCoin come moneta, il potere economico dei fondi è in continua crescita. In questo modo siamo sempre più in grado di finanziare un maggior numero si progetti finalizzati al raggiungimento dei nostri obiettivi comuni.</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Vai a FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org è una mappa virtuale dei luoghi fisici in cui sono presenti attività (di qualsiasi tipo) che accettano FairCoins come metodo di pagamento. Questo strumento è particolarmente utile per gli utenti al fine di trovare facilmentei luoghi in cui spendere FairCoins comprando prodotti e servizi del mondo reale sostenendo in questo modo l'economia circolare in Faircoin.</p>
<a class="btn btn-tools" href="https://use.fair-coin.org/" target="blank">Vai a useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin.net è un modo semplice per acquistare FairCoin utilizzando diverse valute nazionali come EUR, USD, SYP, CNY, STROFINARE, GBP, INR, NGN, BRL, MXN, ecc tramite metodi di pagamento comunemente utilizzati come bonifico bancario, carta di credito o carta di debito o anche utilizzando contanti, attraverso luoghi fisici (Nodi Locali). Il prezzo ufficiale del FairCoin è stabile perchè definito dall'assemblea la quale ne cambia il valore spesso in funzione di diversi parametri di prezzo.</p>

<a class="btn btn-tools"href="https://getfaircoin.net/" target="blank">Vai a getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>FairSaving è un servizio di risparmio in FairCoin fornito da FairCoop attraverso la piattaforma di getFairCoin . Esso è offerto a coloro che vogliono utilizzare la nostra infrastruttura per mantenere i propri risparmi in Faircoins adoperando le più appropriate misure di sicurezza. È consigliato a persone senza computer o a coloro che non si sentono abbastanza sicuri con il computer da assumersi la responsabilità della cura del proprio portafoglio.</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Vai a FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>OCP è lo strumento organizzativo chiave che offriamo a singoli, collettivi e coordinatori di progetto all'interno di FreedomCoop, Bank of the Commons e, in futuro, a molti altri progetti di cooperazione. Può essere utilizzato ad esempio, per gestire l'apertura di progetti collettivi e per l'organizzazione del lavoro in team tramite l'impostazione di task forces, per il conteggio delle ore di lavoro di ogni membro come base per la ripartizione delle retribuzioni o, ancora, per la gestione di un portafoglio online in FairCoin utile per fare e ricevere pagamenti.</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Vai a OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>FreedomCoop è una Società Cooperativa Europea (SCE), che offre un kit di strumenti per l'auto-gestione, l'auto-occupazione, l'autonomia economica e la disobbedienza finanziaria. I lavoratori autonomi membri della cooperativa possono, per esempio, emettere fatture con FreedomCoop e ricevere i loro soldi nel proprio conto virtuale. ID personali o altri documenti non sono richiesti. Tutte le quote di adesione sono pagabili in FairCoin. I profitti di FreedomCoop sono ridistribuiti ai Nodi Locali; Freedom Coop è, quindi, cruciale per finanziare e dinamizzare la loro creazione.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Vai a FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>FairMarket è il mercato online di FairCoop dove tutti i partecipanti possono offrire i loro prodotti e servizi alla comunità. Interconnette gli individui che sono in sintonia con i mercati alternativi, con le valute sociali, il commercio equo e in generale con i principi di FairCoop. Tutti i prodotti e servizi sono a pagamento in FairCoin. Questo aumenta 'usabilità/circolazione della moneta e aiuta a costruire alternative per un'economia più equa.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Vai a FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>Bank of Commons (BotC) è una iniziativa cooperativa il cui obiettivo è quello di trasformare attività bancarie, pagamenti e valute, al fine di sostenere l'economia cooperativa e i movimenti sociali a livello globale e locale. BotC ha adottato FairCoin come valuta sociale strategica globale e la sua tecnologia blockchain come parte dell'elaborazione e dell'adozione di strutture finanziarie decentralizzate a sostegno del bene comune . BotC è indirettamente legato al kit di strumenti di FairCoop per ampliare, per quanto possibile, il campo potenziale di utenti/aderenti.</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Vai a BankoftheCommons</a>

</div>

</div>
