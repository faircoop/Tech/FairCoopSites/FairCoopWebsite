---
title: 'Ferramentas  FairCoop'
media_order: tools.jpg
menu: Ferramentas
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin</h2>
<p>FairCoin é uma moeda digital baseada nume de rede de pessoas entre pares ( p2p), descentralizada e protegida com uma forte encriptação. É uma poderosa ferramenta para mudar o mundo e permite a independencia dos bancos centrais, das instituições financeiras e dos governos. É , por isso, uma ferramenta chave no novo sistema económico. Comparada com outras criptómoedas, como o Bitcoin, o seu valor é estável e apenas se submete a alterações mediante o consenso da comunidade.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Ir para FairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>FairPay é um sistema de chips NFC para que  pagar com FairCoin seja tão simples para usuários e comerciantes como o uso de um cartão de débito ou crédito. O chip NFC contém um ID único que identifica o utilizador e que comunica directamente com o painel da App a que está associado. É através do painel da app, que os comerciantes têm incluída a possibilidade de trocar directamente os FairCoins por euros, caso seja necessário .</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Ir para FairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>Existem três grandes FairFunds ( Fundos Justos ): o Fundo do Sul Global, o Fundo do Comum e o Fundo da Infraestrutura Tecnológica. O FairCoin armazenados neste fundos têm como objectivo financiar futuros projectos que estão em harmonia com as metas de cada fundo e apoiar, em escala maior, a economia circular de FairCoin. Devido ao crescimento estável do valor do FairCoin como moeda, o poder económico do fundos também está a crescer constantemente. Assim, cada vez somos capazes de financiar mais e mayores projectos que estejam orientados aos nossos objectivos comuns.
</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Ir para FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org é um mapa e um directório virtual de espaços físicos de todos aqueles que aceitam FairCoin como meio de pagamento. Isto é especialmente útil para os utilizadores que encontrem com facilidade lugares onde gastar os seus FairCoin em troco de bens e serviços , e ao mesmo tempo apoiam a economia circular.</p>
<a class="btn btn-tools" href="https://use.fair-coin.org/" target="blank">Ir para useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin.net é uma forma fácil de comprar FairCoin através de diferentes moedas nacionais, como EUR, USD, SYP, CNY, RUB, GBP, INR, BRL, MXN, etc com métodos de pagamento usualmente utilizados, como transferência bancária, cartão de crédito ou débito, o inclusivamente, através do lugar físico ( nós locais). Este preço oficial de FairCoin é estável, definido por assembleia e ajustado em função de diferentes parâmetros do preço.</p>

<a class="btn btn-tools"href="https://getfaircoin.net/" target="blank">Ir para getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>FairSavins é um serviço de poupança de FairCoin que a FairCoop proporciona através da plataforma getFairCoin. É um serviço para aqueles que querem utilizar a nossa infraestrutura para manter os seus FairCoins sob as medidas de segurança apropriadas. É recomendado para pessoas que não têm computador próprio ou para aqueles que não se sentem seguros com conhecimentos informáticos para assumir a responsabilidade de cuidar da sua carteira digital .
</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Ir para FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>OCP é uma ferramenta organizativa chave que oferecemos a pessoas, colectivos, e coordenadores de projetos dentro da Freedom Coop, Bank of the Commons e de muitas outras iniciativas cooperativas que o futuro nos trará. Também se pode utilizar, por exemplo, para gerir projectos colectivos abertos e as suas equipas de trabalho, permitindo criar grupos de tarefas, contabilizar tempo empregue por cada membro para distribuir a abundância gerada, ou gerir a carteira FairCoin online para aceitar ou efetuar pagamentos em FairCoin.
</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Ir para OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>Freedom Coop é uma Sociedade Cooperativa Europeia ( SCE) que oferece um conjunto de ferramentas para a autogestão, o auto-emprego, autonomia económica e a desobediência financeira. Os membros autónomos podem, por exemplo, entregar as suas facturas à Freedom Coop e receber o dinheiro na sua conta virtual individual . Não é necessário identificação pessoal ou qualquer outro tipo de documentação. Todas a quotas de membro são pagas em FairCoin. Os benefícios da FreedomCoop se redistribuem pelos Nós Locais e são portanto cruciais para financiar e dinamizar a sua criação.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Ir para FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>FairMarket é o mercado online da FairCoop, em que todos os utilizadores podem oferecer os seus produtos e serviços à comunidade. Está ligado com as pessoas que estão em sintonia com os mercados alternativos, as moedas sociais, o comércio justo e o princípios gerais da FairCoop. Todos os produtos e serviços são pagos em FairCoin. Isto aumenta a usabilidade/circulação da moeda e ajuda a construir alternativas para uma economia mais equitativa.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Ir para FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>O Banco dos Comuns (BotC) é uma iniciativa cooperativa aberta cujo objectivo é transformar a banca, os pagamentos e as moedas para apoiar a economia dos movimentos cooperativos e sociais a nível global e local. O BotC adoptou estrategicamente a FairCoin, como moeda social global e a sua tecnologia de cadeia de blocos como parte do desenvolvimento e a adopção de estruturas financeiras descentralizadas para o comum. BotC está indiretamente ligado ao conjunto de ferramentas da FairCoop para ampliar o seu potencial de cooperantes o tanto quanto possível .</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Ir para BankoftheCommons</a>

</div>

</div>