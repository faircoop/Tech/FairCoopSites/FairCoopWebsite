---
title: 'FairCoop Tools'
media_order: tools.jpg
menu: Tools
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin</h2>
<p>FairCoin is a digital currency based on a decentralized peer-to-peer network which is protected with strong encryption. It is a powerful tool for changing the world through independence from central banks, financial institutions and governments. Thus it is a key tool in the new economic system. Compared to other cryptocurrencies, such as Bitcoin, its value is stable and frequently adjusted from the community by consensus.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Go to FairCoin website</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>FairPay is an NFC card system for customers and merchants to make FairCoin payments as easy as with a credit card. The NFC Sticker contains a unique ID, which identifies the card's user and communicates directly with the linked App Panel. Through this Panel, the merchants even have the possibility of changing these FairCoins directly into Euros if necessary.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Go to FairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>There are existing three major FairFunds, the Global South Fund, the Commons Fund and the Technology Infrastructure Fund. The stored FairCoins in these funds have the aim of financing future projects which are in harmony with the goal of each fund and to support the greater FairCoin circular economy. Due to the stable value growth of FairCoin as a currency, the economic power of the funds is constantly growing as well. Thus, we are more and more able to fund more and larger projects towards our common goals.</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Go to FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org is a virtual map and directory of physical spaces of all kinds accepting FairCoins as a payment method. This is especially useful for users to easily find locations to spend FairCoins for real-world products and services, and to support the circular economy at the same time.</p>
<a class="btn btn-tools" href="https://use.fair-coin.org/" target="blank">Go to useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin.net is an easy way to buy FairCoin via different national currencies like EUR, USD, SYP, CNY, RUB, GBP, INR, NGN, BRL, MXN, etc with commonly-used payment methods like wire transfer, credit or debit card or even using cash, through the physical places (Local Nodes). This official FairCoin price is stable, defined by the assembly and frequently adjusted due to diverse price parameters.</p>

<a class="btn btn-tools"href="https://getfaircoin.net/" target="blank">Go to getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>FairSaving is a FairCoin savings service FairCoop provides through the getFairCoin platform. It is offered to those who want to use our infrastructure to keep their Faircoins under all the appropriate security measures. It is recommended for people without a computer of their own, or for those who don’t feel confident enough with their computer skills to take responsibility for taking care of their Coop wallet.</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Go to FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>The OCP is the key organizational tool we offer to individuals, collectives, and project coordinators inside Freedom Coop, Bank of the Commons and many other cooperative projects in the future. It can also be used, for example, to manage open collective projects and their team work by setting up task forces, account the time spent by every member as a base of income distribution, or to manage an online FairCoin wallet to accept and make payments.</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Go to OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>FreedomCoop is a European Cooperative Society (SCE) which offers a toolkit for self-management, self-employment, economic autonomy and financial disobedience. Self-employed members can, for example, hand in their invoices to FreedomCoop and receive their money in their individual virtual account. Personal IDs or other documents are not required. All membership fees are payable in FairCoin. Profits of FreedomCoop are redistributed to the Local Nodes and thus crucial to finance and dynamize their creation.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Go to FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>FairMarket is FairCoop's online marketplace, where all participants can offer their products and services to the community. It interconnects individuals who are in tune with alternative markets, social currencies, fair trade and the general FairCoop principles. All products and services are paid in FairCoin. This increases the usability/circulation of the currency and helps to construct alternatives for a more equitable economy.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Go to FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>Bank of the Commons (BotC) is an open cooperative initiative whose objective is to transform banking, payments and currencies in order to support the economy used in cooperative and social movements at a global and a local level. BotC has adopted FairCoin as a strategic global social currency and its blockchain technology as part of the development and adoption of decentralized financial structures for the Commons. BotC is indirectly linked to FairCoop's toolkit to broaden the potential field of users as much as possible.</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Go to BankoftheCommons</a>

</div>

</div>
