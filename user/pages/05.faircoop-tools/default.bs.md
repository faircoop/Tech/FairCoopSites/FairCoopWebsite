---
title: 'Herramientas FairCoop'
media_order: tools.jpg
menu: Alati
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin</h2>
<p>FairCoin je digitalna valuta utemeljena na decentralizovanoj mreži lice-u-lice (P2P) koja je zaštićena snažnom enkripcijom. Moćni alat za promjenu svijeta kroz nezavisnost o centralnim bankama, finansijskim institucijama i vladama. Zbog toga je to ključni alat u novom ekonomskom sistemu. Uspoređujući ga sa drugim kriptovalutama, kao što je Bitcoin, njegova vrijednost je stabilna i često usklađivana od zajednice kroz koncenzus.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Idite FairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>FairPay je NFC kartični sistem za klijente i trgovce za obavljanje FairCoin plaćanja sa lakoćom poput kreditne kartice. NFC Naljepnica sadrži jedinstvenu identifikaciju (ID), koja identificira korisnika kartice te komunicira direktno sa povezanom aplikacijom App Panel. Kroz ovu aplikaciju Panel, trgovci čak imaju mogućnost razmjene FairCoin-a direktno u Eure, ako je potrebno.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Idite FairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>Postoje tri glavna FairFunds-a: Global South Fund (Fond Globalnog Juga), Commons Fund (Fond Zajedničara) i Technology Infrastructure Fund (Fond Tehnološke Infrastrukture). Pohranjeni FairCoin-i u ovim fondovima imaju svrhu finansiranja budućih projekata koji su u skladu sa ciljem pojedinačnog fonda te da podrže rastuću FairCoin cirkularnu ekonomiju. Kroz stabilan vrijednosni rast FairCoin-a kao valute, ekonomska moć fondova je u stalnom porastu. Stoga smo sve više i više u mogućnosti finansirati veće projekte usmjerene na ostvarivanje naših zajedničkih ciljeva.</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Idite FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org je virtuelna mapa i direktorij fizičkih mjesta svake vrste koje primaju FairCoin-e kao način plaćanja. Ovo je posebno korisno za korisnike da lakše pronađu lokacije da potroše FairCoin-e za stvarne proizvode i usluge te u isto vrijeme daju podršku cirkularnoj ekonomiji.</p>
<a class="btn btn-tools" href="https://use.fair-coin.org/" target="blank">Idite useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin.net je jednostavan način da se kupi FairCoin putem različitih nacionalnih valuta kao što su EUR, USD, SYP, CNY, RUB, GBP, INR, NGN, BRL, MXN, itd. na uobičajeni način plaćanja kao što su doznake, kreditne ili debitne kartice ili čak gotovinom, na fizičkom mjestu (Lokalni Čvorovi). Službena cijena FairCoin-a je stabilna, definirana skupštinom i frekventno prilagođena različitim parametrima cijena.</p>

<a class="btn btn-tools"href="https://getfaircoin.net/" target="blank">Idite getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>FairSaving je FairCoin usluga štednje koju FairCoop omogućava kroz getFairCoin platformu. Ponuđena je onima koji žele da koriste našu infrastrukturu da pohrane svoje FairCoin-e pod odgovarajuće sigurnosne mjere. Preporučuje se ljudima bez sopstvenog kompjutera ili onima koji se ne osjećaju dovoljno uvjereni u svoje kompjuterske vještine da preuzmu odgovornost za brigu o svom Coop novčaniku.</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Idite FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>OCP je ključni organizacijski alat koji nudimo pojedincima, zajednicama i projekt koordinatorima unutar FreedomCoop-a, Bank of the Commons i mnogim drugim kooperativnim projektima u budućnosti. Može biti korišten, na primjer, da upravlja otvorenim zajedničkim projektima i timskim radom kako bi se uspostavile radne grupe, računati utrošeno vrijeme svakog člana kao bazu za distribuciju prihoda ili upravljati online FairCoin novčanikom da se prime i izvrše uplate.</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Idite OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>FreedomCoop je Evropsko Kooperativno Društvo (SCE) koje nudi set alata za samo-upravljanje, samo-zapošljavanje, ekonomsku autonomiju i finansijsku neposlušnost. Samo-zaposleni članovi mogu, na primjer, predati fakture FreedomCoop-u i primiti novce na svom virtualnom računu. Lični dokumenti sa podacima ili drugi dokumenti nisu potrebni. Svi troškovi za članstvo se mogu platiti FairCoin-ima. Profiti od FreedomCoop-a su redistribuirani Lokalnim Čvorovima te tako finansijski krucijalni i dinamizirajući za njihovo uspostavljanje.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Idite FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>FairMarket je FairCoop-ova online tržnica, gdje svi učesnici mogu ponuditi svoje proizvode i usluge zajednici. Međusobno povezuje pojedince koji su upućeni u alternativna tržišta, društvene valute, pravednu trgovinu i opće principe FairCoop-a. Svi proizvodi i usluge se plaćaju FairCoin-ima. Ovo povećava upotrebljivostt/cirkulaciju valute i pomaže izgraditi alternative za pravičniju ekonomiju.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Idite FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>Bank of the Commons (BotC) je inicijativa otvorene kooperative sa ciljem da transformira bankarenje, plaćanja i valute radi podrške ekonomiji kojom se služe kooperativni i društveni pokreti na globalnom i lokalnom nivou. BotC je usvojila FairCoin kao stratešku globalnu valutu i tehnologiju blockchain kao dio razvijanja i usvajanja decentralizovanih finansijskih struktura za Zajedničare. BotC je indirektno povezana sa setom alata FairCoop-a da se proširi potencijalno polje korisnika što je više moguće.</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Idite BankoftheCommons</a>

</div>

</div>
