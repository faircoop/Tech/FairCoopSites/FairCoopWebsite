---
title: Wirtschaftssystem
media_order: economicSystem.e9a4e2d2.jpg
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

> FairCoop sieht die Entwicklung eines neuen globalen Wirtschaftssystems als vielversprechensten Einstiegspunkt um einen kollektiven Wandel in unserer Gesellschaft zu stimulieren.

Eines der vorrangigsten Ziele von FairCoop ist der Aufbau und die Weiterentwicklung eines neuen globalen Wirtschaftssystems, das auf Zusammenarbeit, Ethik, Solidarität und Gerechtigkeit in unseren gegenseitigen Wirtschaftsbeziehungen basiert. Unser Plan ist es, das größtmögliche Maß an globaler wirtschaftlicher Gerechtigkeit wiederherzustellen, indem wir:
* um unsere digitale Währung ein Wirtschaftssystem aufbauen, welches komplett unabhängig vom Zentralbankensystem ist und außerhalb der Kontrolle von staatlichen Institutionen funktioniert.
* FairCoin für etwas zu nutzen, das bisher immer gegen den globalen Süden gespielt hat: die freien Marktkräfte (Angebot und Nachfrage), diese hacken und den 'kooperativen Virus' einpflanzen.
* den kooperativen Virus nutzen um Vertrauen und Anreize zu schaffen um eher in ein gemeinschaftliches System zu investieren als in ein volatiles, spekulatives und auf Konkurrenz basiertes System.
* eine stabile Wachstumsstrategie verfolgen, um auf Grund der begrenzten Anzahl an Währungseinheiten und einer wachsenden Nachfrage (deflationärer Charakter) den Wert der Währung stetig und regelmäßig nach oben anzupassen.
* die Wertsteigerung nutzen um neue Projekte und Weiterentwicklungen von Werkzeugen zu finanzieren, die im Einklang mit den Zielen von FairCoop sind und somit die Infrastruktur des Systems kontinuierlich verbessern und erweitern.
* die neuen Projekte, Werkzeuge und Infrastrukturen nutzen um die Verwendung von FairCoin für immer mehr Produkte und Dienstleistungen zu erleichtern, die im Einklang mit den FairCoop Prinzipien sind, wodurch ein größerer Nutzwert für FairCoin geschaffen wird.
* den stetig wachsenden Gebrauch von FairCoin nutzen, um global und lokal immer unabhängiger von Fiat-Währungen zu werden, mit dem ultmativen Ziel ein zirkulären Wirtschaft zu etablieren.
* die zirkuläre Wirtschaft, welche durch die Einbeziehung von immer mehr Produkten und Dienstleistungen weltweit gestärkt wird, dazu verwenden um unser Grundbedürfnisse zu decken und um auf natürliche Weise mehr Nutzer (Individuen, Gruppen, Netzwerke) in FairCoin und FairCoop einzubinden.
* unsere kollektive und finanzielle Kraft als Gemeinschaft, die sich aus der stetig wachsenden Anzahl an TeilnehmerInnen des FairCoop System ableitet, dazu verwenden um immer mehr Menschen von jeglicher Art der politischen und wirtschaftlichen Unterdrückung zu befreien.
* die weit verbreiteten und miteinander verbundenen Werkzeuge, menschlichen Netzwerke, Dienstleistungen und Funktionen des unabhängigen dezentralen Systems als Basis für den sozialen Wandel der gesamten Weltgemeinschaft nutzen, welcher auch als integrale Revolution bekannt ist.