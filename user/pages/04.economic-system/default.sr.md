---
title: 'Економски систем'
media_order: economicSystem.e9a4e2d2.jpg
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

> Revoluciju globalnog ekonomskog sistema FairCoop vidi kao polazište za izazivanje i iniciranje obećavajuće kolektivne promene društva.
edan od glavnih ciljeva FairCoop-a je izgradnja globalnog ekonomskog sistema zasnovanog na saradnji, etici, solidarnosti i pravičnosti. Da bi sprovođenje ovog cilja bilo moguće, definisali smo glavne uzroke bolesti ekonomskog sistema i preokrenuli u našu korist mehanizme koji su uzrok nepostojanja globalnog balansa.

Naš plan je da u najvećoj mogućoj meri povratimo ekonomsku pravičnost na najviši mogući nivo, i to:
* upotrebom sopstvene digitalne valute FairCoin-a i razvojem samostalnog ekonomskog sistema, potpuno nezavisnog od centralnih banaka i institucija.
* upotrebom FairCoin-a za nešto što se uglavnom koristilo protiv siromaštva, takozvanog 'Globalnog Juga': za osnaženje tržišta (ponuda - tražnja), koje ćemo da hakujemo i 'zarazimo' virusom kooperativnosti.
* upotrebom kooperativnog virusa za stvaranje poverenja i podsticaja, kako bi svi pre investirali u naš, nego u nestalni i spekulativni kompetitivni sistem .
* upotrebom strategije stabilnog rasta za povećanje vrednosti valute zbog ograničene ponude valutnih jedinica (deflatorni karakter) i povećanjem potražnje za valutom.
* upotrebom uvećane vrednosti valute za finansiranje razvojnih projekata i sredstava, koji su u skladu sa ciljevima FairCoop-a, kao i za konstantno širenje infrastrukture sistema.
* upotrebom projekata, sredstava i infrastrukture, kako bi se olakšala svakodnevna upotreba FairCoin-a za proizvode i usluge koji su u skladu sa FairCoop principima, i samim tim povećala njegova upotrebljivost.
* upotrebom stalnog rasta upotrebljivosti FairCoin-a, kako bi globalno sve više bili nezavisni od običnih valuta, krećući se ka razvoju cirkularne ekonomije.
* upotrebom cirkularne ekonomije, ojačane uključivanjem sve više proizvoda i usluga širom sveta, radi pokrića naših osnovnih potreba i povećanja broja korisnika (pojedinaca, grupa, mreža) FairCoin-a.
* upotrebom naše kolektivne i finansijske moći, izvedene iz konstantno rastuće participacije, za ojačanje i oslobađanje sve više ljudi od bilo koje vrste ekonomske ili političke zabrane.
* upotrebom rasprostranjenih, ali međusobno povezanih sredstava, ljudskih veza, usluga i odlika ovog nezavisnog decentralizovanog sistema, kao baze za promenu celokupnog društva - integralnu revoluciju.