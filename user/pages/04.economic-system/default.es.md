---
title: 'Sistema Económico'
media_order: economicSystem.e9a4e2d2.jpg
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

> FairCoop ve la revolución del sistema económico global como la puerta de entrada más prometedora para provocar e iniciar un cambio colectivo en la sociedad.

Uno de los objetivos prioritarios de FairCoop es construir un nuevo sistema económico global basado en la cooperación, la ética, la solidaridad y la justicia en nuestras relaciones económicas. Para lograrlo, hemos definido cuáles son las principales causas de los fallos actuales del sistema económico y hemos puesto dichos mecanismos, causantes del desequilibrio global de poder, a nuestro favor.

Nuestro plan es restaurar el mayor nivel de justicia económica global que podamos, de la siguiente manera:

* Usando nuestra propia moneda digital FairCoin y desarrollando para ella un sistema económico propio, totalmente independiente de los bancos centrales y de las instituciones.
* Usando FairCoin para algo que normalmente jugaba en contra del Sur Global: las fuerzas del mercado (oferta-demanda). La idea es hackearlas e insertarles el virus de la cooperación.
* Usando el virus de la cooperación para crear confianza e incentivos para que todo el mundo invierta en un sistema cooperativo, en lugar de hacerlo en un sistema competitivo volátil y especulativo.
* Usando la estrategia del crecimiento estable para incrementar el valor de la moneda, dado que el número de unidades en circulación es limitado (carácter deflacionario) y que su demanda es creciente.
* Utilizando el valor incrementado de la moneda para financiar el desarrollo de proyectos y herramientas alineados con los objetivos de FairCoop y para extender constantemente la infraestructura del sistema.
* Empleando esos proyectos, herramientas e infraestructura para facilitar el uso común de FairCoin para más productos y servicios que estén en línea con los Principios de FairCoop, aumentando así su usabilidad.
* Usando este incremento constante de la utilidad de FairCoin para independizarnos cada vez más de las monedas fiat a escala global y para avanzar hacia una economía circular.
* Usando la economía circular, reforzada por la inclusión de un número creciente de productos y servicios en todo el mundo, para cubrir nuestras necesidades básicas y para incorporar de forma natural más usuarias (personas, grupos, redes) a FairCoin.
* Usando nuestro poder colectivo y financiero, derivado de una participación constantemente creciente, y a nuestras usuarias para empoderar y liberar cada vez a más gente de cualquier clase de represión política o económica.
* Utilizando las ampliamente dispersas pero interconectadas herramientas, redes humanas, servicios y características de este sistema descentralizado independiente como base para el cambio global de la sociedad, conocido como revolución integral.