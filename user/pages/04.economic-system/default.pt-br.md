---
title: 'Sistema economico'
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

> A FairCoop enxerga a revolução do sistema econômico global como o ponto de partida mais promissor para provocar e iniciar uma mudança coletiva na sociedade.

Um dos objetivos prioritários da FairCoop é construir um novo sistema econômico global, baseado na cooperação, na solidariedade e na justiça em nossas relações econômicas. Para conseguir isso, definimos as principais dificuldades do atual sistema e seus mecanismos, que causam de um desequilíbrio global do poder, e decidimos como transformá-los a favor de todos nós, globalmente.

Nosso plano é reestabelecer o maior nível de justiça econômica global possível da seguinte maneira:

* Usando a nossa própria moeda digital, a FairCoin, e desenvolvendo o seu próprio sistema econômico, completamente independente dos bancos e instituições centrais.
* Usando a FairCoin para algo que geralmente desfavorece o hemisfério sul, como as forças do mercado (oferta/demanda), que queremos hackear e contaminar com o vírus da cooperação.
* Usando o vírus da cooperação para criar confiança e incentivos para que todos invistam em um sistema cooperativo em vez de um sistema competitivo, volátil e especulativo.
* Usando uma estratégia de crescimento estável para aumentar o valor da moeda devido à oferta limitada de unidades monetárias (caráter deflacionário) e ao aumento da demanda pela FairCoin.
* Usando o aumento do valor da moeda para financiar projetos e ferramentas alinhados aos objetivos da FairCoop, além de estender constantemente a infraestrutura do sistema.
* Usando esses projetos, ferramentas e infraestrutura para facilitar o uso geral da FairCoin com outros produtos e serviços que estejam de acordo com os princípios da FairCoop e, assim, aumentar sua conveniência.
* Usando esta constante e crescente conveniência da FairCoin para torná-la globalmente mais independente das moedas fiduciárias e nos mover para uma economia circular.
* Usando a economia circular, reforçada pela inclusão de cada vez mais produtos e serviços em todo o mundo, para atender a nossas necessidades básicas e, naturalmente, incorporar mais usuários da FairCoin (indivíduos, grupos, redes).
* Usando o nosso poder coletivo e financeiro, derivado de um aumento dos usuários e participação, para capacitar e libertar cada vez mais pessoas de qualquer tipo de supressão econômica ou política.
* Usando os elementos altamente dispersos, porém interconectados – ferramentas, redes humanas, serviços e recursos – deste sistema descentralizado independente como base para a mudança de toda a sociedade, conhecida como a Revolução Integral.
