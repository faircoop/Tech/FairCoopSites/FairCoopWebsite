---
title: 'Sistema economico'
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

> A FairCoop vê a revolução do sistema económico global como a porta de entrada mais prometedora para provocar e iniciar uma mudança colectiva na sociedade.

Um dos objectivos prioritários da FairCoop é construir um novo sistema económico global baseado na cooperação, na ética, na solidariedade e na justiça nas nossa relações económicas. Para alcançá-lo, definimos quais são as principais causas dos actuais falhas do sistema económico e temos posto esses ditos mecanismos que causaram o desequilíbrio global do poder, a nosso favor.

O nosso plano é restaurar o maior nível de justiça económica global, da seguinte maneira :

* Usar a nossa própria moeda digital FairCoin e desenvolver um sistema económico próprio, totalmente independente dos bancos centrais e das instituições.
* Usar o FairCoin para algo que normalmente se julgava contra o Sul Global: as forças de mercado ( oferta-procura). A ideia é hackear e injetar o vírus da cooperação.
* Usar o vírus da cooperação para criar a confiança e incentivos para que todo o mundo adote um sistema cooperativo, em vez de um sistema competitivo, volátil e especulativo.
* Usar a estratégia de crescimento estável para incrementar o valor da moeda, dado que o número de unidades em circulação é limitado ( carácter deflacionário ) e que a sua procura seja crescente.
* Usando esses projetos, ferramentas e infraestrutura para facilitar o uso comum da FairCoin para mais produtos e serviços que estejam em linha com os Princípios da FairCoop, aumentando assim a sua usabilidade.
* Usar este incremento constante da utilidade do FairCoin para nos tornarmos cada vez mais independentes das moedas fiat à escala global e para construir uma economia circular.
* Usar a economia circular, reforçada pela inclusão de um número crescente de produtos e serviços em todo o mundo, para cobrir as nossas necessidades básicas e para incorporar de uma forma natural mais cooperantes ( pessoas, grupos, redes ) .
* Usar o nosso poder coletivo e financeiro, derivado de uma participação constantemente crescente, e os nossos cooperantes para empoderar e libertar cada vez mais pessoas de qualquer classe de repressão política e económica .
* Utilizando amplamente as ferramentas, embora dispersas, mas interconectadas, redes humanas, serviços e características de este sistema descentralizado independente como base para a mudança global da sociedade, conhecido como revolução integral .
