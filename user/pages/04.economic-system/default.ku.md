---
title: 'Sîstema Aborî'
media_order: economicSystem.e9a4e2d2.jpg
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

> FairCoop şoreşa aborî ya cîhanî bingeh digre weku xala têketinê ya herî zêde hêvîyê dide ji bo provekekirin û însiyatîf girtina guhertinên kolektîv di civakê de armanç dike.

Yek ji tişta pêşahîk ya FairCoopê di têkiliyên me yên aborî de li ser bingehên koparasyonê, ehlaqî, piştgirî û edaletê avakirina sîstemek aborî ya cîhanî ya nû ye. Ji bo em vê serbixin me pênaseya sedemên sereke yên nexweşiyên sîstema aborîya heyî çêkiriye û van mekanîzmeyan ku dibin sedema nehevahengiya aborîya cîhanê ji xwe re kiriye çavkanî.

Weku mijarek herî ciddî, em bandora hêza zindî ya banqên navendî û dezgehên wan ku mirovan neçar dikin bes sîstemek diravî bikarbînin ku bi piranî rêjeya 1% li ser bingehê deynkirinê ava buye, û enfilasyonek sabît ku dibe sedema hem lawaztîya piraniya civakê çêbibe û hem jî dibe sedem em bikevin dibin barê giran yê nehevahengiya tenduristiya cîhanî de. Plana me ewe ku em bikarin edaleta aborîya cîhanî di asta wê ya herî baş de restore-nûjen bikîn,bi rêya:
* Em pereyên xwe yên dijîtal FairCoin bikarbînin û sîstema aborîya wê pêşve bibin ku bi temamî serbixwe ye ji banqên navendî û dezgehên wan.
* Bikaranîna FairCoin ji hinek tiştên bi piranî li dijberê Başûra Cîhanî: hêzên marketan (daxwaza hilberînê), wana hack kirin û virusa koparasyonê bixe nava wan de.
* Bikaranîna virusa koperativê ji bo afirandina sermîyandana rast û dilkêş ji bo herkesê li şuna yekê spekulatîv û ber bi çav sîstema pêşbirkê.
* Bikaranîna stratejiya mezinbuna sabît ji bo zêdekirina nirxê dirav li gora daxwaza hilberandina sinordarkirî ya yekeyên diravî (karektera deflasyonê) û daxwaza zêdebuyî ya dirav.
* Ji bo pêşxistina amêr û projeyan em nirxê zêdebuyî yê dirav bo fînansekirina wê bikarbînin ku li hîzaya serkeftinên FairCoop re dibe yek û bi domdarî bunyana vê sîstemê berfireh dike.
* Bikaranîna van proje, amêr û bunyanan ji bo hesankirina karanîna giştî ya FairCoop bo xwe gihandina xizmet û berhemên zèdetir bi xeta FairCoop Prensîb, û bi vî awayî bikaranîna wê zêde bikîn.
* Bi domdarî bikaranîna vê sîstemê sûdmendiya FairCoonê zêdetir dike ku pitir bibe cîhanî û serbixwe ji diravên bi destûrên fermî ne û vê ber bi Aborîya Çemberî ve pal dide.
* Bikaranîna aboriya Çemberî -Bazinî, bi tevilkirina xizmetguzarî û berhemên zêdetir ji seranserê cîhanê ve dikare bêt bihêzkirin, ji bo em pêwîstiyên xwe yên bingehîn veşêrîn û bi xwezayî pitir bikarhîner bi dest bixin (kes, grûp,torên grupan) ji bo FairCoinê ev girînge.
* Bihêzbûna me ya kolektîv û fînansî çavkaniya xwe ji bi domdarî tevilkirina beşdar û bikarhîneran digre û ji bo hêj bihêztirkirin û serbestkirinê beşdarkirina hêj zêdetir mirovane ji hemî beşên aborî û polîtîkî.
* Ji van amêr, torên mirovî, xizmet û taybetmendiyên sîstema ne navendî ya serbixwe re ku bingeha guhertina çivakî ye û hemî cîhanê digre nava xwe re Şoreşa Întegral tê gotin.