---
title: 'Pridruži nam se'
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

pridružite se našoj [grupi dobrodošlice](https://t.me/askfaircoop) ili na [FairCoop Forum](https://forum.fair.coop/) i ostvarite direktan kontakt. U neprestanoj smo potrebi za ljudim kojji posjeduje vještine a željeli bi da pomognu u razvoju ovog prekrasnog pokreta. Međutim, bilo ko sa proaktivnim razmišljanjem i dobrodušnosti je više nego dobrodošao:)