---
title: Únete
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

Únete a nuestro [grupo de bienvenida en telegram](https://t.me/askfaircoop) o al [FairCoop foro](http://forum.fair.coop) para estar en contacto directo con nosotros. Estamos continuamente buscando gente cualificada que quiera contribuir al desarrollo de este bonito movimiento. De cualquier forma, todas aquellas personas con una mentalidad proactiva y un buen espíritu son más que bienvenidas :)