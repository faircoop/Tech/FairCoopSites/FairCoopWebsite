---
title: 'FairCoin y los grupos de consumo'
media_order: faircoin_and_consumers_groups.jpg
date: '10-02-2018 13:34'
taxonomy:
    category:
        - blog
    tag:
        - faircoin
        - 'consumer group'
        - comsumer
        - products
---

La autogestión en general es uno de los principios hacia los que FairCoop orienta el diseño de sus herramientas, para fomentar tanto como sea posible la autonomía descentralizada, en cualquier parte y en todas sus formas. **Los grupos de consumo ecológico autogestionados son una interesante opción para alimentarse de manera sana por un precio justo, dignificar la agricultura ecológica, fomentar la economía y las relaciones comunitarias locales, y cuidar el medio ambiente.**

Consisten en grupos de personas que optan por organizar ellas mismas, sin intermediarias, su cesta de la compra. Escogen a las productoras a quienes quieren adquirir los productos alimentarios, llegan a acuerdos con ellas y gestionan los pedidos, la forma de pago y la de reparto.

¿Qué implicaciones tiene esto? Entre otras:

1. Al descartar agentes intermediarias, la parte del precio que se llevan éstas desaparece, con lo que, por lo general, los productos pueden salir más baratos a las consumidoras, al tiempo que las productoras reciben unos precios más justos.

2. Se dignifica el concepto de agricultura ecológica, tan denostado por los criterios "fuera de toda lógica" que aplican muchas de las certificaciones ecológicas oficiales. Lo que es ecológico o no pasa ahora a ser criterio exclusivo del grupo de consumo que, generalmente, tiene en cuenta, no sólo el empleo de productos no transgénicos o químicos tóxicos, sino también aspectos como la cercanía geográfica y la situación laboral de las trabajadoras a quienes les compran. En este sentido, resulta interesante el surgimiento de muchas certificaciones participativas que, aunque no son oficiales, son aceptadas de buena gana por quienes participan en su definición (lo que realmente es "ecológico", según su criterio).

3. Se fomenta la economía local, al escoger siempre que sea posible productos de cercanía, al tiempo que las consumidoras pueden conocer directamente a quienes producen y visitar sus explotaciones, generándose así un tipo de relación comunitaria que no existe en absoluto cuando los alimentos se adquieren en un supermercado.

4. Se cuida el medio ambiente, porque los criterios de los grupos de consumo generalmente adoptan decisiones mucho más conscientes que cualquier garantía medioambiental que provenga de una entidad oficial.

**Muchos de estos grupos efectúan sus transacciones en moneda fiat y algunos también la combinan con monedas locales. FairCoin es una buena opción para llegar allí donde estas monedas locales no lo hacen. Es decir, se puede integrar en un círculo económico de este tipo del mismo modo que cualquier moneda social, pero con algunas otras posibilidades.**

Al incorporar FairCoin a estos grupos, no existe la necesidad de ser "prosumidora" dentro del círculo, como ocurre la mayoría de las veces en las monedas de crédito mutuo, ya que es intercambiable por fiat, siempre que las productoras estén registradas en FairCoop. Además, no siempre les será necesario cambiar sus FairCoin por euros, dólares, etc. puesto que fuera de ese círculo del grupo de consumo hay un mercado mucho más amplio en el que las participantes pueden gastar sus FairCoin, en un ámbito que no sólo supera al grupo de consumo y su área local, sino que es internacional.

**Distintos nodos locales pueden coordinarse para satisfacer necesidades mutuas que no estén a su alcance y existe también un grupo de Economía Circular para grandes proveedores a escala internacional. Así, desde lo más local hasta lo más global, poco a poco, vamos conformando una economía más justa, más ecológica y más satisfactoria para todas. Ni más ni menos que una economía autogestionada; es decir: la economía que todas queremos.**