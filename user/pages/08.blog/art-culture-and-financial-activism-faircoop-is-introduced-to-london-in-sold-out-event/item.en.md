---
title: 'Art, Culture and Financial Activism: FairCoop is Introduced to London in Sold Out Event'
media_order: 'legislation_cryptocurrencies_faircoin_0 (1).jpg'
main_image:
    heading: 'Art, Culture and Financial Activism'
date: '26-01-2018 16:46'
taxonomy:
    category:
        - blog
    tag:
        - activism
        - culture
        - 'local nodes'
        - london
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

> "Art, Culture and Financial Activism" was the theme of the sold-out [MoneyLab, hosted at Somerset House, in London - UK, on January 20th.](https://www.somersethouse.org.uk/whats-on/moneylab-art-culture-and-financial-activism) Around 40 researchers, artists, designers, and cultural entrepreneurs from around the world presented a day-long programme of **workshops, discussions and artistic experimentation to explore the relationship between critical art-making, decentralised financial technologies, collective organisation, and civil disobedience.**

> Previously organised by the Amsterdam-based Institute of Network Cultures and now in its 4th edition, MoneyLab is a programme of critical research and artistic intervention that explores the connections between contemporary art, financial activism and digital culture. 

> **As its Coordinator Patricia de Vries put it on the opening considerations, as we enter the 10th year of the global financial crisis, it is very crucial to distinguish "old wine from its fancy new bottles"**. According to her, "solutions" that have been presented as an alternative are not always real alternatives, such as many of the cryptocurrencies available nowadays: "Tech-savvy elites are hoarding Bitcoins and using them as a commodity, it is a speculative object, it is not a currency. You need to have the money to build the infrastructure to mine them; that is not disruptive, that is not alternative, it is just a new object of speculation. It is the same thing, just a new bottle".

**Using this metaphor, FairCoop and FairCoin can be understood as refreshing and delicious new wines.**

Our movement was introduced in a workshop titled "Earth’s Cooperative for Economic Justice". Juan Cruz explained that our goal is building another system – open, cooperative and kind to the planet – from the bottom up; Jorge Saiz showed how FairCoin aims to become a digital green currency for a new global economic system; and Enric Duran joined us through a video link to share the wider vision for the Integral Revolution. 

	To celebrate the way that the first lot of FairCoin was distributed, we promoted an airdrop: anyone who downloaded and sent their wallet addresses within 24 hours of our workshop received an equal share of a 500 FAIR, donated by a member of the group for the occasion. As a result, 16 more people are now able to buy goods and services through the FairMarket, or save their FairCoin to spend in the shops that will eventually join the local ecosystem.

Although for most people present this was the first introduction to the FairCoop ecosystem, **a few members of the audience were already aware of our movement, and some even had FairCoin wallets**, such as Katharine Vega, an artist and researcher who heard about our digital money in 2017. As someone passionate about climate change, she started to worry about the ethical dimension of cryptocurrencies and the impact of mining on the environment, which led her to look for alternatives. "I was googling ethical cryptocurrencies, climate change, ecology and stuff like that", Katharine says, explaining that she had been feeling depressed by the way people talk about cryptomoney. "There is this whole opportunity to reorganise how we do things, but it seems to be encroached on by the same old forces of people wanting to make a profit for themselves. There is a higher potential and I think the best way to claim that higher potential is to have working models with communities", Katharine concluded, explaining that she plans to use FairCoin to support a community in Vanuatu and also wants to get involved in the upcoming London local node. 

The event ended with a book launch – "MoneyLab Reader 2: Overcoming the Hype" – a collection of essays from artists, academics and activists critically exploring art, finance and technology, as well as the political and social territories created since the financial crash. FairCoin gets a mention in "Postcards From The World Of Decentralized Money: A Story In Three Parts", essay by Jaya Klara Brekke. To download or order a free physical copy, [click here](http://networkcultures.org/blog/publication/moneylab-reader-2-overcoming-the-hype/).