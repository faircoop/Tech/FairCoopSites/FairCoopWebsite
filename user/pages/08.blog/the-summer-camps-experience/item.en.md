---
title: 'The Summer Camps experience'
media_order: 'portada_post_summercamp-882x6001.jpg,Summer-Camp-1.jpg,Summer-Camp-2.jpg,Summer-Camp-3.jpg,Summer-Camp-4.jpg,Summer-Camp-5.jpg'
summary:
    description: 'FairCoop is beginning to consolidate its new form of organization. Finally, thanks to the work of the last Summer Camps (in Décentrale, Mont Soleil, Switzerland, with the help of Synergiehub and SCI Schweiz), a new operating scheme has emerged, which we will detail in our next post.'
thumbnail_image:
    image: portada_post_summercamp-882x6001.jpg
date: '08-09-2017 17:29'
publish_date: '08-09-2017 17:29'
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

FairCoop is beginning to consolidate its new form of organization. Finally, thanks to the work of the last Summer Camps (in Décentrale, Mont Soleil, Switzerland, with the help of Synergiehub and SCI Schweiz), a new operating scheme has emerged, which we will detail in our next post. There, in Jura, we spent eight weeks of intense work in which we approached different aspects related to the FairCoop ecosystem. Two months of pleasant cohabitation and a continuous flow of ideas whose development we describe below.

 

**July 30th – August 13rd
Introduction to FairCoop ecosystem. How to create a local node in your region**

The first FairCoop camp introduced dozens of young people from all over the world (Ukraine, Mexico, Spain, Czech, Russia, Italy, Ethiopia) to the principles of permaculture, self-empowerment, synergy, solidarity, cooperatives, alternative economies in general, citizen journalism, democratic schools, etc… Working on building the dome, yurt, wood chopping, renovation of the veranda was an important part of learning how to self-organize and share the values of community life.

At first, the camp was “chaotic”, according to the participants; later realizing that the “chaos” was part of the order they needed to establish with their hosts. Intensive work outside and inside the house, in the kitchen, promoting diversity and vegetarian food, weekend trips to lakes and mountain houses, and cities around Jura, was all part of the plan for them to learn and enjoy as much as they can during this process.

Young people were surprised that everybody was equal, in the sense that there were no classic representative authorities like owners, leaders, teachers… They often didn’t understand everything that was part of the workshops and lectures, as a complex study of theory and praxis of subjects like permaculture, anarchy, cooperatives, blockchain and cryptocurrency economics, protesting at first that there was too much information. Participants were slowly introduced to communicating on Telegram, installing FairCoin wallets, buying some FairCoin, spending some in the Espace Noir bar in St. Immier, and learning a lot about how differences can be peacefully harmonized in a diverse group of people.

It was interesting that one of the “conclusions” of the people participating in first of four summer camps dedicated to further development of FairCoop, was that they witnessed the movement needed to change the world away from its current capitalist system.
 
![](Summer-Camp-1.jpg)

**August 16th – August 23rd
Blockchain for women**
During the 3rd week of August, a meeting of women took place to learn and reflect on this new technology. It was a great opportunity to meet, live together and learn both from Blockchain and the need to readjust everyone’s expectations when we want to move forward in a group towards a common goal. Women from diverse backgrounds, knowledge and expectations, embarked on the task of self-organizing both at the academic and logistical levels, with diverse results. The experience was certainly very enriching.

In addition to the enormous and worrisome gender and class gap that this new technology is generating, we learned some of the enormous possibilities that will emerge in the immediate future from it. Complicated but exciting, we understood how the FairCoop ecosystem uses this technological opportunity to generate value and support the creation of a fairer world. We also set about creating a space for the study of crypto trading and its intricacies, in order to contribute from a female/feminist perspective.

No less important was the realization that neglecting to care for each other and to consciously generate spaces for exchange and reflection – on what we want, and how we want to do things – leads us time after time to ways of doing that constrict us. A true reflection of what we have to do in our daily lives is needed, if we really want to organise from below and cooperatively – without anyone having to direct or supervise anyone else. Also if we want cooperative spaces really to be cooperative, so that in spite of the individualistic and hierarchical culture in which we have been raised, we are able to create spaces where we are all able to give the best of ourselves, and so take care of both people and the planet.

![](Summer-Camp-2.jpg)

**August 23 – September 6
FairCoop ecosystem open cooperative work**

This module was intended more for people already experienced in the FairCoop ecosystem, and had as its goal the refinement and development of the already existing FairCoop protocols and ways of organising, plus some new additions to these.

The workshop welcomed to Mont Soleil people from Switzerland, France, Austria, Catalonia/Spain, Argentina, Greece, Italy, South Africa, Philippines… and we were particularly happy to have some people from Bakur in Kurdistan (Turkish state), who had come to learn about merging their own style of self-organised cooperativism with our own.

The focus of this summer camp was to develop five areas of open cooperative work. This areas are: welcome, communication, common management, circular economy and tech. In each one of the areas an assembly was created that is now taking care of the priorities of the area, and defining tasks to be done and paid for in a fair and open way – simplifing the way in which any participant of the ecosystem has the opportunity to be rewarded based on the time spent in doing useful tasks-.

![](Summer-Camp-3.jpg)

A common budget was proposed (and approved later on in the September FairCoop assembly), in order to begin this innovative and inclusive method of collaboration. Most of the areas also activated roles of facilitators who will help the development of the assemblies and tasks, and some subareas were also introduced, looking forward to an even more decentralized open coop structure. We also learnt about dynamising assemblies and scrum/agile techniques, and applying these to the newly formed Areas of FairCoop.

We concentrated quite heavily on the new Circular Economy Area, intended to increase participation in FairCoop at a local level, and this does seem to have been successful, judging by the number of new Local Nodes which have been formed since the Summer Camp. We also worked a lot on the Welcome Area, Media and Communication, and Tech areas.

We couldn’t have left Jura without paying for beers in Faircoin at the fantastic Espace Noir bar, and the walk down from Mont Soleil to St. Imier was as much of a highlight as getting the cable car back up!

We really feel that the Summer Camp was a massive injection of energy into the project, and we unblocked some longstanding issues which allowed us to move the whole thing up to another level. This process is ongoing since then and we will be seeing the results soon. It was also great fun to meet everyone who we may up until then have only known ‘virtually’. FairCoop is all about building trust and community, unlike many other ‘crypto’ projects which are focused on individualism, so it was vital to be able to interact face-to-face and in person with the other people building the project. There was a lot of cooking and cleaning, and also a Fair bit of dancing and laughing!

![](Summer-Camp-4.jpg)

**September 7 – September 22
Developing a model of glocal governance**

Beyond just the immediate steps of developing the organizational structure of the FairCoop projects for the next phase of the next months and years, we looked at ways to scale a governance culture to society at large. A governance culture that embraces autonomy and gives authoritative answers to what we collectively want to consent to doing.

People from Switzerland, Finland, Spain, South Africa, Austria and Greece were involved in this part of the summer camp. We focused on collecting various governance best practices such as the Constellation Model, Sociocracy 3.0 and Occupy-Style Open Assemblies.

The governance structure that resulted from these workshops is one that aims to be parallel to other governance structures. It does not aim to replace them. It is complementary to them. It has, however, the inherent power to transform them or to bring about new structures that make old ones obsolete. It is glocal, translocal in nature. It is decentralised, powered through the interdependent participation of local and translocal random groups of people.

This governance culture does not make decisions. Its function is to facilitate awareness of current consensus in society. Once collective realization arises, there should be no need for making “decisions”. However, where decisions are made in other parallel governance structures, this collective realization is taken into account as part of agreeing on the chosen path forward. We defined ways to implement such a glocal governance culture that can be taken into account as we move forward.

![](Summer-Camp-5.jpg)