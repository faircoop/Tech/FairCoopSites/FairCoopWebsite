---
title: 'La experiencia de los Summer Camps'
media_order: 'portada_post_summercamp-882x6001.jpg,Summer-Camp-1.jpg,Summer-Camp-2.jpg,Summer-Camp-3.jpg,Summer-Camp-4.jpg,Summer-Camp-5.jpg'
summary:
    description: 'FairCoop está comenzando a consolidar su nueva forma de organización. Finalmente, gracias al trabajo del último encuentro de verano (en Décentrale, Mont Soleil, Suiza, con la ayuda de Synergiehub y SCI Schweiz), ha surgido una nueva forma de organización que detallaremos en nuestro próximo post.'
thumbnail_image:
    image: portada_post_summercamp-882x6001.jpg
date: '08-09-2017 17:29'
publish_date: '08-09-2017 17:29'
taxonomy:
    category:
        - blog
    tag:
        - camps
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

FairCoop está comenzando a consolidar su nueva forma de organización. Finalmente, gracias al trabajo del último encuentro de verano (en Décentrale, Mont Soleil, Suiza, con la ayuda de Synergiehub y SCI Schweiz), ha surgido una nueva forma de organización que detallaremos en nuestro próximo post. Allí, en Jura, pasamos ocho semanas de intenso trabajo en las que abordamos diferentes aspectos del ecosistema FairCoop. Dos meses de una grata convivencia y un continuo bullir de ideas, cuyo desarrollo describimos a continuación.


**30 julio – 13 agosto
Introducción al ecosistema FairCoop. Cómo crear un nodo local en tu región**

El primer camp de FairCoop introdujo a docenas de jóvenes de todo el mundo (Ucrania, México, España, Chequia, Rusia, Italia, Etiopía) en los principios de la permacultura, el autoempoderamiento, la sinergia, la solidaridad, las cooperativas, las economías alternativas en general, el periodismo ciudadano, las escuelas democráticas,… Trabajar en la construcción del domo, la yurta, la tala de madera, la restauración de la veranda, fue una parte importante del aprendizaje sobre cómo autoorganizarse y compartir los valores de la vida comunitaria.

Al principio, el camp era “caótico”, según las participantes; dándose cuenta más tarde de que ese “caos” era parte del orden que necesitaban establecer con sus anfitrionas. Trabajo intensivo en el exterior y el interior de la casa; en la cocina, promoviendo la diversidad y la comida vegetariana; excursiones de fin de semana a lagos y casas de montaña y a ciudades cercanas a Jura, era todo parte del plan para que aprendieran y disfrutaran al máximo durante este proceso.

La gente joven se sorprendió de que todo el mundo fuera igual, en el sentido de que no había las clásicas autoridades representativas, como propietarias, líderes, profesoras… A menudo, no entendían todo lo que formaba parte de los talleres y las charlas, como un estudio complejo de la teoría y la pŕactica de temas como la permacultura, la anarquía, las cooperativas, la blockchain y la economía de las criptomonedas, quejándose al principio de que había demasiada informaciónn. Las participantes fueron lentamente introducidas en la comunicación a través de Telegram, instalaron monederos de FairCoin, compraron FairCoin, gastando parte de ellos en el bar Espace Noir, en St. Imier, y aprendiendo un montón acerca de cómo las diferencias pueden ser pacíficamente armonizadas en un grupo de gente diversa.

Fue interesante que una de las “conclusiones” de las personas que participaron en el primero de los cuatro campamentos de verano dedicados al desarrollo de FairCoop, fue que fueron testigos del movimiento que hace falta para cambiar el mundo y alejarlo del actual sistema capitalista.
 
![](Summer-Camp-1.jpg)

**16 agosto – 23 agosto
Blockchain para mujeres**
Durante la tercera semana de agosto, tuvo lugar un encuentro de mujeres para aprender y reflexionar sobre esta nueva tecnología. Fue una gran oportunidad para conocerse, convivir y aprender tanto de Blockchain como de la necesidad de reajustar las expectativas de cada una cuando queremos avanzar en grupo hacia un objetivo común. Mujeres de diversas procedencias, conocimientos y expectativas, nos embarcamos en la tarea de autoorganizarse tanto a nivel académico como logístico, con diversos resultados. La experiencia fue ciertamente muy enriquecedora.

Además de la enorme y preocupante brecha de género y de clase que está generando esta nueva tecnología, aprendimos algunas de las enormes posibilidades que surgirán de la misma en un futuro inmediato. Complicado pero emocionante, entendimos cómo el ecosistema de FairCoop se sirve de esta oportunidad tecnológica para generar valor y apoyar la creación de un mundo más justo. También nos propusimos crear un espacio para el estudio del comercio criptográfico y sus complejidades, con el fin de contribuir a dicho ámbito desde una perspectiva femenina/feminista.

No menos importante fue darnos cuenta de que descuidar el cuidado mutuo y la generación consciente de espacios de intercambio y reflexión -sobre lo que queremos y cómo queremos hacerlo- nos lleva, una y otra vez, a formas de hacer que nos constriñen. Necesitamos un verdadero reflejo de lo que tenemos que hacer en nuestra vida diaria, si realmente queremos organizarnos desde abajo y cooperativamente – sin que nadie tenga que dirigir o supervisar a nadie-. También, si queremos que los espacios cooperativos realmente sean tales para que, a pesar de la cultura individualista y jerárquica en la que nos hemos criado, seamos capaces de crear espacios donde todas podamos dar lo mejor de nosotras mismas, y así cuidar de las personas y del planeta.

![](Summer-Camp-2.jpg)

**23 agosto – 6 septiembre
Trabajo abierto cooperativo del ecosistema FairCoop**

Este módulo estaba destinado a personas ya experimentadas en el ecosistema de FairCoop, y su objetivo era perfeccionar y desarrollar los protocolos y las formas de organización de FairCoop ya existentes, además de introducir algunas novedades.

El taller dio la bienvenida a Mont Soleil a gente de Suiza, Francia, Austria, Cataluña/España, Argentina, Grecia, Italia, Sudáfrica, Filipinas… y nos alegró especialmente tener algunas personas de Bakur, en Kurdistán (Estado turco), que vinieron para que aprendiésemos la forma de fusionar su propio estilo de cooperativismo autoorganizado con el nuestro.

El foco de este campamento de verano fue el desarrollo de cinco áreas de trabajo abierto cooperativo. Estas áreas son: Bienvenida, Comunicación, Gestión Común, Economía Circular y Tecnología. En cada una de ellas se creó una asamblea que ahora se ocupa de las prioridades de su competencia, definiendo las tareas que hay que realizar y que se van a remunerar, de una forma justa y abierta -simplificando la forma en la que cualquier participante del ecosistema tiene la oportunidad de ser recompensado en función del tiempo que dedique a realizar tareas útiles-.

![](Summer-Camp-3.jpg)

Se propuso un presupuesto común (posteriormente aprobado por la asamblea general de FairCoop de septiembre) para iniciar este innovador e inclusivo método de colaboración. La mayoría de las áreas también activaron roles de facilitadoras, que ayudarán en el desarrollo de las asambleas y las tareas, e introdujeron algunas subáreas, en busca de una estructura cooperativa abierta aún más descentralizada. También aprendimos sobre dinamización de asambleas y técnicas de scrum/agile, y sobre su aplicación a las nuevas áreas de FairCoop.

Nos concentramos bastante en la nueva área de Economía Circular, con la intención de aumentar la participación en FairCoop a nivel local, y esto parece haber tenido éxito, a juzgar por el número de nuevos nodos locales que se han formado desde el Summer Camp. También trabajamos mucho en las áreas de Bienvenida, Comunicación y Tecnología.

No podíamos haber dejado Jura sin pagar las cervezas en FairCoin en el fantástico bar Espace Noir, y la caminata de Mont Soleil a St. Imier ¡fue tan memorable como el regreso en el funicular!

Realmente sentimos que el Campamento de Verano fue una inyección masiva de energía al proyecto, y en él desbloqueamos algunos viejos asuntos que nos permitieron llevar todo a un nivel superior. Este proceso está en marcha desde entonces y pronto veremos los resultados. También fue muy divertido conocer a todas aquellas que hasta entonces sólo conocíamos “virtualmente”. FairCoop es construir confianza y comunidad, a diferencia de muchos otros “criptoproyectos” que se enfocan en el individualismo, por lo que era vital que pudiéramos interactuar cara a cara y en persona con el resto de la gente que está construyendo el proyecto. Hubo mucho trabajo de cocina y de limpieza, ¡y también una justa ración de baile y de risas!

![](Summer-Camp-4.jpg)

**7 septiembre – 22 septiembre
Desarrollando un modelo de gobernanza glocal**

Más allá de los pasos inmediatos para desarrollar la estructura organizativa de los proyectos de FairCoop, para la próxima fase de los próximos meses y los próximos años estuvimos buscando formas de escalar una cultura de gobernanza para la sociedad en general.  Una cultura de gobernanza que abrace la autonomía y que dé respuestas autorizadas a lo que colectivamente queremos consentir.

En esta parte del Summer Camp participaron personas de Suiza, Finlandia, España, Sudáfrica, Austria y Grecia. Nos centramos en recopilar las mejores prácticas de gobierno, como el Modelo de Constelación, la Sociocracia 3.0 y las asambleas abiertas al estilo Occupy.

La estructura de gobernanza resultante de estos talleres es una estructura que pretende ser paralela a las estructuras de gobernanza. No pretende sustituirlas, sino complementarlas. Sin embargo, tiene el poder inherente de transformarlas o de crear nuevas estructuras que dejen obsoletas a las viejas. Es de naturaleza glocal, translocal. Es descentralizada, impulsada por la participación interdependiente de grupos aleatorios de gente locales y translocales.

Esta cultura de gobernanza no toma decisiones. Su función es facilitar la concienciación del consenso actual en la sociedad. Una vez que surge la realización colectiva, no debería haber necesidad de tomar “decisiones”. No obstante, cuando se adoptan decisiones en otras estructuras de gobernanza paralelas, esta realización colectiva se tiene en cuenta como parte del acuerdo en la ruta escogida a seguir. Definimos formas de implementar dicha cultura de gobernanza glocal que pueden tomarse en consideración a medida que avancemos.

![](Summer-Camp-5.jpg)