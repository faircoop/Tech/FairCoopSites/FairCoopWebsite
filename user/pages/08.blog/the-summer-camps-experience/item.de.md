---
title: 'Erfahrungen aus den Summer-Camps'
media_order: 'portada_post_summercamp-882x6001.jpg,Summer-Camp-1.jpg,Summer-Camp-2.jpg,Summer-Camp-3.jpg,Summer-Camp-4.jpg,Summer-Camp-5.jpg'
summary:
    description: 'FairCoop is beginning to consolidate its new form of organization. Finally, thanks to the work of the last Summer Camps (in Décentrale, Mont Soleil, Switzerland, with the help of Synergiehub and SCI Schweiz), a new operating scheme has emerged, which we will detail in our next post.'
thumbnail_image:
    image: portada_post_summercamp-882x6001.jpg
date: '08-09-2017 17:29'
publish_date: '08-09-2017 17:29'
taxonomy:
    category:
        - blog
    tag:
        - camps
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

FairCoop ist gerade dabei seine neue Organisationsstruktur zu konsolidieren. Dank der Arbeit des letzten Sommercamps (in Décentrale, Mont Soleil, Schweiz, mit Hilfe vom Synergiehub und SCI Schweiz) ist ein neues Arbeitskonzept entstanden, das wir in unserem nächsten Beitrag noch einmal im Detail erläutern werden. Dort, im Jura, haben wir acht Wochen lang intensiv an verschiedenen Aspekten des FairCoop-Ökosystems gearbeitet. Zwei Monate des angenehmen Zusammenlebens und ein kontinuierlicher Fluss von Ideen, deren Entwicklung wir nachfolgend beschreiben.

 

**30. Juli – 13. August
Einführung in das FairCoop-Ökosystem. So legen Sie einen lokalen Knoten in Ihrer Region an**

Das erste FairCoop Camp führte Dutzende von jungen Menschen aus der ganzen Welt (Ukraine, Mexiko, Spanien, Tschechien, Russland, Italien, Äthiopien) in die Prinzipien der Permakultur, Selbstermächtigung, Synergie, Solidarität, Genossenschaften, alternative Wirtschaften im Allgemeinen, Bürgerjournalismus, demokratische Schulen usw. ein. Die Arbeit am Bau der Kuppel und der Jurte, das Holzhacken, sowie die Renovierung der Veranda waren ein wichtiger Teil des Lernens.

Gastgebern aufbauen mussten. Intensive Arbeit außerhalb und innerhalb des Hauses, in der Küche, Förderung von Vielfalt und vegetarischem Essen, Wochenendtrips zu Seen und Berghütten und Städten rund um den Jura waren Teil des Plans, dass sie während dieses Prozesses so viel wie möglich lernen und genießen sollten.

Die jungen Leute waren überrascht, dass alle gleichgestellt sind, in dem Sinne, dass es keine klassischen repräsentativen Autoritäten wie Besitzer, Führer, Lehrer gibt. Sie haben oft nicht sofort alles verstanden, was in de Workshops und Vorträge erklärt wurde. Die Einführungen in komplexe theoretische Studien und Praxis der Theorie über Themen wie Permakultur, Anarchie, Genossenschaften, Blockchain- und Kryptowährungsökonomie waren anfänglich sehr viel Information. Die Teilnehmer lernten langsam die Kommunikation über Telegramm kennen, installierten Faircoin-Wallets, bekamen Faircoins, gaben einige in der Bar Espace Noir in St. Immier aus und lernten viel darüber, wie Unterschiede in einer vielfältigen Gruppe von Menschen friedlich harmonisiert werden können.

Es war interessant, dass eine der Schlussfolgerungen der Teilnehmer an dem ersten von vier Sommercamps darin bestand, dass sie die notwendige von FairCoop erkannten die Welt vom derzeitigen kapitalistischen System weg zu verändern.
 
![](Summer-Camp-1.jpg)

**16. August – 23. August
Blockchain für Frauen**
In der 3. Augustwoche fand ein Frauentreffen statt, bei dem über diese neue Technologie nachgedacht und gelernt wurde. Es war eine großartige Gelegenheit, sich zu begegnen, zusammenzuleben und sowohl von Blockchain zu lernen, als auch die Notwendigkeit zu erkennen, die Erwartungen aller neu anzupassen, wenn wir in einer Gruppe auf ein gemeinsames Ziel hinarbeiten wollen. Frauen mit unterschiedlichen Hintergründen, Kenntnissen und Erwartungen begannen mit der Aufgabe, sich auf akademischer und logistischer Ebene selbst zu organisieren, mit unterschiedlichen Ergebnissen. Die Erfahrung war mit Sicherheit sehr bereichernd.

Zusätzlich zu der enormen und besorgniserregenden Kluft die diese neue Technologie zwischen den Geschlechtern und den Klassen erzeugt hat, haben wir einige der enormen Möglichkeiten erkannt, die sich in nächster Zukunft daraus ergeben werden. Kompliziert, aber spannend haben wir verstanden, wie das FairCoop-Ökosystem diese technologische Chance nutzt, um Werte zu generieren und die Schaffung einer gerechteren Welt zu unterstützen. Wir haben uns auch daran gemacht, einen Raum für die Erforschung des Krypto-Handels und seiner Feinheiten zu schaffen, um einen Beitrag aus weiblich-feministischer Perspektive zu leisten.

Nicht minder wichtig war die Erkenntnis aufeinander aufpassen zu müssen und bewusst Räume für Austausch und Reflexion zu schaffen – über das, was wir wollen und wie wir Dinge tun wollen. Eine wirkliche Widerspiegelung dessen, was wir in unserem täglichen Leben tun müssen, ist notwendig, wenn wir uns wirklich von unten und kooperativ organisieren wollen – ohne dass jemand anderes anleiten oder beaufsichtigen muss. Wir wollen, dass Kooperationsräume kooperativ sind. Trotz der individualistischen und hierarchischen Kultur in der wir aufgewachsen sind, sind wir durch diese Reflexiion in der Lage Räume zu schaffen, in denen wir alle das Beste aus uns selbst herausholen können, und uns so um die Menschen und den Planeten kümmern können.

![](Summer-Camp-2.jpg)

**23. August bis 6. September
FairCoop Ökosystem – offene kooperative Zusammenarbeit**

Dieses Modul war eher für diejenigen gedacht, die bereits Erfahrungen im FairCoop-Ökosystem gesammelt haben und hatte zum Ziel, die bereits  existierenden FairCoop-Protokolle und Organisationsformen zu verfeinern und weiterzuentwickeln, sowie einige neue Ergänzungen hinzuzufügen.

Der Workshop begrüßte Menschen aus der Schweiz, Frankreich, Österreich, Katalonien/Spanien, Argentinien, Griechenland, Italien, Österreich, Südafrika, den Philippinen… und wir waren besonders glücklich, dass einige Leute aus Bakur in Kurdistan (Türkischer Staat) gekommen waren, um ihren eigenen Stil der selbstorganisierten Genossenschaft mit unserem eigenen zu verschmelzen.

Der Schwerpunkt dieses Sommercamps lag auf der Entwicklung von fünf offenen Kooperationsfeldern. Diese Bereiche sind: Willkommen, Kommunikation, gemeinsames Management, Kreislaufwirtschaft und Technologie. In jedem der Gebiete wurde eine Versammlung geschaffen, die sich nun um die Prioritäten des Gebiets kümmert und die Aufgaben definiert, die auf faire und offene Weise erledigt und bezahlt werden müssen, um jedem Teilnehmer des Ökosystems die Möglichkeit zu geben, auf der Grundlage der Zeit, die er für die Erledigung nützlicher Aufgaben aufgewendet hat, eine Belohnung zu erhalten.

![](Summer-Camp-3.jpg)

Ein gemeinsames Budget wurde vorgeschlagen (und später in der September-Versammlung von FairCoop genehmigt), um diese innovative und integrative Methode der Zusammenarbeit zu beginnen. Die meisten Bereiche aktivierten auch Rollen von Moderatoren, die bei der Entwicklung der Versammlungen und Aufgaben behilflich sein werden, und einige Teilbereiche wurden ebenfalls eingeführt, um sich auf eine noch dezentralere offene Kooperationsstruktur vorzubereiten. Wir lernten auch das Dynamisieren von Assemblies und Scrum/Agil-Techniken und wandten diese auf die neu formierten Bereiche von FairCoop an.

Wir haben uns sehr stark auf die neue Circular Economy Area konzentriert, um die Beteiligung an FairCoop auf lokaler Ebene zu erhöhen, und das scheint gelungen zu sein, wenn man die Anzahl der neuen Local Nodes betrachtet, die seit dem Sommercamp entstanden sind. Wir haben auch viel an der Welcome Area, den Bereichen Medien und Kommunikation sowie Technik gearbeitet.

Wir hätten den Jura nicht verlassen können, ohne in der fantastischen Espace Noir-Bar für ein Bier in Faircoin zu bezahlen. Der Abstieg vom Mont Soleil nach St. Imier war genauso ein Höhepunkt wie der Rückweg mit der Luftseilbahn!

Wir sind der festen Überzeugung, dass das Sommercamp eine massive Energieschub in das Projekt war, und wir haben einige seit langem bestehende Probleme gelöst, was es uns ermöglicht, das Ganze auf eine andere Ebene zu heben. Dieser Prozess ist seitdem im Gange und wir werden bald die Ergebnisse sehen. Es hat auch viel Spaß gemacht, alle kennenzulernen, die wir bis dahin nur‘ virtuell‘ gekannt haben. FairCoop ist im Gegensatz zu vielen anderen Krypto-Projekten, die sich auf den Individualismus konzentrieren, auf Vertrauen und Gemeinschaft ausgerichtet. Daher war es für FairCoop von entscheidender Bedeutung, persönlich und von Angesicht zu Angesicht mit den anderen Menschen, die das Projekt aufbauen, interagieren zu können. Es wurde viel gekocht und geputzt, und auch ein bisschen getanzt und gelacht!

![](Summer-Camp-4.jpg)

**7. September – 22. September
Entwicklung eines Glocal Governance-Modells**

Über die unmittelbare vor uns liegenden Schritte hinaus, um die Organisationsstruktur der FairCoop-Projekte für die nächste Phase der nächsten Monate und Jahre weiterzuentwickeln, haben wir nach Wegen gesucht, wie wir eine neue Governance-Kultur in die Gesellschaft hinein skalieren können. Eine Governance-Kultur, die Autonomie umfasst und autoritative Antworten auf das gibt, was wir gemeinsam wollen.

Menschen  aus der Schweiz, Finnland, Spanien, Südafrika, Österreich und Griechenland waren an diesem Teil des Sommercamps beteiligt. Wir konzentrierten uns auf die Sammlung verschiedener Best Practices im Bereich Governance, wie z. B. das Konstellationsmodell, Sociocracy 3.0 und Offene Assemblies im Occupy-Stil.

Die daraus resultierende Governance-Struktur, die aus diesen Workshops hervorgegangen ist, zielt darauf ab, parallel zu den bisherigen Governance-Strukturen zu co-existieren. Sie soll diese nicht ersetzen. Sie hat jedoch die Kraft, sie zu transformieren oder neue Strukturen zu schaffen, die alte überflüssig machen. Die neuen  Strukturen beziehen sich auf natürliche glokale und translokal Gegebenheiten. Sie ist dezentralisiert und werden durch die wechselseitige Beteiligung von lokalen und translokalen Zufallsgruppen angetrieben.

Diese Governance-Kultur trifft keine Entscheidungen. Ihre Aufgabe ist es, das Bewusstsein für den aktuellen gesellschaftlichen Konsens zu fördern. Sobald die kollektive Erkenntnis erst einmal entstanden ist, sollte es keinen Grund mehr geben,“Entscheidungen“zu treffen. Bei Entscheidungen in anderen parallelen Governance-Strukturen wird diese kollektive Umsetzung jedoch im Rahmen der Vereinbarung des gewählten Weges berücksichtigt. Wir haben Wege definiert, wie wir eine solche glokale Governance-Kultur umsetzen können, die bei unseren Fortschritten berücksichtigt werden können.

![](Summer-Camp-5.jpg)