---
title: 'FairCoin Migration successful!'
media_order: 'hurraylaunch2017-07-18.png,poclaunch_foto7.png,applause.png,poclaunch_foto10.png,poclaunch_foto8.png,poclaunch_foto6.png,poclaunch_foto5.png,poclaunch_foto3.png,poclaunch_foto11.png'
thumbnail_image:
    image: hurraylaunch2017-07-18.png
main_image:
    heading: 'Migration successful!'
    image: poclaunch_foto5.png
date: '18-07-2017 22:45'
taxonomy:
    category:
        - blog
    tag:
        - faircoin
        - software
        - launched
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
image:
    thumbnail_image: hurraylaunch2017-07-18.png
---

On July 18th, 2017 at 18:07 CEST all coins have been transferred from the old to the new blockchain. [Block 160 of the blockchain](https://chain.fair.to/block?block=160) shows this specific payload (in 14 transactions). Now we run the first Proof-of-Cooperation blockchain!  7 chain admins and 13 CVNs have been added, 3 more are pending.

[Bittrex](https://cryptrader.com/charts/bittrex/fair/btc) was following our change and accepted sells and buys again after 19:00.

Here is the log of the [upgrade announcement channel](https://t.me/FairCoinLaunchUpdates) we have been going through that day:


    Creating genesis block now.

    The blockchain has been started.
    Committing genesis block shortly.

    Genesis block committed.
    Now doing test build before creating release branch.

    Release created on https://github.com/faircoin/faircoin.git
    GitHub
    faircoin/faircoin
    faircoin - FairCoin2 Core working tree
    Creating binary releases for the Core wallet now.

    Built for: x86_64, i686
    Next: ARM, WIN32, WIN64, Mac OS X

    Now last build for Mac OS X.
    Builds are finished, now PGP signing the binaries and uploading them to the server.
    Wallets can now be downloaded from:
    https://download.faircoin.world/

    NOTE: these wallets auto-migrate your FariCoin1 wallet.dat. But you will not be able to see the balance until later today at 18:00 CEST (16:00 GMT).
    download.faircoin.world
    FairCoin wallet download world
    The official download site for all kinds of FairCoin wallets.
    Channel photo updated

    Block explorer is updated:
    [chain.fair.to](https://chain.fair.to/)
    FairCoin Explorer
    FariCoin2 Block Explorer & Statistics. Detailed information on blocks and transactions.

    First chain admin added to the network.

    All chain admins have successfully been added to the blockchain.
    Next we add the CVNs on by one.

    The FairCoin1 blockchain has come to an end!
    The money supply has been created in the FairCoin2 blockchain by the chain admins.
    In 45 min. all the balances are going to re-distributed to the original addresses in a 1:1 manner.

    16:09 All balances are re-distributed now!
    FairCoin2 can now be considered LAUNCHED!

    19:00 Current status:

    *     - FairCoin2 Core wallet is deployed
    *     - All coins have bee re-distribute
    *     - Android wallet is ready but will be deployed to tomorrow
    *     - Electrum wallet needs to be finished and is going to be deployed most likely tomorrow or the day after tomorrow.

Meanwhile, all new wallets are available [here.](https://download.faircoin.world/)

![](poclaunch_foto3.png)![](poclaunch_foto5.png)![](poclaunch_foto7.png)![](poclaunch_foto6.png)![](poclaunch_foto8.png)![](poclaunch_foto10.png)![](poclaunch_foto11.png)![](applause.png)