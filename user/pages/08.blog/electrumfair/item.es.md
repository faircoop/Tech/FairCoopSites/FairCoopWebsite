---
title: 'Actualiza ahora tu monedero ElectrumFair'
media_order: electrumfair1_0.jpg
date: '17-02-2018 17:34'
taxonomy:
    category:
        - blog
    tag:
        - carteras
        - aplicaciones
---

ElectrumFair es uno de los monederos disponibles para FairCoin. Su uso es sencillo y, como cualquier otro monedero de FairCoin, es muy seguro. **Se acaba de lanzar la nueva versión mejorada 3.0.5 y te recomendamos que actualices la versión anterior.  En concreto, si todavía estás usando la versión 2.7.1 de ElectrumFair o alguna anterior, debes actualizarla sin dudarlo.**

En enero de 2018 se detectó un bug en todos los monederos Electrum, que fue corregido en la versión 2.8.4. La nueva actualización a 3.0.5 también incorpora la corrección de un problema menor. El desarrollador principal de FairCoin, Thomas König, lo explica de la siguiente manera: "Se trataba de una cuestión que creó transacciones con múltiples firmas. El tamaño y, por tanto, las tarifas, se calculaban de forma incorrecta, de manera que la red no aceptaba las transacciones. El fallo ha sido corregido en esta versión."

Si todavía no tienes tu monedero ElectrumFair, esta será también la versión que tendrás que instalar a partir de ahora, en caso de que quieras probarla. Es normal tener más de un monedero para FairCoin, ya que se puede dedicar uno a los gastos habituales y otro para guardar mayores cantidades; o destinar uno para uso personal y otro para alguna actividad comercial, por ejemplo. Si aún no conoces el ElectrumFair, estas son algunas de sus características:

* ** Es un monedero ligero:**
 Esto significa que apenas necesita recursos del dispositivo para funcionar, de forma que ocupa muy poco espacio en el disco del ordenador o en la memoria o la tarjeta SD del móvil.  

* **Seguridad:**
 Tu clave privada consiste en 12 palabras, llamadas "seed" o "semilla", que te serán proporcionadas por el propio programa y que debes apuntar y guardar (preferiblemente en un lugar distinto del propio dispositivo en el que lo instales), porque son las que te permitirán recuperar el monedero e instalarlo de nuevo, con tu saldo actual y el registro de tus transacciones, en caso de que se estropee tu ordenador o tu móvil o pase cualquier cosa que requiera que necesites recuperarlo.

 Recuerda: Es importante que guardes estas palabras en un sitio seguro y que no las pierdas.

 A partir esta clave privada maestra, como en los otros monederos, se pueden generar distintas direcciones para recibir FairCoin.

* **Se puede instalar en distintos dispositivos:**
 ElectrumFair está disponible para Linux, Windows, MacOS y Android, y puedes instalarlo en varios dispositivos a la vez, si así lo quieres. Si usas la misma semilla para todos, tendrás un único monedero sincronizado y lo que hagas en uno se reflejará en los otros. Si utilizas semillas diferentes, cada monedero será independiente.

**Actualizarlo o instalarlo te resultará muy sencillo. Únicamente tienes que descargar el archivo adecuado para tu sistema operativo en [https://download.faircoin.world](https://download.faircoin.world) y ejecutarlo.  **

* Si lo que quieres es simplemente actualizar, basta con que escojas la opción “I already have a seed” (ya tengo una semilla) cuando lo requiera el programa.
* Si quieres instalarlo desde cero, escoge la opción “create new seed” (generar semilla).

Si tienes cualquier duda a la hora de actualizar o instalar ElectrumFair 3.0.5, no dudes en ponerte en contacto con nosotras en los grupos de Telegram o pregunta en tu nodo local más cercano.

Una vez instalado o actualizado, tendrás un monedero FairCoin ligero, seguro y muy práctico. ¡Esperamos que lo disfrutes!