---
title: Organização
media_order: organization.864f291a.jpg
taxonomy:
    category:
        - test
    tag:
        - tag
        - tags
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

As comunidades da FairCoop organizam-se através da cooperação aberta. Todo mundo é encorajado a participar com as próprias habilidades individuais e ajudar o movimento a crescer.

Ao passo que trabalhamos globalmente para apoiar a descentralização e a economia local, nossa abordagem segue um modelo de organização 'glocal' (uma combinação de global e local):

## Área de Trabalho Global (Open Coop Work)
Refere-se principalmente ao trabalho online, com o objetivo de estruturar, organizar e desenvolver informações e tecnologia para a rede global. Todas as tarefas são organizadas em nossa plataforma colaborativa aberta, a Open Coop Work (OCP). Os participantes podem atuar como voluntários ou solicitar remuneração em FairCoin pelo trabalho que precisa ser feito.

As áreas de trabalho são:
* Administração geral
* Comunicação
* Economia Circular
* Desenvolvimento de tecnologias
* Recepção/Educação/Suporte

[Saiba mais sobre as estruturas de trabalho globais](https://www.mindomo.com/mindmap/free-mind-map-ffa5505555b54101960e3138e13b5e89)

## Área de Trabalho Local
Refere-se a atividades online e offline de cada Nodo Local para desenvolver a rede física em campo, com foco no trabalho regional individual. Dentre as atividades da área de trabalho local, temos:
* Dinamização da economia circular (apoio a comerciantes)
* Promoção das várias ferramentas da FairCoop
* Gerenciamento de ponto de troca (PoE)
* Promoção dos valores e práticas da FairCoop para projetos locais
* Criação local de projetos específicos no ecossistema da FairCoop

** Cada Área de Trabalho Global e Nodo Local tem autonomia para tomar decisões através de assembleias, a fim de executar as tarefas necessárias e atingir os próprios objetivos da melhor maneira possível. Não é necessário, portanto, pedir permissão a nenhuma autoridade centralizada. No entanto, assuntos chaves que dizem respeito a toda a comunidade devem ser decididos na assembleia global geral por consenso, como sempre. **

** Todas as áreas de trabalho globais e nodos locais têm diversos grupos de bate-papo e subáreas para comunicação e coordenação. **

Se você tiver interesse em participar, entre em contato direto conosco por meio do nosso [grupo de boas-vindas](https://t.me/askfaircoop). A partir daí, podemos ajudar você a chegar ao local que melhor se adapta aos seus interesses e intenções.


## Rede de Nodos Locais
Rede de grupos que representam e expandem a FairCoop a nível local. Eles desenvolvem e ampliam a rede regional de forma independente: promovem as ferramentas da FairCoop para lojistas e indivíduos, dão suporte técnico, fazem apresentações, respondem a todos os tipos de perguntas e até mesmo iniciam projetos locais individuais no âmbito dos princípios da FairCoop. Os nodos locais podem receber pequenos financiamentos para cobrir os custos básicos, com a intenção de chegar à autossuficiência a longo prazo.

Para estabelecer um nodo local, são necessários pelo menos três participantes ativos. Encorajamos a todos os interessados ??a entrar em contato direto com as pessoas em algum nodo da região onde moram. Veja mais detalhes no [mapa dos Nodos Locais] (http://map.fairplayground.info/). Se não conseguir encontrar um nodo na sua região, teremos o prazer de ajudar você a começar um grupo.

[Veja mais detalhes no guia para nodos locais](https://git.fairkom.net/faircoop/MediaCommunication/wikis/como-criar-nodos-locais-(pt))