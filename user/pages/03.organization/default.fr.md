---
title: Organisation
media_order: organization.864f291a.jpg
taxonomy:
    category:
        - test
    tag:
        - tag
        - tags
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

La communauté FairCoop s'organise à travers un coopérativisme ouvert. Tout le monde est encouragé à participer avec ses compétences individuelles et à aider le mouvement à grandir.

Comme nous travaillons globalement pour soutenir la décentralisation et l'économie locale, notre approche consiste à suivre un modèle d'organisation glocal (global et local combiné):

Les domaines d'activité au niveau global
se réfèrent principalement aux activités en ligne, visant à structurer, organiser et développer l'information et la technologie pour le réseau global. Toutes les tâches sont organisées dans notre plateforme collaborative ouverte (OCP). Les participants peuvent soit travailler sur base de bénévolat ou demander une rémunération en FairCoins pour le travail presté.

Nos domaines d'activité au niveau global sont :
* La gestion commune
* La communication
* L'économie circulaire
* Le développement et la technologie
* L'accueil-l'éducation-le soutien

[En savoir plus sur notre structure du niveau global Ici](https://www.mindomo.com/mindmap/free-mind-map-ffa5505555b54101960e3138e13b5e89)

## L'activité des nœuds locaux
fait référence aux activités en ligne et hors ligne du nœud local correspondant pour développer le réseau physique sur le terrain en mettant l'accent sur l'activité régionale individuelle. Certains domaines de nœuds locaux peuventt être:
* La dynamisation de l'économie circulaire (soutien aux commerçants)
* La promotion des différents outils de FairCoop
* La gestion d'un point d'échange (PoE)
* La promotion des valeurs et des pratiques de FairCoop auprès des projets locaux
* La construction de projets locaux spécifiques au sein de l'écosystème Faircoop

Chaque domaine d'activité global et chaque nœud local a une autonomie de décision interne, via des assemblées, pour accomplir leurs tâches et atteindre leurs objectifs de la meilleure façon possible. Par conséquent, des autorisations centralisées ne sont pas nécessaires. Cependant, les questions clés, qui concernent l'ensemble de la communauté, sont décidées par l'assemblée générale mondiale, toujours par consensus.

Tous les domaines d'activité globaux et les réseaux de nœuds locaux ont divers groupes de discussion et sous-groupes pour la communication quotidienne et la coordination.

Si vous souhaitez participer, rejoignez notre [groupe d'accueil](https://t.me/askfaircoop) et prenez contact directement avec nous. Nous pourrons alors vous rediriger vers le groupe qui correspond le mieux à vos intérêts et intentions.

## Réseau de nœuds locaux
Les nœuds locaux sont les groupes de représentation régionaux et d'expansion de FairCoop au niveau local. Ils développent et élargissent le réseau régional de manière indépendante en promouvant les outils FairCoop auprès des commerçants et des particuliers, en leur apportant un support technique, en organisant des présentations, en répondant à toutes sortes de questions ou même en démarrant leur projet de nœuds locaux dans le cadre des principes FairCoop. Les nœuds locaux peuvent recevoir de petits financements pour couvrir les coûts de base dans le but d'être autosuffisants au fil du temps.

Pour établir un nœud local, au moins 3 participants actifs sont nécessaires. Si vous êtes intéressés par FairCoop, nous vous encourageons à entrer directement en contact avec les responsables du nœud local de votre région. Vous pouvez trouver la carte des nœuds locaux [ici:](http://map.fairplayground.info/). Si vous ne trouvez pas de nœud local dans votre région, nous sommes à votre disposition pour vous aider à en créer un.

<a class="block-link" href="https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D">Plus de détails dans le guide des nœuds locaux ici.</a>