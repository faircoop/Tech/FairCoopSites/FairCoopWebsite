---
title: Organizació
media_order: organization.864f291a.jpg
taxonomy:
    category:
        - test
    tag:
        - tag
        - tags
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

La comunitat FairCoop s'organitza a través del cooperativisme obert. S’anima a tothom a participar amb les seues habilitats individuals i així ajudar al moviment a créixer.

Com que treballem globalment per a donar suport a la descentralització i l'economia local, el nostre plantejament és seguir un model d'organització glocal (combinació dels àmbits global i local):

## Treball Global
Les Àrees de Treball Global es refereixen principalment al treball en línia (online), orientat a estructurar, organitzar i desenvolupar informació i tecnologia per a la xarxa global. Totes les tasques s'organitzen a la nostra plataforma de col·laboració oberta (OCP). Les persones participants poden optar per aportar voluntàriament les seues hores de treball o sol·licitar una remuneració en FairCoins pel treball que cal fer.

Les nostres àrees de Treball Global (OCW-Open Coop Work) són:
* Gestió comuna
* Comunicació
* Economia Circular
* Desenvolupament Tecnològic
* Benvinguda-Educació-Cures

[Més informació aquí](https://www.mindomo.com/mindmap/free-mind-map-ffa5505555b54101960e3138e13b5e89)

## Treball dels Nodes Locales
El treball dels Nodes Locals es refereix tant a activitats en línia com fora de línia (online i offline) que realitza cadascun dels nodes locals per a desenvolupar una xarxa física sobre el terreny, centrant-se en el treball regional individual. Algunes competències dels nodes locals podrien ser:
* Dinamitzar l'economia circular (suport a comerciants)
* Promoure les diferents eines FairCoop
* Gestionar un Punt d'Intercanvi (PoE - Point of Exchange*)
* Promoure els valors i les pràctiques de FairCoop entre els projectes locals
* Construir projectes locals específics dins de l'ecosistema Faircoop

Cada àrea de treball global i cada node local tenen autonomia per a la presa de decisions internes a l'hora de dur a terme les seues tasques i de dur a terme els seus objectius de la millor manera possible. D'aquesta manera, no són necessaris permisos centralitzats. No obstant això, els assumptes clau, que afecten tota la comunitat, es decideixen en l'assemblea general global, com sempre, per consens.

Si t'interessa participar en ells, per favor, uneix-te al nostre [grup de Benvinguda](https://t.me/askfaircoop) i posa't en contacte directe amb nosaltres. Des d'allí, podrem redirigir-teal lloc més apropiat en funció dels teus interessos i intencions.

## Xarxa de Nodes Locals
Els nodes locals són els grups de representació regionals i d'expansió de FairCoop en l'àmbit local. Desenvolupen i amplien la xarxa regional de forma independent promovent les eines de FairCoop a botigues i particulars, donant suport tècnic, fent presentacions, responent a tot tipus de dubtes i, fins i tot, iniciant els seus propis projectes locals en el marc dels principis de FairCoop. Els nodes locals poden rebre xicotets finançaments per a cobrir els costos bàsics amb la intenció de què siguen autosuficients amb el temps.

Per a establir un node local són necessaris almenys tres participants actives. Animem a totes les persones que estiguen interessades a contactar directament amb les persones del node local de la vostra regió. Pots trobar el mapa dels nodes locals amb més detalls [aquí.](http://map.fairplayground.info/). Si no trobes un node local a la teua zona, estarem encantades d'ajudar-te i donar-te el suport perquè crees un.

<a class="block-link" href="https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D">Pots Trobar Més Detalls A La Guia De Nodes Locals Aquí.</a>