---
title: 'Rete e organizzazione'
media_order: organization.864f291a.jpg
taxonomy:
    category:
        - test
    tag:
        - tag
        - tags
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

La comunità di FairCoop si autorganizza attraverso il cooperativismo aperto. Tutti sono invitati a partecipare con le proprie abilità individuali alla crescita del movimento.

Dato che stiamo lavorando globalmente per sostenere una decentralizzazione del potere e un'economia locale, abbiamo le competenze per conseguire un modello di organizzazione GloCale

## Le aree di lavoro globale
Le Aree di lavoro globale si riferiscono principalmente al lavoro che si pone l'obiettivo di strutturare, organizzare, amministrare e sviluppare le informazioni e la tecnologia per la rete globale

Le aree di lavoro sono:
* Gestione del comune
* Comunicazione
* Economia circolare
* Tecnologia e sviluppo
* Accoglienza - educazione - supporto


<a class="block-link" href="https://www.mindomo.com/mindmap/free-mind-map-ffa5505555b54101960e3138e13b5e89">Ulteriori Informazioni Sulla Struttura Dell'area Di Lavoro Globale Qui</a>

## Area dei nodi locali
L'Area dei nodi locali si riferisce sia al lavoro online sia a quello sul territorio operato dai vari nodi locali al fine di sviluppare la rete concreta sul territorio, con le proprie strategie e prospettive di lavoro specifiche. Alcuni tipi di lavoro all'intero dei nodi locali possono essere:

* Favorire l'economia circolare (appoggio ai commercianti)
* Promuovere gli strumenti di FairCoop
* Gestire un Punto di Scambio (POE)
* Promuovere i valori e le pratiche di FairCoop ai progetti locali
* Costruzione di progetti specifici locali all'interno dell'ecosistema di FairCoop

Ogni Area di Lavoro Globale e ogni Nodo Locale hanno la totale autonomia nel prendere le decisioni interne, attraverso il modello assembleario, che viene usato come strumento per i processi decisionali , per suddividere i compiti e fare in modo di raggiungere gli obiettivi al meglio. Per questo motivo, non c'è bisogno di avere permessi dal centro. Le questioni chiave, che riguardano tutta la comunità, si decidono nell'assemblea generale, come sempre, per consenso**

Tutte le Aree di Lavoro globali e locali si organizzano tramite diversi gruppi e sottogruppi su Telegram per la comunicazione e il coordinamento giornaliero. Se sei interessato/a a partecipare, unisciti al nostro [gruppo accoglienza](https://t.me/askfaircoop) ed entra in contatto diretto con noi. Da lì, saremo in grado di metterti in contatto con il gruppo che meglio si adatta ai tuoi interessi o competenze.

## I nodi locali
I Nodi locali sono dei gruppi di rappresentanza regionale, e l'espansione di FairCoop a livello locale. Essi sviluppano la rete regionale in modo indipendente promuovendo gli strumenti di FairCoop ad individui, negozi ed attività in genere, offrendo supporto tecnico, organizzando presentazioni, rispondendo a tutte le domande o anche iniziando progetti affini ai principi di FairCoop nell'ambito dei singoli nodi locali. I Nodi locali possono ricevere finanziamenti per coprire i costi di base, rimane comunque l'intenzione di essere autosufficienti col passare del tempo.

Per creare un nodo locale, sono necessari almeno 3 partecipanti attivi. Invitiamo tutti coloro che sono interessati, a entrare in contatto diretto con le persone del nodo locale nella propria regione. È possibile trovare la mappa dei Nodi Locali, con ulteriori dettagli [qui:](http://map.fairplayground.info/). Se non trovate un nodo locale nella vostra zona, siamo felici di aiutare e sostenere gli interessati alla creazione di un nuovo nodo.


<a class="block-link" href="https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D">Puoi Trovare Maggiori Dettagli Qui.</a>