---
title: Organización
media_order: organization.864f291a.jpg
taxonomy:
    category:
        - test
    tag:
        - tag
        - tags
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

La comunidad FairCoop se organiza a través del cooperativismo abierto. Se anima a todo el mundo a participar con las habilidades que tenga y a contribuir a que el movimiento crezca.

Como trabajamos globalmente para apoyar la descentralización y la economía local, nuestro planteamiento es seguir un modelo de organización glocal (combinación de los ámbitos global y local):

## Áreas de Trabajo Global
Se refieren principalmente al trabajo online, orientado a estructurar, organizar y desarrollar información y tecnología para la red global. Todas las tareas se organizan en nuestra plataforma abierta colaborativa (OCP-Open Collaborative Platform). Las personas que participen pueden trabajar de forma voluntaria o solicitar una remuneración en FairCoin por el trabajo que haya que hacer.

Nuestras Áreas de Trabajo Global (OCW-Open Coop Work) son:
* Gestión Común
* Comunicación
* Economía Circular
* Desarrollo Tecnológico
* Bienvenida-Educación-Cuidados

[Más información aquí](https://www.mindomo.com/mindmap/free-mind-map-ffa5505555b54101960e3138e13b5e89)

## Trabajo de los Nodos Locales
Se refiere a las actividades online y offline que realiza cada uno de los nodos locales para desarrollar una red física sobre el terreno, centrándose en el trabajo regional individual. Algunas competencias de los nodos locales podrían ser:

* Dinamizar la economía circular (apoyo a los comercios)
* Promover diferentes herramientas de FairCoop
* Gestionar un Punto de Intercambio (PoE-Point of Exchange)
* Promover los valores y las prácticas de FairCoop entre los proyectos locales
* Construir proyectos locales específicos dentro del ecosistema FairCoop

Cada área de trabajo global y cada nodo local dispone de autonomía para la toma de decisiones internas a la hora de llevar a cabo sus tareas y de cumplir sus objetivos de la mejor forma posible. De esta manera, no son necesarios permisos centralizados. No obstante, los asuntos clave que afectan a toda la comunidad se deciden en la asamblea general global, como siempre, por consenso. Todas las áreas de trabajo local y todos los nodos locales disponen de distintos chats y subáreas para su comunicación diaria y su coordinación.

Si te interesa participar en ellos, por favor, únete a nuestro [grupo de bienvenida](https://t.me/askfaircoop) y ponte en contacto directo con nosotras. Desde allí, podemos redirigirte al lugar más apropiado en función de tus intereses e intenciones.

## Red de Nodos Locales
Los nodos locales son los grupos de representación regional de FairCoop a nivel local. Ellos desarrollan y amplian la red regional de manera independiente, promoviendo las herramientas de FairCoop entre las tiendas y las personas de su localidad, proporcionándoles apoyo técnico, haciendo presentaciones, respondiendo todo tipo de dudas e incluso iniciando sus propios proyectos de nodo local dentro del marco de los principios de FairCoop. Los nodos locales pueden recibir una pequeña financiación para cubrir sus costes básicos, con la idea de que, con el tiempo, se hagan autosuficientes.

Para crear un nodo local se necesitan, al menos, tres participantes activas. Desde aquí, animamos a todas las personas interesadas a que entren en contacto directo con el nodo local de su región. Puedes encontrar el mapa de nodos locales con más detalles [aquí.](http://map.fairplayground.info/) Si no encuentras un nodo local en tu zona, estaremos encantadas de ayudarte y apoyarte para que crees uno.

<a class="block-link" href="https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D">Pueden Encontrarse Más Detalles En La Guía De Nodos Locales Aquí.</a>