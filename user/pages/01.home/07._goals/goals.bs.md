---
title: '# Neke od glavnih prekretnica koje smo postigli'
---

* Već napravljena i u upotrebi, inovativna, sigurna i ekološka valuta (FairCoin)
* Započeta brzo-rastuća globalna mreža lokalnih čvorova
* Otvoren Evropski kooperativni okvir (FreedomCoop)
* Suosnivač Kooperativne Banke (Bank of the Commons)
* Izgrađeno online tržište (FairMarket) i Otvorena saradnička platforma(OCP)
* Nismo se (još) poubijali ;)