---
title: '# Alguns dos marcos que já conquistamos'
---

* Criamos e usamos uma moeda inovadora, segura e ecológica: a FairCoin
* Começamos uma rede global cada vez maior de nodos locais
* Criamos a estrutura para uma cooperativa europeia: Freedom Coop
* Cofundamos uma cooperativa bancária: Bank of the Commons
* Criamos um mercado online – Fair Market – e uma plataforma de trabalho colaborativa – OCP
* Não nos matamos uns aos outros (ainda) ;)