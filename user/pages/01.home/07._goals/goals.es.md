---
title: '# Algunos de los principales logros que hemos conseguido hasta ahora'
---

* Hemos creado y estamos usando una moneda (FairCoin) innovadora, segura y ecológica
* Hemos iniciado una red global de nodos locales que crece rápidamente
* Hemos abierto una cooperativa europea (FreedomCoop)
* Hemos cofundado un banco cooperativo (Bank of the Commons)
* Hemos construido un mercado online (FairMarket) y una plataforma de trabajo colaborativa (OCP)
* No nos hemos matado las unas a las otras (todavía) ;)