---
title: '# Pêşketina me heta niha'
---

* Diravê nû, ewle, ekolojîk çêdike û her roj bikartînin (FairCoin)
* Tora cîhaniya nodên heremî ku zû mezin dibe destpêkir
* Çarçoveyeke koperatîva Ewropî vekiriye (FreedomCoop)
* Banka koperativ ya hevbeşan avakiriye (Bank of the Commons)
* Sûkekî ser internetê (FairMarket) û platforma hevkariyê (OCP) avakiriye
* Hêj me hevdu ne dayî ;)