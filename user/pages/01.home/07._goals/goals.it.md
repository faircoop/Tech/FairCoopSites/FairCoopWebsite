---
title: '# Alcune delle tappe principali che abbiamo raggiunto finora'
---

* Creazione ed utilizzo di una moneta innovativa, sicura ed ecologica (FairCoin)
* Abbiamo dato il via ad una rete globale di nodi locali in rapida crescita
* Abbiamo fondato un contesto cooperativo europeo (FreedomCoop)
* Abbiamo cofondato una banca cooperativa (Bank of the Commons)
* Abbiamo costruito un mercato online ed una piattaforma di lavoro collaborativo
* Non ci siamo uccisi l'un l'altro (per ora) ;)