---
title: Nodes
buttons:
    -
        text: 'Pogledajte rastuću mrežu'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong> # Lokalni Čvorovi </strong></h2>

Osim globalne vizije, mnogobrojni Lokalni Čvorovi su dodati u mrežu FairCoop-a po cijelom svijetu, u cilju spajanja lokalnih inicijativa, grupa i ljudi u ekosistem. To je dvosmjerni odnos koji podržava i realizuje ciljeve FairCoop-a u lokalnim zajednicama omogućavajući važan povrat informacija. Ovo je zajednička mreža pomoći izgrađena da proširi naš ekonomski sistem na globalnom nivou kroz izgradnju povjerenja i podržavanja stvarne ekonomske aktivnosti.