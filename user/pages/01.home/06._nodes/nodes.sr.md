---
title: '# Lokalne petlje'
buttons:
    -
        text: 'Pogledajte mrežu koja se razvija'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong> # Lokalne petlje</strong></h2>

Pored globalne vizije, Faircoop mreži se priključilo mnoštvo lokalnih petlji širom sveta, sa ciljem da se ovim ekosistemom povežu lokalne inicijative, grupe i ljudi. Ovo je obostrano korisna veza, u kojoj se shvataju i podržavaju ciljevi FairCoop-a u okviru lokalnih zajednica, a istovremeno obezbeđuju važne povratne informacije. To je mreža za međusobnu pomoć, izgrađena da bi se na globalnom nivou proširio naš ekonomski sistem, kroz izgradnju lokalnog poverenja i podršku istinskim ekonomskim aktivnostima.