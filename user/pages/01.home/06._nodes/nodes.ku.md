---
title: Nodên
buttons:
    -
        text: 'Tor ku mezin dibe mêzeke'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong> # Nodên Heremî</strong></h2>

Derve vîzyona cîhanî, gelek Nodên Heremî ji tora FCê re tên zêdekirin li seranserê cîhanê û evane ji bo serkêşiyên heremî, grup û mirovan têkilî bi vê ekosîstemê re bikin erk digrin û bihêz dikin. Ev pêwendiyek berbiçave ji bo armancên FC raste rast di komînan de pêkbîne. Ew torê alîkariya hevpeymanî ye, hatiye avakirin ji bo sîstema aboriya me belavbikin di asta cîhanî, di asta avakirina baweriya heremî û piştgiriya çalakiya aborî ya rast de ye.