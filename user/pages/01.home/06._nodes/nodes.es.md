---
title: Nodes
buttons:
    -
        text: 'Échale un vistazo a esta red creciente'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong> #Nodos locales </strong></h2>

Aparte de la visión global, múltiples nodos locales se están sumando a la red FairCoop en todo el mundo para conectar al ecosistema iniciativas, grupos y gente local. Se trata de una relación bidireccional que apoya y hace posible el logro de las metas de FairCoop en las comunidades locales, al tiempo que recibe un importante feedback por parte de éstas. Es una red de ayuda mutua construida para expandir nuestro sistema económico a una escala global, mediante la construcción de la confianza local y del apoyo a la actividad económica real.