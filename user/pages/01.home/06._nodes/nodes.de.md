---
title: Nodes
buttons:
    -
        text: 'Eine Karte des weltweiten Netzwerks'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong> # Lokale Knoten </strong></h2>

Neben den globalen digitalen Strukturen werden weltweit mehr und mehr lokale Knotenpunkte in das FairCoop-Netzwerk aufgenommen, um das Ökosystem mit lokalen Initiativen, Gruppen und Individuen in Kontakt zu bringen und untereinander zu verbinden. Die globale FairCoop Community unterstützt die lokalen Knoten bei der Verwirklichung ihrer Ziele. Die lokalen Knoten wiederum geben wichtige Rückmeldungen und Erfahrungen zurück an das globale Netzwerk. Es handelt sich um ein symbioses Verhältnis, das auf gegenseitiger Hilfe und Vertrauen beruht, und somit das kooperative Wirtschaftssystem weltweit verbreitet.