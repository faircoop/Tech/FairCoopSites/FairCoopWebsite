---
title: Nodes
buttons:
    -
        text: 'Inspecter le réseau en expension'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong> # Noeuds locaux</strong></h2>

En parallèle à la représentation globale, de nombreux noeuds locaux viennent renforcer le mouvement à travers le monde. Ils servent à connecter les initiatives locales, les collectivitéss et les particuliers à l'écosystème. Il s'agit d'une relation bidirectionnelle où les noeuds locaux soutiennent et tendent de réaliser les objectifs de FairCoop dans les communautés locales tout en apportant un feedback au niveau global. Il s'agit bien d'une aide mutuelle : le local fait grandir le global qui aide à son tour le local dans ses activités économiques.