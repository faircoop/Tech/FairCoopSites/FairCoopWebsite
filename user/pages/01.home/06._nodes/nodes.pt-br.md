---
title: 'Nós Locais'
buttons:
    -
        text: 'Veja a rede que não para de crescer'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong> # Nodos Locais</strong></h2>

Além da visão global, vários nodos locais estão sendo adicionados à rede da FairCoop em todo o mundo, a fim de conectar iniciativas, grupos e pessoas ao ecossistema. Esta relação bidirecional apoia e realiza os objetivos da FairCoop nas comunidades locais, ao mesmo tempo em que oferece um retorno importante. É uma rede de ajuda mútua, construída para expandir nosso sistema econômico a nível global, através da conquista da confiança local e do apoio à atividade econômica real.