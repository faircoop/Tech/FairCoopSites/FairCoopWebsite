---
title: Inici
media_order: homeback.349201ea.png
heading: 'QUÈ VE DESPRÉS DEL CAPITALISME?'
background_image: homeback.png
---

<h1>L'ecosistema<br> 
cooperatiu global<br> 
<em>per a </em>una economia justa</h1>