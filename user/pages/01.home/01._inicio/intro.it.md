---
title: Home
media_order: homeback.349201ea.png
heading: 'DOPO IL CAPITALISMO'
background_image: homeback.png
---

<h1>La cooperativa<br> 
della terra<br> 
<em>per</em> una fair economy.</h1>