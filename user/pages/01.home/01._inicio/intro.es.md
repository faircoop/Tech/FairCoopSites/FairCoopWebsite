---
title: Inicio
media_order: homeback.349201ea.png
heading: '¿QUÉ VIENE DESPUÉS DEL CAPITALISMO?'
background_image: homeback.png
---

<h1>El ecosistema<br> 
cooperativo de la Tierra<br> 
<em>para una</em> economía justa.</h1>