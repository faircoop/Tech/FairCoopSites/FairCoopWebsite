---
title: Home
media_order: homeback.349201ea.png
heading: 'QU''EST CE QUI VIENT APRÈS LE CAPITALISME ?'
background_image: homeback.png
---

<h1>Ecosystème<br> 
coopératif global<br> 
<em>pour une </em>économie équitable</h1>