---
title: '# Ce qui nous fait vraiment agir'
values1:
    heading: 'Révolution intégrale'
    description: 'Une transformation profonde et claire de la société, incluant ses valeurs et sa structure. La nouvelle société auto gérée est basée sur l''autonomie et l''abollition de toute forme de domination: l''état, le capitalisme, la patrie et tout ce qui affecte les relations humaines et l''environnement. Des actions stratégiques sont nécessaires afin de recycler les structures obsolètes et recréer ces valeurs et qualités qui nous permettent de vivre en commun. Le point le plus prometteur de ce changement collectif est un nouveau système économique. Cela donne au peuple la possibilité de sortir du cercle vicieux du capitalisme et de ses effets collatéraux, afin de trouver de l''espace pour de nouvelles idées et permettre la passage à une vie saine et équilibrée.'
values2:
    heading: Désobéissance
    description: 'La désobéissance implique dans une certaine mesure de désobéir à certaines lois de structures dominantes. Cela peut devenir un acte de civilité lorsque lesdites lois sont considérées fausses ou abusives. Pour bien comprendre cela, nous nous réferons à la différence entre légal et juste. Certaines lois peuvent être légales sur le papier mais ne peuvent pas être considerées comme justes, ce qui arrive fréquemment avec des gouvernements totalitaires ou fascistes. Cela peut arriver aussi avec des gouvernements dits ''démocratiques'' lorsque, par exemple, l''argent public est utilisé pour démarrer des guerres dont personne ne veut.'
values3:
    heading: 'Coopératisme ouvert'
    description: 'Nous suivons et supportons les principes des coopératives dans le sens ou elles sont gérées par leurs membres et basées sur l''aide mutuelle, l''auto-responsabilité , l''égalité, l''équité , la solidarité et la démocratie. Tout le monde peut participer activement et égalitairement aux décisions et règles. Le capital accumulé , gagné principalement avec la hausse de notre monnaie, est réinvesti dans le dévellopement de l''écosystème. De cette manière, les participants peuvent acoir accès à un réseau en constante progression de biens et services dans l''économie équitable.'
values4:
    heading: Décentralisation
    description: 'Un de nos buts est d''avoir un système d''organisation décentralisé. En faisant cela, nous empêchons un point central d''échec, sous entendu, qu''une autorité puisse nous fermer. Nous faisons en sorte que tous les participants puissent communiquer, partager des idées et travailler sur des projets sans répression ou censure. La distribution du pouvoir politique et économique, combiné avec la transparence nous protège des abus de personnes avec trop de droits et des fraudes ou autre corruption. Sans parler de prise de pouvoirs abusive.'
values5:
    heading: 'Démocratie sans états'
    description: 'Le capitalisme moderne dépend d''un système de nation-état qui lui même dépend d''une culture hiérarchique et patriarcale profondément forgée dans les esprits depuis des centaines d''années. Pour la première fois dans l''histoire, grâce à une combinaison d''éveil politique, du capitalisme défaillant et de la technologie cryptographique P2P, nous pouvons présenter une alternative réaliste aux modèles de gouvernance actuels. Un système financier coopératif global soutenant un marché libre, autonome et équitable permet une grande avancée. Si nous combinons cela avec des valeurs basées sur la liberté et un vrai système démocratique, nous commencons à obtenir une réalité. Si vous croyez en une vie en dehors des états, joignez FairCoop dès aujourd''hui !'
---

