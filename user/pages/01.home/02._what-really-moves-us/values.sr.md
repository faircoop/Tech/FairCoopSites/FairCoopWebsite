---
title: '# Šta nas zaista pokreće'
values1:
    heading: 'Integralna revolucija'
    description: 'Korenita i obimna transformacija svih delova društva, uključujući njegove vrednosti i strukturu. Novo, samoupravno društvo, zasnovano je na autonomiji i ukidanju svih oblika dominacije: države, kapitalista, patrijarhalnih odnosa, kao i svih ostalih ustaljenih obrazaca koji utiču na društvene veze i prirodnu sredinu. Potrebne su svesne i strateške akcije, kojima bi se preuredile zastarele strukture i kako bi se oporavile vrednosti i kvaliteti koji nam omogućavaju zajednički suživot. Kao obećavajuću početnu tačku za postizanje kolektivnih promena, vidimo. Na ovaj način ljudima se pruža mogućnost da konačno izadju iz začaranog kruga kapitilističkog potlačivanja i njegovih štetnih posledica, i otvara prostor za nove ideje, bez granica, omogućavajući preokret ka zdravom životu, u balansu sa prirodom.'
values2:
    heading: Neposlušnost
    description: 'Neposlušnost podrazumeva, do nekog stepena, građansko nepoštovanje pojedinih zakona vladajućih struktura. Kada se zakon smatra pogrešnim, ovo može postati i civilna dužnost. U cilju boljeg razumevanja, upućujemo na postojanje razlike između pojma prava i pravde. Određena pravila i propisi mogu biti zakoniti na papiru, ali se ne moraju nužno smatrati pravednim, što je najčešće slučaj kod represivnih, fašističkih i totalitarnih režima vladavine. Ovo može da se odnosi i na zapadne države, na primer, kada njihove vlade budžetom poreskih obveznika finansiraju ''zakonite'' ratove protiv ''neposlušnih'' država i njihovog stanovništva.'
values3:
    heading: 'Otvorena kooperativnost'
    description: 'Pratimo i podržavamo osnovne smernice kooperativa, jer njihov rad organizuju učesnici povezani idejama međusobne pomoći, samoodgovornosti, jednakosti, pravičnosti, participativne demokratije i solidarnosti. Svako može aktivno i podjednako da učestvuje u donošenju odluka i postavljanju pravila. Akumulirani kapital, uglavnom ostvaren povećanjem vrednosti naše valute, ulaže se u razvoj ekosistema i sa njim povezane projekte. Na ovaj način učesnici mogu da imaju pristup instrumentima za samoorganizovanje i konstatno rastućoj mreži dobara i usluga u okviru pravične ekonomije.'
values4:
    heading: Decentralizacija
    description: 'Jedan od naših ciljeva je decentralizovani organizacioni model učešća i odlučivanja. Postupcima decentralizacije, sprečavamo pojavu najizvesnije mogućnosti nesupeha, izbegavajući da bilo koja vlast ukine naš kooperativni ekosistem. Želimo da budemo sigurni da svi učesnici nesmetano komuniciraju, dele ideje, rade na projektima ili prenošenju vrednosti, bez bilo kakve represije ili cenzure. Raspodela ekonomske i političke vlasti, u kombinaciji sa, predupređuje zloupotrebe, dok jednaka prava i pristupačnost, sprečavaju prevare, korupciju ili preotimanje.'
values5:
    heading: 'Demokratija bez države'
    description: 'Savremeni kapitalizam zavisi od državnog uređenja, koji je utemeljen na principima hijerarhijskog i patrijahalnog kolektiviteta, duboko ukorenjenog viševekovnim ugnjetavanjem ljudi. Prvi put u istoriji, kombinacija političke svesnosti, slabost kapitalističke ekonomije i p2p kriptografska tehnologija nam dozvoljavaju da predstavimo ozbiljne i realne alternative postojećim modelima upravljanja. Globalni kooperativni finansijski sistem nam daje slobodno i pravično tržište, koje je ogroman korak unapred. U kombinaciji sa neraskidivim sistemom etičkih vrednosti, zasnovanim na slobodi i demokratskim procesima odlučivanja, postaje vidljiva nova realnost. Ukoliko verujete da je moguć život van državnih okvira, pridružite se [povežite se sa najbližom petljom, ili slično] danas FairCoop-u.'
---

