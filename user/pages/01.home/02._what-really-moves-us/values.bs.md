---
title: '# Šta nas uistinu pokreće'
values1:
    heading: 'Integralna Revolucija'
    description: 'Duboka i sveobuhvatna transformacija svih dijelova društva, uključujući vrijednosti i strukturu. Novo, samo-upravljajuće društvo je zasnovano na autonomiji i napuštanju svih oblika dominacije: države, kapitalizma, patrijarhalizma i svih drugih oblika koji utiču na ljudske odnose i odnose sa prirodnim okruženjem. Svjesna i strateška djelovanja su potrebna da se raspadnu zastarjele strukture i povrate one vrijednosti i kvalitete koje će nam omogućiti da živimo život u zajedništvu. Kao najobećavajuću polaznu tačku za kolektivnu promjenu vidimo novi ekonomski sistem. Ovo ljudima daje mogućnost da konačno izađu iz zlobnog kruga kapitalističkog porobljavanja i njegovih popratnih pojava, da pronađu prostor za nove ideje bez granica i učine mogućim prelazak na zdrav život u skladu sa prirodom.'
values2:
    heading: Neposlušnost
    description: 'Neposlušnost implicira, u izvjesnoj mjeri, neposlušnost određenim zakonima ili zahtjevima od strane dominantnih struktura. Ponekad to postaje građanska dužnost i to onda kada se zakon smatra pogrešnim. Da bi se navedeno razumjelo upućujemo na razliku između zakonito i pravično. Određena pravila i zakoni mogu biti zakoniti na papiru ali ne mogu se smatrati pravednim, što je često slučaj u represivnim, fašističkim ili totalitarnim režimima. Ovo se, indirektno ili direktno može primijeniti na zapadne države, kao naprimjer, kada vlast koristi uobičajene poreze da finansira ''zakoniti'' agresivni rat protiv druge zemlje i njenog civilnog društva.'
values3:
    heading: 'Otvoreni Kooperativizam'
    description: 'Mi slijedimo i podržavamo temeljne smjernice kooperativnosti kojima su vođeni učesnici, utemeljeni na uzajamnoj pomoći, samo-odgovornosti, jednakosti, pravičnosti, demokratiji i solidarnosti. Svi mogu aktivno i jednako učestvovati u donošenju odluka i pravljenju politika. Akumulirani kapital, prikupljen uglavnom kroz povećanje vrijednosti naše valute, je ponovo investiran u razvoj ekosistema i odgovarajućih projekata. Na ovaj način učesnici imaju pristup većem broju samo-osnažujućih alata i konstantno rastućoj mreži dobara i usluga unutar Fer Ekonomije.'
values4:
    heading: Decentralizacija
    description: 'Jedan od naših ciljeva je da imamo decentralizovan model odlučivanja i uloga. Decentraliziranjem naše tehnologije, mi sprečavamo centralnu tačku neuspjeha, time izbjegavajući da nas ugasi bilo koja vlast. Želimo da osiguramo da svi učesnici mogu komunicirati, dijeliti ideje, raditi na projektima i prenositi vrijednost bez represije ili cenzure. Raspodjela političke i ekonomske moći u kombinaciji sa radikalnom transparentnošću, preduprijediti će zloupotrebu uloga sa jedinstvenim pravima ili pristupom i tako spriječiti prijevaru, korupciju ili preuzimanja.'
values5:
    heading: 'Demokratija bez Države'
    description: 'Savremeni kapitalist ovisi o sistemu nacionalnih država, što se zauzvrat zasniva na patrijarhalnom i hijerarhijskom društvenom misaonom sklopu, duboko urezanim vjekovima ugnjetavanja ljudi od strane drugih ljudi. Po prvi put u istoriji, kombinacija političke svjesnosti, slabosti kapitalističke ekonomije i p2p kriptografske tehnologije dopušta nam da predstavimo ozbiljne i realistične alternative trenutnom modelu upravljanja. Globalni kooperativni financijski sistem nudi nam slobodna i fer tržišta, a to je divovski korak naprijed. Ako se ovo kombinuje sa neraskidivim sklopom etičkih vrijednosti utemeljenih na slobodi i demokratskom protokolu za donošenje odluka, počinje da postaje vidljiva realnost. Ako vjerujete da je život izvan države moguć, pridružite se FairCoop-u već danas.'
---

