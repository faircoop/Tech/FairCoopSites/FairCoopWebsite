---
title: 'Um mergulho na nossa metodologia'
media_order: diagram.268de66e.jpg
---

# # Assembleias abertas e consenso
A comunidade é quem decide sobre como alcançar nossos objetivos comuns em assembleias abertas. Todos podem participar e propor novos empreendimentos, sugerir mudanças ou apresentar projetos de trabalho. Quando uma proposta é aprovada por consenso, o respectivo proponente e os grupos de trabalho envolvidos tomarão as medidas. Organizamos assim tanto as reuniões globais online, quanto as reuniões presenciais nos Nodos Locais.

# # Auto-organização e distribuição dos recursos
O ecossistema da FairCoop organiza-se mediante a cooperação aberta. Todo mundo é encorajado a participar com as próprias habilidades, ideias e ter iniciativa para assumir responsabilidades, ajudando assim o movimento a crescer. Com base em nossa inovadora ferramenta de trabalho colaborativo, os participantes podem registrar o tempo dedicado ao projeto como voluntariado ou solicitar uma remuneração em FairCoin para atender a suas necessidades pessoais. Desta forma, recompensamos as pessoas cujo trabalho proporciona um valor concreto à FairCoop, ao mesmo tempo que fazemos a distribuição dos recursos de forma justa e descentralizada.