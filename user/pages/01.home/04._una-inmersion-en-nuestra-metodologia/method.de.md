---
title: 'Ein Einblick in unsere Arbeitsweise'
media_order: diagram.268de66e.jpg
---

# Offene Versammlungen und Konsens
In offenen Versammlungen entscheidet die Community darüber, wie die gemeinsamen Ziele erreicht werden sollen. Jeder kann sich beteiligen und Vorschläge für Weiterentwicklungen, Änderungen oder Projekte machen an denen gearbeitet werden soll. Wenn ein Vorschlag im Konsens verabschiedet wird, werden die jeweiligen Antragssteller und die entsprechende Arbeitsgruppe aktiv. Auf diese Weise organisieren wir sowohl die globalen Online-Versammlungen als auch die physischen Versammlungen der individuellen lokalen Knoten.

# Selbstorganisation und Wertschöpfungsverteilung
Das FairCoop-Ökosystem organisiert sich selbst durch das Modell des offenen Kooperationswesens. Jeder Einzelne wird ermutigt, sich mit seinen individuellen Fähigkeiten und Ideen einzubringen, proaktive Verantwortung zu übernehmen und so der Bewegung zu helfen, sich weiterzuentwickeln. Durch unsere innovative und kollaborative Arbeitspattform OCP können die Teilnehmer entweder ehrenamtlich ihren Beitrag leisten oder eine Vergütung in FairCoin beantragen um ihre Bedürfnisse zu decken. Auf diese Art können wir diejenigen belohnen, die mit ihrer Arbeit einen echten Mehrwert in FairCoop bringen. Die Wertschöpfungsverteilung erfolgt dabei auf faire und dezentrale Weise.