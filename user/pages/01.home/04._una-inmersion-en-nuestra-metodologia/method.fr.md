---
title: 'Un zoom sur notre méthodologie'
media_order: diagram.268de66e.jpg
---

# # Assemblées ouvertes et concensus
Dans nos assemblées ouvertes, la communauté décide elle même des moyens d'atteindre un but commun. Tout le monde peut participer et faire des propositions sur les projets en cours ou à développer. Lorsqu'une proposition passe et mène à un consensus, le créateur et le groupe respectif peuvent commencer à travailler sur le projet. Nous organisons les assemblées globales en ligne et les locales sont physiques.

# # Auto organisation et distribution des valeurs
L'écosystème FairCoop s'auto organise à l'aide du coopérativisme ouvert. Tout le monde est encouragé à participer suivant ses  compétences et envies et à prendre des initiatives, donc  à aider le mouvement à grandir. Les participants de notre outil collaboratif innovant  peuvent le faire sur base volontaire ou demander une rémunération en FairCoins afin de pouvoir couvrir leurs besoins. De cette manière, les participants de FairCoop sont rétribués à leur vraie valeur et la distribution est faite d'une manière équitable et décentralisée.