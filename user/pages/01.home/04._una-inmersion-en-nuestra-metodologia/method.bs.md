---
title: 'Uronite u našu metodologiju'
media_order: diagram.268de66e.jpg
---

# Otvorene Skupštine i konsenzus
Na otvorenim skupštinama, zajednica odlučuje kako će ostvariti zajedničke ciljeve. Svi mogu učestvovati i davati prijedloge za razvoj, promjene ili projekte na kojima bi se radilo. Kada se prijedlog usvoji kroz konsenzus, dotični predlagač i relevantne radne grupe preduzimaju akciju. Na ovakav način organizujemo globalne online sastanke i sastanke članova lokalnih čvorova

# Samo-Organizacija i Distribucija Vrijednosti
FairCoop ekosistem je organizovan kroz otvoreni kooperativizam. Svi se potiču da učestvuju skupa sa njihovim individualnim vještinama i idejama, te preuzmu proaktivne odgovornosti i tako pomognu da pokret raste. Temeljem našeg inovativnog kolaborativnog radnog alata, učesnici mogu volonterski odraditi sate ili tražiti naknadu u faircoinima - fer novčićima da pokriju svoje troškove. Na ovaj način nagrađujemo sve one koji unose stvarne vrijednosti svojim radom u Fair Coop-u, a distribucija vrijednosti je urađena na fer/pravedan i decentralizovan način.