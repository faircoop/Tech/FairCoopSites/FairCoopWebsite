---
title: 'A dive into our methodology'
media_order: diagram.268de66e.jpg
---

# #Open assemblies and consensus
In open assemblies, the community decides on how to reach our common goals. Everybody can participate and make proposals for developments, changes, or projects to work on. When a proposal passes via consensus the respective proposal maker and the relevant working groups will take action. We organise both the global online meetings and the physical local node meetings in this way.

# #Self-organization and Value Distribution
The FairCoop ecosystem organizes itself through Open Cooperativism. Everybody is encouraged to participate with their individual skills and ideas, and take proactive responsibilities, thus helping the movement grow. Based on our innovative collaborative working tool, the participants can either volunteer their hours or claim a remuneration in faircoin to cover their needs. In this way we reward those who are bringing real value with their work into FairCoop, and the value distribution is done in a fair and decentralized way.