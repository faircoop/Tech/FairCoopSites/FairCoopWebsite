---
title: 'Un assaggio della nostra metodologia'
media_order: diagram.268de66e.jpg
---

# # Assemblee aperte e consenso
Nelle assemblee aperte, la comunità decide come raggiungere gli obiettivi comuni. Tutti possono partecipare e presentare proposte per sviluppi, cambiamenti o progetti a cui lavorare. Quando una proposta raccoglie il consenso, ognuno si attiva per dare corpo alle scelte maturate. E' questo il metodo di organizazzione che seguiamo sia per gli incontri online globali che nelle riunioni territoriali dei nodi locali.

# # Auto organizzazione e distribuzione delle risorse
L'ecosistema FairCoop si organizza attraverso il cooperativismo aperto. Tutti sono incoraggiati a partecipare con le proprie capacità e idee individuali e ad assumere responsabilità attive, aiutando così il movimento a crescere. Collaborando, i partecipanti possono offrire volontariamente il proprio lavoro o richiedere una remunerazione in faircoin per coprire le proprie esigenze. Il tutto secondo una logica di mutuo vantaggio tra singoli o gruppi e l'intero ecosistema