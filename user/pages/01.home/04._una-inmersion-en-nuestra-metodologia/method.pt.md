---
title: 'Uma imersão na nossa metodologia'
media_order: diagram.268de66e.jpg
---

# #Assembleias abertas e consenso
Nas assembleias abertas, a comunidade decide como realizar metas comuns. Qualquer um pode participar nelas e fazer propostas para desenvolvimentos, mudanças ou projetos para trabalhar. Quando uma proposta é aprovada por consenso, quem a propôs e os grupos de trabalho relevantes para ela,  começam a trabalhar. Desta forma, organizamos as reuniões globais on-line e as reuniões físicas dos nós locais.

# #Auto organizacão e distribução do valor
O ecossistema FairCoop é organizado por meio do cooperativismo aberto. Todos são encorajados a participar com suas habilidades e ideias particulares e a ser pró-ativos em assumir responsabilidades,contribuindo assim no crescimento colectivo. Através da nossa inovadora ferramenta de trabalho colaborativo (OCP), os participantes podem registar o tempo dedicado ao projeto, seja voluntariamente ou solicitando remuneração em FairCoins para cobrir as suas necessidades. Desta forma, recompensamos aqueles que estão contribuindo com valor real para a FairCoop com seu trabalho e distribuímos o valor de maneira justa e descentralizada.