---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'FairCoin Mapa Mundial'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## Nós usamos 
## **FairCoin**
FairCoin é uma moeda digital ética, definida com os valores do ecosistema da FairCoop e com o suporte de um movimento cooperativo global em rápido crescimento. É descentralisada, como outras criptomoedas, mas ao mesmo tempo tem implementadas inovações radicais que a tornam única em termos ecológicos, valor de crescimento estavél, comercio justo, oportunidades de poupança e financiamento. O seu continuo desenvolvimento tem como objectivo a criação de ums sistema económico mais justo para o Planeta. Ler mais sobre [ Ler mais.](https://fair-coin.org/)

## A construír a  
## **Economia Circular**
Nós estamos a cosntruir uma economia real e proditiva com o FairCoin, com a criação de um uso circular de produtos e serviços quer a nível global ou a nível local. Mais e mais comerciantes, lojas e produtores do mundo inteiro confiam na FairCoin como meio de troca, tirando vantagem do seu valor estável e de várias ferramentas livre para o seu supporte.