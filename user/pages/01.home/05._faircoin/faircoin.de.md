---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'FairCoins weltweit'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## Wir verwenden 
## **FairCoin**
FairCoin ist eine ethische digitale Währung, die durch die Werte des FairCoop Ökosystems vorangetrieben wird und durch eine schnell wachsende kooperative Bewegung unterstützt wird. Sie ist dezentralisiert, wie jede andere Kryptowährung auch, setzt aber gleichzeitig radikale Innovationen um, die sie in Bezug auf ökologisches Handeln, Wertbeständigkeit, ethischen Anwendung, Ersparnisse und Finanzierungsmöglichkeiten einzigartig macht. Die Entwicklung geht kontinuierlich weiter, mit dem Ziel ein weltweit gerechtes Wirtschaftssystem zu errichten. Mehr über FairCoin.[hier](https://fair-coin.org/)



## für eine
## **Zirkuläre Wirtschaft**
Mit der Schaffung einer kreisförmigen Nutzung von Produkten und Dienstleistungen auf lokaler und globaler Ebene bauen wir um FairCoin herum eine reale und produktive Wirtschaft auf. Immer mehr Händler, Shops und Produzenten weltweit vertrauen FairCoin als Zahlungs -und Wertaufbewahrungsmittel und nutzen die Vorteile seiner Wertbeständigkeit, sowie den diversen offenen und kostenlosen Tools um FairCoin herum.