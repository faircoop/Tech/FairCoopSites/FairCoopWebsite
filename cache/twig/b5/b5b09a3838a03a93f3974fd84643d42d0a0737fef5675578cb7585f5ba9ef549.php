<?php

/* modular/method.html.twig */
class __TwigTemplate_670ba6d1390ca03a1b9f2ddf45ccf9f94b64c9d8d2e3cebfea51ef72b1d385ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-method\">

        ";
        // line 3
        if ($this->getAttribute(($context["header"] ?? null), "title", array())) {
            // line 4
            echo "\t<h2>";
            echo $this->getAttribute(($context["header"] ?? null), "title", array());
            echo "</h2>
        ";
        }
        // line 6
        echo "    <div class=\"row wrapper\">
\t<div class=\"col-md-8\">
        ";
        // line 8
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "
\t</div>

\t<div class=\"col-md-4\">
\t";
        // line 12
        if (($context["blog_image"] ?? null)) {
            // line 13
            echo "\t";
            echo $this->getAttribute($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), "images", array())), "cropResize", array(0 => 441, 1 => 580), "method"), "html", array(0 => $this->getAttribute(($context["page"] ?? null), "title", array()), 1 => $this->getAttribute(($context["page"] ?? null), "title", array()), 2 => "image method"), "method");
            echo "
        ";
        } else {
            // line 15
            echo "\t";
            echo $this->getAttribute($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), "images", array())), "cropZoom", array(0 => 441, 1 => 580), "method"), "html", array(0 => $this->getAttribute(($context["page"] ?? null), "title", array()), 1 => $this->getAttribute(($context["page"] ?? null), "title", array()), 2 => "image method"), "method");
            echo "
\t";
        }
        // line 17
        echo "\t</div>
    </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/method.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 17,  50 => 15,  44 => 13,  42 => 12,  35 => 8,  31 => 6,  25 => 4,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-method\">

        {% if header.title  %}
\t<h2>{{ header.title }}</h2>
        {% endif %}
    <div class=\"row wrapper\">
\t<div class=\"col-md-8\">
        {{ page.content }}
\t</div>

\t<div class=\"col-md-4\">
\t{% if blog_image %}
\t{{ page.media.images|first.cropResize(441,580).html(page.title, page.title, 'image method') }}
        {% else %}
\t{{ page.media.images|first.cropZoom(441,580).html(page.title, page.title, 'image method') }}
\t{% endif %}
\t</div>
    </div>
</section>
", "modular/method.html.twig", "/var/www/clients/client8/web19/web/user/themes/fair-coop/templates/modular/method.html.twig");
    }
}
