<?php

/* modular/image.html.twig */
class __TwigTemplate_8534dd9b49ecbb08e7f1accad48116bfa2e8ae91c69a2899a9fd7d47f53a189a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "image", array()), "background_image", array())) {
            // line 2
            echo "<section class=\"section-image fullscreen\" style=\"background-repeat: no-repeat;padding-bottom: 5rem;min-height: 300px;background-size: cover;background-image: url('";
            echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($this->getAttribute(($context["header"] ?? null), "image", array()), "background_image", array()), array(), "array"), "url", array());
            echo "');\">
";
        }
        // line 4
        echo "        ";
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "image", array()), "heading", array())) {
            // line 5
            echo "\t<h3>";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "image", array()), "heading", array());
            echo "</h3>
        ";
        }
        // line 6
        echo "        
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 6,  30 => 5,  27 => 4,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if header.image.background_image  %}
<section class=\"section-image fullscreen\" style=\"background-repeat: no-repeat;padding-bottom: 5rem;min-height: 300px;background-size: cover;background-image: url('{{ page.media[header.image.background_image].url }}');\">
{% endif %}
        {% if header.image.heading  %}
\t<h3>{{ header.image.heading }}</h3>
        {% endif %}        
</section>
", "modular/image.html.twig", "/var/www/clients/client8/web19/web/user/themes/fair-coop/templates/modular/image.html.twig");
    }
}
