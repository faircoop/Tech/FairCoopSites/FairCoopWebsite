<?php

/* item.html.twig */
class __TwigTemplate_551a29aaf7f1bf82685aca817c495e1d6191545faf4392a0b17c5764638a862d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("partials/base.html.twig", "item.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"defaultpage container\">
<div class=\"image-tittle\">
";
        // line 6
        if ($this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "main_image", array()), "heading", array())) {
            // line 7
            echo "<h1> ";
            echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "main_image", array()), "heading", array());
            echo "</h1>
";
        } else {
            // line 9
            echo "<h1>";
            echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "title", array());
            echo "</h1>\t
";
        }
        // line 11
        echo "</div>
<div class=\"image-header\">
        ";
        // line 13
        if ($this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "main_image", array()), "image", array())) {
            // line 14
            echo "           <img title=\"";
            echo $this->getAttribute(($context["page"] ?? null), "title", array());
            echo "\" alt=\"";
            echo $this->getAttribute(($context["page"] ?? null), "title", array());
            echo "\" class=\"image featured\" src=\"";
            echo $this->getAttribute(($context["page"] ?? null), "url", array());
            echo "/";
            echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "main_image", array()), "image", array());
            echo "\">
        ";
        } else {
            // line 16
            echo "            ";
            echo $this->getAttribute($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), "images", array())), "cropZoom", array(0 => 1038, 1 => 437), "method"), "html", array(0 => $this->getAttribute(($context["page"] ?? null), "title", array()), 1 => $this->getAttribute(($context["page"] ?? null), "title", array()), 2 => "image featured"), "method");
            echo "
        ";
        }
        // line 18
        echo "</div>
    ";
        // line 19
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 19,  73 => 18,  67 => 16,  55 => 14,  53 => 13,  49 => 11,  43 => 9,  37 => 7,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'partials/base.html.twig' %}

{% block content %}
<div class=\"defaultpage container\">
<div class=\"image-tittle\">
{% if page.header.main_image.heading %}
<h1> {{ page.header.main_image.heading }}</h1>
{% else %}
<h1>{{ page.header.title }}</h1>\t
{% endif %}
</div>
<div class=\"image-header\">
        {% if page.header.main_image.image %}
           <img title=\"{{ page.title }}\" alt=\"{{ page.title }}\" class=\"image featured\" src=\"{{ page.url }}/{{ page.header.main_image.image }}\">
        {% else %}
            {{ page.media.images|first.cropZoom(1038,437).html(page.title, page.title, 'image featured') }}
        {% endif %}
</div>
    {{ page.content }}
</div>
{% endblock %}

", "item.html.twig", "/var/www/clients/client8/web19/web/user/themes/fair-coop/templates/item.html.twig");
    }
}
