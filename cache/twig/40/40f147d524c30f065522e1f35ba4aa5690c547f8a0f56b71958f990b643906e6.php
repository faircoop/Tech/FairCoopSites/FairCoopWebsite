<?php

/* partials/header.html.twig */
class __TwigTemplate_e959cbf1a6481ec67877a1e29238b244a405b7fb4e2b5fced0d67353b2cb8d86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"topbar navbar navbar-expand-md navbar-dark bg-dark\">
        <div class = \"navbar-header\">
\t<a name=\"logo\" class=\"navbar-brand top-logo\" href=\"/\">FairCoop</a>

   \t</div>


\t<div class=\"topbar_meta\">
\t<div class=\"dropdown languages\">

  \t<button class=\"btn btn-secondary dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
   \t";
        // line 12
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("Languages");
        echo "
  \t</button>
  \t\t<div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">
\t\t";
        // line 15
        $this->loadTemplate("partials/langswitcher.html.twig", "partials/header.html.twig", 15)->display($context);
        // line 16
        echo "
  \t\t</div>
\t</div>
\t<div class=\"meta_social\">
\t\t<i class=\"social\">
\t\t<a href=\"https://twitter.com/Fair_Coop\" target=\"_blank\"><svg xmlns=\"http://www.w3.org/2000/svg\" color=\"white\" width=\"16\" height=\"16\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-users\"><path d=\"M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z\"></path></svg></a></i><i class=\"social\"><a href=\"https://git.fairkom.net/faircoop/Tech/FairCoopSites/FairCoopWebsite\" target=\"_blank\"><svg xmlns=\"http://www.w3.org/2000/svg\" color=\"white\" width=\"16\" height=\"16\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-users\"><path d=\"M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22\"></path></svg>
\t\t</a>
\t\t</i>
\t</div>
 </div>

</nav>


<nav class=\"navbar navbar-expand-md navbar-dark bg-dark\">
   <div class = \"container\">
      <div class = \"navbar-header\">
\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\" aria-controls=\"navbarCollapse\" aria-expanded=\"true\" aria-label=\"Toggle navigation\">
          <span class=\"navbar-toggler-icon\"></span>
        </button>


         <a name=\"logo\" class=\"navbar-brand header-logo\" href=\"/\"></a>
      </div>
      <div class = \"navbar-collapse collapse\" id=\"navbarCollapse\">
         <ul class = \"nav navbar-nav navbar-right\">
            ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["pages"] ?? null), "children", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 43
            echo "            ";
            if ($this->getAttribute($context["page"], "visible", array())) {
                // line 44
                echo "            ";
                $context["current_page"] = ((($this->getAttribute($context["page"], "active", array()) || $this->getAttribute($context["page"], "activeChild", array()))) ? ("active") : (""));
                // line 45
                echo "            <li class = \"";
                echo ($context["current_page"] ?? null);
                echo "\"><a href = \"";
                echo $this->getAttribute($context["page"], "url", array());
                echo "\">";
                echo $this->getAttribute($context["page"], "menu", array());
                echo "</a></li>
            ";
            }
            // line 47
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "         </ul>
      </div>
   </div>
</nav>
";
    }

    public function getTemplateName()
    {
        return "partials/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 48,  88 => 47,  78 => 45,  75 => 44,  72 => 43,  68 => 42,  40 => 16,  38 => 15,  32 => 12,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<nav class=\"topbar navbar navbar-expand-md navbar-dark bg-dark\">
        <div class = \"navbar-header\">
\t<a name=\"logo\" class=\"navbar-brand top-logo\" href=\"/\">FairCoop</a>

   \t</div>


\t<div class=\"topbar_meta\">
\t<div class=\"dropdown languages\">

  \t<button class=\"btn btn-secondary dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
   \t{{ 'Languages'|t }}
  \t</button>
  \t\t<div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">
\t\t{% include 'partials/langswitcher.html.twig' %}

  \t\t</div>
\t</div>
\t<div class=\"meta_social\">
\t\t<i class=\"social\">
\t\t<a href=\"https://twitter.com/Fair_Coop\" target=\"_blank\"><svg xmlns=\"http://www.w3.org/2000/svg\" color=\"white\" width=\"16\" height=\"16\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-users\"><path d=\"M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z\"></path></svg></a></i><i class=\"social\"><a href=\"https://git.fairkom.net/faircoop/Tech/FairCoopSites/FairCoopWebsite\" target=\"_blank\"><svg xmlns=\"http://www.w3.org/2000/svg\" color=\"white\" width=\"16\" height=\"16\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-users\"><path d=\"M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22\"></path></svg>
\t\t</a>
\t\t</i>
\t</div>
 </div>

</nav>


<nav class=\"navbar navbar-expand-md navbar-dark bg-dark\">
   <div class = \"container\">
      <div class = \"navbar-header\">
\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\" aria-controls=\"navbarCollapse\" aria-expanded=\"true\" aria-label=\"Toggle navigation\">
          <span class=\"navbar-toggler-icon\"></span>
        </button>


         <a name=\"logo\" class=\"navbar-brand header-logo\" href=\"/\"></a>
      </div>
      <div class = \"navbar-collapse collapse\" id=\"navbarCollapse\">
         <ul class = \"nav navbar-nav navbar-right\">
            {% for page in pages.children %}
            {% if page.visible %}
            {% set current_page = (page.active or page.activeChild) ? 'active' : '' %}
            <li class = \"{{ current_page }}\"><a href = \"{{ page.url }}\">{{ page.menu }}</a></li>
            {% endif %}
            {% endfor %}
         </ul>
      </div>
   </div>
</nav>
", "partials/header.html.twig", "/var/www/clients/client8/web19/web/user/themes/fair-coop/templates/partials/header.html.twig");
    }
}
