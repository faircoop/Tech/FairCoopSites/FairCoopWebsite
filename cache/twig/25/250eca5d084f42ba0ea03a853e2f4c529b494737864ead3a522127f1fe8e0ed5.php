<?php

/* error.json.twig */
class __TwigTemplate_17b3b559b7d4b3c5d1a5aa9c132940b836a915f0ed7bfd2154a19535c977671f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_jsonencode_filter($this->getAttribute(($context["page"] ?? null), "content", array()));
    }

    public function getTemplateName()
    {
        return "error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ page.content|json_encode()|raw }}", "error.json.twig", "/var/www/clients/client8/web19/web/user/plugins/error/templates/error.json.twig");
    }
}
