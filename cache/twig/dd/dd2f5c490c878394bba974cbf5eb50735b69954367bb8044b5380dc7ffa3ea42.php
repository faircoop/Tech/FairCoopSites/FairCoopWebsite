<?php

/* modular/goals.html.twig */
class __TwigTemplate_e13eec98cce1a141b09514a8d7146bbd0a80cc19364f6e233f0952168e71f073 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-goals\">
<div class=\"container\">
        ";
        // line 3
        if ($this->getAttribute(($context["header"] ?? null), "title", array())) {
            // line 4
            echo "\t<h2><strong>";
            echo $this->getAttribute(($context["header"] ?? null), "title", array());
            echo "</strong></h2>
        ";
        }
        // line 6
        echo "    
\t<div class=\"col-md-12\">
        ";
        // line 8
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "
\t</div>

\t</div>
    </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/goals.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 8,  31 => 6,  25 => 4,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-goals\">
<div class=\"container\">
        {% if header.title  %}
\t<h2><strong>{{ header.title }}</strong></h2>
        {% endif %}
    
\t<div class=\"col-md-12\">
        {{ page.content }}
\t</div>

\t</div>
    </div>
</section>
", "modular/goals.html.twig", "/var/www/clients/client8/web19/web/user/themes/fair-coop/templates/modular/goals.html.twig");
    }
}
