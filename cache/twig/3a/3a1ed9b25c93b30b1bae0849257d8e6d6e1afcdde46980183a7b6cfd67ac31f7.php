<?php

/* error.html.twig */
class __TwigTemplate_d1e09dbef291cf7f70e92f59198c3c102ef8b140b75edad07d19becaf57d7a29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<META HTTP-EQUIV=Refresh CONTENT=\"0; URL=https://2017.fair.coop/";
        echo $this->getAttribute(($context["uri"] ?? null), "path", array());
        echo "\">
";
    }

    public function getTemplateName()
    {
        return "error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<META HTTP-EQUIV=Refresh CONTENT=\"0; URL=https://2017.fair.coop/{{ uri.path }}\">
", "error.html.twig", "/var/www/clients/client8/web19/web/user/themes/fair-coop/templates/error.html.twig");
    }
}
