<?php

/* modular/intro.html.twig */
class __TwigTemplate_64d07b650c85eebc4a0d480393def28aa5e4be340efaeeb2108654b957ee8896 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute(($context["header"] ?? null), "background_image", array())) {
            // line 2
            echo "<section class=\"section-intro fullscreen\" style=\"background-repeat: no-repeat;padding-bottom: 5rem;background-position: bottom;min-height: 600px;background-size: cover;background-image: url('";
            echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute(($context["header"] ?? null), "background_image", array()), array(), "array"), "url", array());
            echo "');\">
";
        }
        // line 4
        echo "<div class=\"row wrapper\">
\t\t<div class=\"col-md-6\">
\t\t";
        // line 6
        if ($this->getAttribute(($context["header"] ?? null), "heading", array())) {
            // line 7
            echo "\t\t<h2>";
            echo $this->getAttribute(($context["header"] ?? null), "heading", array());
            echo "</h2>
\t\t";
        }
        // line 9
        echo "         \t";
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "
\t</div>
\t<div class=\"col-md-6\">
\t\t<div class=\"welcome\">
\t\t\t<span><svg xmlns=\"http://www.w3.org/2000/svg\" color=\"#333\" width=\"18\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"1\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-users\"><path d=\"M4 19.5A2.5 2.5 0 0 1 6.5 17H20\"></path><path d=\"M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z\"></path></svg></span>
\t\t\t<h5>";
        // line 14
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("Faircoop Beginner guide");
        echo "</h5>
\t\t\t<div class=\"feed_actions\">
\t\t\t\t<a class=\"inline\" href=\"https://git.fairkom.net/faircoop/MediaCommunication/wikis/welcome-to-faircoop\" target=\"blank\">";
        // line 16
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("Read online");
        echo "</a>
\t\t\t</div>
\t\t</div>
\t
\t</div>

<div class=\"button btn-read\">
\t<div class=\"meta_desc\">
\t<a href=\"/about\">";
        // line 24
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("read more");
        echo "</a>
\t<a class=\"inline\" href=\"https://forum.fair.coop\">";
        // line 25
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("join discussion");
        echo "</a>
\t</div>
</div>

</div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/intro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 25,  64 => 24,  53 => 16,  48 => 14,  39 => 9,  33 => 7,  31 => 6,  27 => 4,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if header.background_image  %}
<section class=\"section-intro fullscreen\" style=\"background-repeat: no-repeat;padding-bottom: 5rem;background-position: bottom;min-height: 600px;background-size: cover;background-image: url('{{ page.media[header.background_image].url }}');\">
{% endif %}
<div class=\"row wrapper\">
\t\t<div class=\"col-md-6\">
\t\t{% if header.heading %}
\t\t<h2>{{ header.heading }}</h2>
\t\t{% endif %}
         \t{{ page.content }}
\t</div>
\t<div class=\"col-md-6\">
\t\t<div class=\"welcome\">
\t\t\t<span><svg xmlns=\"http://www.w3.org/2000/svg\" color=\"#333\" width=\"18\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"1\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-users\"><path d=\"M4 19.5A2.5 2.5 0 0 1 6.5 17H20\"></path><path d=\"M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z\"></path></svg></span>
\t\t\t<h5>{{ 'Faircoop Beginner guide'|t }}</h5>
\t\t\t<div class=\"feed_actions\">
\t\t\t\t<a class=\"inline\" href=\"https://git.fairkom.net/faircoop/MediaCommunication/wikis/welcome-to-faircoop\" target=\"blank\">{{ 'Read online'|t }}</a>
\t\t\t</div>
\t\t</div>
\t
\t</div>

<div class=\"button btn-read\">
\t<div class=\"meta_desc\">
\t<a href=\"/about\">{{ 'read more'|t }}</a>
\t<a class=\"inline\" href=\"https://forum.fair.coop\">{{ 'join discussion'|t }}</a>
\t</div>
</div>

</div>
</section>
", "modular/intro.html.twig", "/var/www/clients/client8/web19/web/user/themes/fair-coop/templates/modular/intro.html.twig");
    }
}
