<?php

/* partials/base.html.twig */
class __TwigTemplate_e2be45f8af60a7713ceb7dffdc563f540f39d834c171f6355d7ae3a794f83be2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
            'bottom' => array($this, 'block_bottom'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
   <head>
      ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 43
        echo "   </head>
      <body>
         ";
        // line 46
        echo "         ";
        $this->loadTemplate("partials/header.html.twig", "partials/base.html.twig", 46)->display($context);
        // line 47
        echo "            ";
        $this->displayBlock('content', $context, $blocks);
        // line 48
        echo "         <div class = \"footer\">
            <div class = \"container\">
               <p>";
        // line 50
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("footer");
        echo "</p>
\t       <p><a name=\"logo\" class=\"footer-logo\" href=\"/\">
\t\t<img src=\"/user/themes/fair-coop/images/footer-logo.png\" alt\"Footer Logo\">
\t       </a></p>
            </div>
         </div>
      </body>
   ";
        // line 57
        $this->displayBlock('bottom', $context, $blocks);
        // line 58
        if ($this->getAttribute($this->getAttribute($this->getAttribute(($context["config"] ?? null), "plugins", array()), "simplesearch", array()), "enabled", array())) {
            // line 59
            echo "    <script src=\"/user/plugins/simplesearch/js/simplesearch.js\" type=\"text/javascript\" ></script>
";
        }
        // line 61
        echo "
</html>
";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "      <meta charset = \"utf-8\">
      <meta http-equiv = \"X-UA-Compatible\" content = \"IE = edge\">
      <meta name = \"viewport\" content = \"width = device-width, initial-scale = 1\">
      ";
        // line 8
        if ($this->getAttribute(($context["header"] ?? null), "description", array())) {
            // line 9
            echo "      <meta name = \"description\" content = \"";
            echo $this->getAttribute(($context["header"] ?? null), "description", array());
            echo "\">
      ";
        } else {
            // line 11
            echo "      <meta name = \"description\" content = \"";
            echo $this->getAttribute(($context["site"] ?? null), "description", array());
            echo "\">
      ";
        }
        // line 13
        echo "      ";
        if ($this->getAttribute(($context["header"] ?? null), "robots", array())) {
            // line 14
            echo "      <meta name = \"robots\" content = \"";
            echo $this->getAttribute(($context["header"] ?? null), "robots", array());
            echo "\">
      ";
        }
        // line 16
        echo "      <link rel = \"icon\" type = \"image/png\" href=\"";
        echo ($context["theme_url"] ?? null);
        echo "/images/favicon.ico\">

      <title>";
        // line 18
        if ($this->getAttribute(($context["header"] ?? null), "title", array())) {
            echo $this->getAttribute(($context["header"] ?? null), "title", array());
            echo " | ";
        }
        echo $this->getAttribute(($context["site"] ?? null), "title", array());
        echo "</title>

      ";
        // line 20
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 29
        echo "
      ";
        // line 30
        $this->displayBlock('javascripts', $context, $blocks);
        // line 41
        echo "
      ";
    }

    // line 20
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 21
        echo "         ";
        // line 22
        echo "         ";
        $this->getAttribute(($context["assets"] ?? null), "add", array(0 => "theme://css/bootstrap.min.css", 1 => 101), "method");
        // line 23
        echo "
      ";
        // line 25
        echo "         ";
        $this->getAttribute(($context["assets"] ?? null), "add", array(0 => "theme://css/bootstrap-custom.css", 1 => 100), "method");
        // line 26
        echo "
         ";
        // line 27
        echo $this->getAttribute(($context["assets"] ?? null), "css", array(), "method");
        echo "
      ";
    }

    // line 30
    public function block_javascripts($context, array $blocks = array())
    {
        // line 31
        echo "         ";
        $this->getAttribute(($context["assets"] ?? null), "add", array(0 => "theme://js/jquery.min.js", 1 => 101), "method");
        // line 32
        echo "         ";
        $this->getAttribute(($context["assets"] ?? null), "add", array(0 => "theme://js/bootstrap.min.js"), "method");
        // line 33
        echo "
         ";
        // line 34
        if (((($this->getAttribute(($context["browser"] ?? null), "getBrowser", array()) == "msie") && ($this->getAttribute(($context["browser"] ?? null), "getVersion", array()) >= 8)) && ($this->getAttribute(($context["browser"] ?? null), "getVersion", array()) <= 9))) {
            // line 35
            echo "            ";
            $this->getAttribute(($context["assets"] ?? null), "add", array(0 => "theme://js/html5shiv.min.js"), "method");
            // line 36
            echo "            ";
            $this->getAttribute(($context["assets"] ?? null), "add", array(0 => "theme://js/respond.min.js"), "method");
            // line 37
            echo "         ";
        }
        // line 38
        echo "
         ";
        // line 39
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
      ";
    }

    // line 47
    public function block_content($context, array $blocks = array())
    {
    }

    // line 57
    public function block_bottom($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "partials/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 57,  182 => 47,  176 => 39,  173 => 38,  170 => 37,  167 => 36,  164 => 35,  162 => 34,  159 => 33,  156 => 32,  153 => 31,  150 => 30,  144 => 27,  141 => 26,  138 => 25,  135 => 23,  132 => 22,  130 => 21,  127 => 20,  122 => 41,  120 => 30,  117 => 29,  115 => 20,  106 => 18,  100 => 16,  94 => 14,  91 => 13,  85 => 11,  79 => 9,  77 => 8,  72 => 5,  69 => 4,  63 => 61,  59 => 59,  57 => 58,  55 => 57,  45 => 50,  41 => 48,  38 => 47,  35 => 46,  31 => 43,  29 => 4,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
   <head>
      {% block head %}
      <meta charset = \"utf-8\">
      <meta http-equiv = \"X-UA-Compatible\" content = \"IE = edge\">
      <meta name = \"viewport\" content = \"width = device-width, initial-scale = 1\">
      {% if header.description %}
      <meta name = \"description\" content = \"{{ header.description }}\">
      {% else %}
      <meta name = \"description\" content = \"{{ site.description }}\">
      {% endif %}
      {% if header.robots %}
      <meta name = \"robots\" content = \"{{ header.robots }}\">
      {% endif %}
      <link rel = \"icon\" type = \"image/png\" href=\"{{ theme_url }}/images/favicon.ico\">

      <title>{% if header.title %}{{ header.title }} | {% endif %}{{ site.title }}</title>

      {% block stylesheets %}
         {# Bootstrap core CSS #}
         {% do assets.add('theme://css/bootstrap.min.css',101) %}

      {# Custom styles for this theme #}
         {% do assets.add('theme://css/bootstrap-custom.css',100) %}

         {{ assets.css() }}
      {% endblock %}

      {% block javascripts %}
         {% do assets.add('theme://js/jquery.min.js', 101) %}
         {% do assets.add('theme://js/bootstrap.min.js') %}

         {% if browser.getBrowser == 'msie' and browser.getVersion >= 8 and browser.getVersion <= 9 %}
            {% do assets.add('theme://js/html5shiv.min.js') %}
            {% do assets.add('theme://js/respond.min.js') %}
         {% endif %}

         {{ assets.js() }}
      {% endblock %}

      {% endblock head %}
   </head>
      <body>
         {# include the header + navigation #}
         {% include 'partials/header.html.twig' %}
            {% block content %}{% endblock %}
         <div class = \"footer\">
            <div class = \"container\">
               <p>{{ 'footer'|t }}</p>
\t       <p><a name=\"logo\" class=\"footer-logo\" href=\"/\">
\t\t<img src=\"/user/themes/fair-coop/images/footer-logo.png\" alt\"Footer Logo\">
\t       </a></p>
            </div>
         </div>
      </body>
   {% block bottom %}{% endblock %}
{% if config.plugins.simplesearch.enabled %}
    <script src=\"/user/plugins/simplesearch/js/simplesearch.js\" type=\"text/javascript\" ></script>
{% endif %}

</html>
", "partials/base.html.twig", "/var/www/clients/client8/web19/web/user/themes/fair-coop/templates/partials/base.html.twig");
    }
}
