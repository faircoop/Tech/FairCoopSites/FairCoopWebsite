<?php

/* modular/values.html.twig */
class __TwigTemplate_ea6f5bf6dcdeba47e31f0b631d91404e69bae67433186c934e886e54f64fd3c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-values\">
<div class=\"container\">
\t<h2>";
        // line 3
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "title", array());
        echo "</h2>
\t<div id=\"accordion\">
  <div class=\"row\">
   <div class=\"col-md-3\">
    <div class=\"card-header\"  id=\"headingOne\">
      <h5 class=\"mb-0\">
        <button class=\"btn-values\" data-toggle=\"collapse\" data-target=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
         \t";
        // line 10
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "values1", array()), "heading", array())) {
            // line 11
            echo "\t\t";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "values1", array()), "heading", array());
            echo "
\t\t";
        }
        // line 13
        echo "        </button >
      </h5>
    </div>
    <div class=\"card-header\"  id=\"headingTwo\">
      <h5 class=\"mb-0\">
        <button class=\"btn-values collapsed\" data-toggle=\"collapse\" data-target=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
         \t";
        // line 19
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "values2", array()), "heading", array())) {
            // line 20
            echo "\t\t";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "values2", array()), "heading", array());
            echo "
\t\t";
        }
        // line 22
        echo "        </button >
      </h5>
    </div>
    <div class=\"card-header\"  id=\"headingThree\">
      <h5 class=\"mb-0\">
        <button class=\"btn-values collapsed\" data-toggle=\"collapse\" data-target=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
          \t";
        // line 28
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "values3", array()), "heading", array())) {
            // line 29
            echo "\t\t";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "values3", array()), "heading", array());
            echo "
\t\t";
        }
        // line 31
        echo "        </button >
      </h5>
    </div>
    <div class=\"card-header\"  id=\"headingFour\">
      <h5 class=\"mb-0\">
        <button class=\"btn-values collapsed\" data-toggle=\"collapse\" data-target=\"#collapseFour\" aria-expanded=\"false\" aria-controls=\"collapseFour\">
         \t";
        // line 37
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "values4", array()), "heading", array())) {
            // line 38
            echo "\t\t";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "values4", array()), "heading", array());
            echo "
\t\t";
        }
        // line 40
        echo "        </button >
      </h5>
    </div>
    <div class=\"card-header\" id=\"headingFive\">
      <h5 class=\"mb-0\">
        <button class=\"btn-values collapsed\" data-toggle=\"collapse\" data-target=\"#collapseFive\" aria-expanded=\"false\" aria-controls=\"collapseFive\">
         \t";
        // line 46
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "values5", array()), "heading", array())) {
            // line 47
            echo "\t\t";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "values5", array()), "heading", array());
            echo "
\t\t";
        }
        // line 49
        echo "        </button >
      </h5>
    </div>
   </div>



   <div class=\"col-md-9\">
    <div id=\"collapseOne\" class=\"collapse show\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">
      <div class=\"card-body\">
        \t";
        // line 59
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "values1", array()), "description", array())) {
            // line 60
            echo "\t\t<p>";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "values1", array()), "description", array());
            echo "</p>
\t\t";
        }
        // line 62
        echo "      </div>
    </div>


    <div id=\"collapseTwo\" class=\"collapse\" aria-labelledby=\"headingTwo\" data-parent=\"#accordion\">
       <div class=\"card-body\">
        \t";
        // line 68
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "values2", array()), "description", array())) {
            // line 69
            echo "\t\t<p>";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "values2", array()), "description", array());
            echo "</p>
\t\t";
        }
        // line 71
        echo "      </div>
    </div>


    <div id=\"collapseThree\" class=\"collapse\" aria-labelledby=\"headingThree\" data-parent=\"#accordion\">
       <div class=\"card-body\">
        \t";
        // line 77
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "values3", array()), "description", array())) {
            // line 78
            echo "\t\t<p>";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "values3", array()), "description", array());
            echo "</p>
\t\t";
        }
        // line 80
        echo "      </div>
    </div>


    <div id=\"collapseFour\" class=\"collapse\" aria-labelledby=\"headingFour\" data-parent=\"#accordion\">
      <div class=\"card-body\">
        \t";
        // line 86
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "values4", array()), "description", array())) {
            // line 87
            echo "\t\t<p>";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "values4", array()), "description", array());
            echo "</p>
\t\t";
        }
        // line 89
        echo "      </div>
    </div>


    <div id=\"collapseFive\" class=\"collapse\" aria-labelledby=\"headingFive\" data-parent=\"#accordion\">
      <div class=\"card-body\">
        \t";
        // line 95
        if ($this->getAttribute($this->getAttribute(($context["header"] ?? null), "values5", array()), "description", array())) {
            // line 96
            echo "\t\t<p>";
            echo $this->getAttribute($this->getAttribute(($context["header"] ?? null), "values5", array()), "description", array());
            echo "</p>
\t\t";
        }
        // line 98
        echo "      </div>
    </div>
  </div>
</div>
\t";
        // line 102
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "
</div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/values.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 102,  189 => 98,  183 => 96,  181 => 95,  173 => 89,  167 => 87,  165 => 86,  157 => 80,  151 => 78,  149 => 77,  141 => 71,  135 => 69,  133 => 68,  125 => 62,  119 => 60,  117 => 59,  105 => 49,  99 => 47,  97 => 46,  89 => 40,  83 => 38,  81 => 37,  73 => 31,  67 => 29,  65 => 28,  57 => 22,  51 => 20,  49 => 19,  41 => 13,  35 => 11,  33 => 10,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-values\">
<div class=\"container\">
\t<h2>{{ page.header.title }}</h2>
\t<div id=\"accordion\">
  <div class=\"row\">
   <div class=\"col-md-3\">
    <div class=\"card-header\"  id=\"headingOne\">
      <h5 class=\"mb-0\">
        <button class=\"btn-values\" data-toggle=\"collapse\" data-target=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
         \t{% if header.values1.heading %}
\t\t{{ header.values1.heading }}
\t\t{% endif %}
        </button >
      </h5>
    </div>
    <div class=\"card-header\"  id=\"headingTwo\">
      <h5 class=\"mb-0\">
        <button class=\"btn-values collapsed\" data-toggle=\"collapse\" data-target=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
         \t{% if header.values2.heading %}
\t\t{{ header.values2.heading }}
\t\t{% endif %}
        </button >
      </h5>
    </div>
    <div class=\"card-header\"  id=\"headingThree\">
      <h5 class=\"mb-0\">
        <button class=\"btn-values collapsed\" data-toggle=\"collapse\" data-target=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
          \t{% if header.values3.heading %}
\t\t{{ header.values3.heading }}
\t\t{% endif %}
        </button >
      </h5>
    </div>
    <div class=\"card-header\"  id=\"headingFour\">
      <h5 class=\"mb-0\">
        <button class=\"btn-values collapsed\" data-toggle=\"collapse\" data-target=\"#collapseFour\" aria-expanded=\"false\" aria-controls=\"collapseFour\">
         \t{% if header.values4.heading %}
\t\t{{ header.values4.heading }}
\t\t{% endif %}
        </button >
      </h5>
    </div>
    <div class=\"card-header\" id=\"headingFive\">
      <h5 class=\"mb-0\">
        <button class=\"btn-values collapsed\" data-toggle=\"collapse\" data-target=\"#collapseFive\" aria-expanded=\"false\" aria-controls=\"collapseFive\">
         \t{% if header.values5.heading %}
\t\t{{ header.values5.heading }}
\t\t{% endif %}
        </button >
      </h5>
    </div>
   </div>



   <div class=\"col-md-9\">
    <div id=\"collapseOne\" class=\"collapse show\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">
      <div class=\"card-body\">
        \t{% if  header.values1.description %}
\t\t<p>{{  header.values1.description }}</p>
\t\t{% endif %}
      </div>
    </div>


    <div id=\"collapseTwo\" class=\"collapse\" aria-labelledby=\"headingTwo\" data-parent=\"#accordion\">
       <div class=\"card-body\">
        \t{% if  header.values2.description %}
\t\t<p>{{  header.values2.description }}</p>
\t\t{% endif %}
      </div>
    </div>


    <div id=\"collapseThree\" class=\"collapse\" aria-labelledby=\"headingThree\" data-parent=\"#accordion\">
       <div class=\"card-body\">
        \t{% if  header.values3.description %}
\t\t<p>{{  header.values3.description }}</p>
\t\t{% endif %}
      </div>
    </div>


    <div id=\"collapseFour\" class=\"collapse\" aria-labelledby=\"headingFour\" data-parent=\"#accordion\">
      <div class=\"card-body\">
        \t{% if  header.values4.description %}
\t\t<p>{{  header.values4.description }}</p>
\t\t{% endif %}
      </div>
    </div>


    <div id=\"collapseFive\" class=\"collapse\" aria-labelledby=\"headingFive\" data-parent=\"#accordion\">
      <div class=\"card-body\">
        \t{% if  header.values5.description %}
\t\t<p>{{  header.values5.description }}</p>
\t\t{% endif %}
      </div>
    </div>
  </div>
</div>
\t{{ page.content }}
</div>
</section>
", "modular/values.html.twig", "/var/www/clients/client8/web19/web/user/themes/fair-coop/templates/modular/values.html.twig");
    }
}
