<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/clients/client8/web19/web/user/plugins/frontend-edit-button/languages.yaml',
    'modified' => 1522159736,
    'data' => [
        'en' => [
            'PLUGIN_FRONTEND_EDIT_BUTTON' => [
                'ENABLED' => 'Enabled',
                'DISABLED' => 'Disabled',
                'POSITION_LABEL' => 'Label Position',
                'POSITION_LABEL_HELP' => 'Where to put the button on the frontend',
                'SHOW_LABEL' => 'Show text on button',
                'SHOW_ICON' => 'Show icon on button',
                'BUTTON_TEXT' => 'Edit this page',
                'TOP_LEFT' => 'Top Left',
                'TOP_RIGHT' => 'Top Right',
                'BOTTOM_LEFT' => 'Bottom Left',
                'BOTTOM_RIGHT' => 'Bottom Right',
                'REQUIRES_AUTH_LABEL' => 'Only visible after login',
                'REQUIRES_AUTH_LABEL_HELP' => 'If deactivated, the button will always be shown if the required plugins are active.'
            ]
        ],
        'de' => [
            'PLUGIN_FRONTEND_EDIT_BUTTON' => [
                'ENABLED' => 'Aktiviert',
                'DISABLED' => 'Deaktiviert',
                'POSITION_LABEL' => 'Position',
                'POSITION_LABEL_HELP' => 'Wo soll der Button angezeigt werden',
                'SHOW_LABEL' => 'Text auf dem Button anzeigen',
                'SHOW_ICON' => 'Symbol auf dem Button anzeigen',
                'BUTTON_TEXT' => 'Bearbeiten',
                'TOP_LEFT' => 'Linksoben',
                'TOP_RIGHT' => 'Rechtsoben',
                'BOTTOM_LEFT' => 'Linksuntern',
                'BOTTOM_RIGHT' => 'Rechtsuntern',
                'REQUIRES_AUTH_LABEL' => 'Nur anzeigen wen eingeloggt',
                'REQUIRES_AUTH_LABEL_HELP' => 'Wenn deaktiviert, wird der Button immer angezeigt wenn die benötigten Plugins aktiviert sind.'
            ]
        ],
        'hr' => [
            'PLUGIN_FRONTEND_EDIT_BUTTON' => [
                'ENABLED' => 'Omogućen',
                'DISABLED' => 'Onemogućen',
                'POSITION_LABEL' => 'Položaj gumba',
                'POSITION_LABEL_HELP' => 'Gdje da postavim gumb na stranici',
                'SHOW_LABEL' => 'Prikaži text na gumbu',
                'SHOW_ICON' => 'Prikaži ikonu na gumbu',
                'BUTTON_TEXT' => 'Uredi ovu stranicu',
                'TOP_LEFT' => 'Gore Lijevo',
                'TOP_RIGHT' => 'Gore Desno',
                'BOTTOM_LEFT' => 'Dolje Lijevo',
                'BOTTOM_RIGHT' => 'Dolje Desno',
                'REQUIRES_AUTH_LABEL' => 'Vidljiv samo nakon prijave.',
                'REQUIRES_AUTH_LABEL_HELP' => 'Ako je deaktiviran, gumb će uvijek biti prikazan ako su potrebni plugini aktivni.'
            ]
        ],
        'nl' => [
            'PLUGIN_FRONTEND_EDIT_BUTTON' => [
                'ENABLED' => 'Ingeschakeld',
                'DISABLED' => 'Uitgeschakeld',
                'POSITION_LABEL' => 'Positie van de button',
                'POSITION_LABEL_HELP' => 'Waar moet de button worden getoond',
                'SHOW_LABEL' => 'Tonen tekst op de button',
                'SHOW_ICON' => 'Tonen icon op de button',
                'BUTTON_TEXT' => 'Pagina wijzigen',
                'TOP_LEFT' => 'Linksboven',
                'TOP_RIGHT' => 'Rechtsboven',
                'BOTTOM_LEFT' => 'Linksonder',
                'BOTTOM_RIGHT' => 'Rechtsonder',
                'REQUIRES_AUTH_LABEL' => 'Alleen zichtbaar indien ingelogt',
                'REQUIRES_AUTH_LABEL_HELP' => 'Indien inaktief, wordt de button altijd getoond als de plugin aktief is.'
            ]
        ],
        'es' => [
            'PLUGIN_FRONTEND_EDIT_BUTTON' => [
                'ENABLED' => 'Habilitado',
                'DISABLED' => 'Deshabilitado',
                'POSITION_LABEL' => 'Posición de etiqueta',
                'POSITION_LABEL_HELP' => 'Donde poner el botón en la portada',
                'SHOW_LABEL' => 'Muestra la etiqueta',
                'SHOW_ICON' => 'Muestra el icono en el botón',
                'BUTTON_TEXT' => 'Editar',
                'TOP_LEFT' => 'Arriba a la izquierda',
                'TOP_RIGHT' => 'Arriba a la derecha',
                'BOTTOM_LEFT' => 'Debajo a la izquierda',
                'BOTTOM_RIGHT' => 'Debajo a la derecha',
                'REQUIRES_AUTH_LABEL' => 'Solo visible después del inicio de sesión',
                'REQUIRES_AUTH_LABEL_HELP' => 'Si se desactiva, el botón siempre se mostrará si los complementos necesarios están activos.'
            ]
        ],
        'ca' => [
            'PLUGIN_FRONTEND_EDIT_BUTTON' => [
                'ENABLED' => 'Habilitat',
                'DISABLED' => 'Deshabilitat',
                'POSITION_LABEL' => 'Posició d\'etiqueta',
                'POSITION_LABEL_HELP' => 'On posar el botó a la portada',
                'SHOW_LABEL' => 'Mostra l\'etiqueta',
                'SHOW_ICON' => 'Mostra la icona al botó',
                'BUTTON_TEXT' => 'Edita',
                'TOP_LEFT' => 'A dalt a l\'esquerra',
                'TOP_RIGHT' => 'A dalt a la dreta',
                'BOTTOM_LEFT' => 'A sota a l\'esquerra',
                'BOTTOM_RIGHT' => 'A sota a la dreta',
                'REQUIRES_AUTH_LABEL' => 'Només visible després d\'iniciar sessió',
                'REQUIRES_AUTH_LABEL_HELP' => 'Si està desactivat, el botó sempre es mostrarà si els connectors requerits estan actius.'
            ]
        ],
        'fr' => [
            'PLUGIN_FRONTEND_EDIT_BUTTON' => [
                'ENABLED' => 'Activé',
                'DISABLED' => 'Désactivé',
                'POSITION_LABEL' => 'Position du tag',
                'POSITION_LABEL_HELP' => 'Où mettre le bouton sur la couverture',
                'SHOW_LABEL' => 'Afficher le tag',
                'SHOW_ICON' => 'Afficher l\'icône sur le bouton',
                'BUTTON_TEXT' => 'Modifier',
                'TOP_LEFT' => 'En haut à gauche',
                'TOP_RIGHT' => 'En haut à droite',
                'BOTTOM_LEFT' => 'Ci-dessous à gauche',
                'BOTTOM_RIGHT' => 'Ci-dessous à droite',
                'REQUIRES_AUTH_LABEL' => 'Seulement visible après la connexion',
                'REQUIRES_AUTH_LABEL_HELP' => 'Si désactivé, le bouton sera toujours affiché si les plugins requis sont actifs.'
            ]
        ]
    ]
];
