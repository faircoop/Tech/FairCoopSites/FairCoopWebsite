<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/clients/client8/web19/web/user/plugins/login-oauth/login-oauth.yaml',
    'modified' => 1519806210,
    'data' => [
        'enabled' => true,
        'built_in_css' => true,
        'providers' => [
            'Facebook' => [
                'enabled' => false,
                'enable_email' => false,
                'credentials' => [
                    'key' => NULL,
                    'secret' => NULL
                ]
            ],
            'Google' => [
                'enabled' => false,
                'credentials' => [
                    'key' => NULL,
                    'secret' => NULL
                ]
            ],
            'GitHub' => [
                'enabled' => true,
                'credentials' => [
                    'key' => '0de9a25e83847d515c80',
                    'secret' => 'cbf902510cdf3e09d8f38d61e22da0e82a677d29'
                ]
            ],
            'Twitter' => [
                'enabled' => false,
                'credentials' => [
                    'key' => NULL,
                    'secret' => NULL
                ]
            ],
            'Linkedin' => [
                'enabled' => false,
                'credentials' => [
                    'key' => NULL,
                    'secret' => NULL
                ]
            ]
        ]
    ]
];
